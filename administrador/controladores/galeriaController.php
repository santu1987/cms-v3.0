<?php
require_once("../modelos/galeriaModel.php");
require_once("../modelos/categoriasModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar_imagenes':
			consultar_imagenes($arreglo_datos);
			break;	
		case 'borrar_imagen':
			borrar_imagen($arreglo_datos["imagenes"]);
			break;	 			
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	
	if(isset($data->categoria))
		$user_data["categoria"] = $data->categoria;
	else
		$user_data["categoria"] = "";

	if(isset($data->nombre)){
		if ($data->nombre){
			$user_data["nombre"] = $data->nombre;	
		}else{
			$user_data["nombre"] = "";
		}
	}else
		$user_data["nombre"] = "";
	if(isset($data->id_idioma))
		$user_data["id_idioma"] = $data->id_idioma;
	else
		$user_data["id_idioma"] = "";	
	if(isset($data->imagenes))	
		$user_data["imagenes"] = $data->imagenes;
	else
		$user_data["imagenes"] = "";
			
	return $user_data;
}
//------------------------------------------------------
function consultar_imagenes($arreglo_datos){
	//var_dump($arreglo_datos);
	//------------------------------------
	$recordset = array();
	$objeto = new galeriaModel();
	$categoria = $arreglo_datos["categoria"];
	$nombre = $arreglo_datos["nombre"];

	$recordset = $objeto->consultar_galeria($categoria,$nombre);
	$i = 0;
	if($recordset!=null){
		//-------------------------------------------------
		foreach ($recordset as $campo) {
			//$campo_def = explode("..",$campo[1]);
			$campo_dos = $campo[1];
			$imagenes[] = array("id"=>$campo[0],"path_imagen"=>$campo_dos);
			$i++;
		}
		//-------------------------------------------------
		die(json_encode($imagenes));

	}
	die("");	
	//----------------------------------
}
//-------------------------------------------------------
function borrar_imagen($imagenes){
	$recordset = array();
	$objeto = new galeriaModel();
	$recordset = $objeto->borrar_imagen($imagenes);
	$i = 0;
	//die(json_encode($recordset));
	if($recordset==true){
		$mensajes["mensajes"] = "proceso exitoso";
	}else{
		$mensajes["mensajes"] = "error";
	}
	die(json_encode($mensajes));
}