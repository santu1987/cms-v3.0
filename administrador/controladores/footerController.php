<?php
require_once("../modelos/footerModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_footer':
			registrar_footer($arreglo_datos);
			break;
		case 'consultar_footer':
			consultar_footer();
			break;	
		case 'modificar_footer':
			modificar_footer($arreglo_datos);
			break;		
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;				
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	
	if(isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] = "";

	if (isset($data->descripcion)){
		$user_data["descripcion"] = $data->descripcion;	
	}else{
		$user_data["descripcion"] = "";
	}

	if (isset($data->correo)){
		$user_data["correo"] = $data->correo;	
	}else{
		$user_data["correo"] = "";
	}

	if (isset($data->rif)){
		$user_data["rif"] = $data->rif;	
	}else{
		$user_data["rif"] = "";
	}	
	
	return $user_data;
}
//------------------------------------------------------
function registrar_footer($arreglo_datos){
	//var_dump($arreglo_datos);
	//------------------------------------
	$recordset = array();
	$objeto = new footerModel();
	$recordset = $objeto->registrar_footer($arreglo_datos);
	if($recordset==1){
		$mensajes["mensajes"] = "registro_procesado";
		$id_footer = $objeto->maximo_id_footer();
		$mensajes["id"] = $id_footer[0][0];
		$id_footer = $id_footer[0][0];
		//----------------------------------------------------
		die(json_encode($mensajes));
	}else{
		$recordset[0][0]="error";
		die(json_encode($recordset));
	}
	//die(json_encode($recordset));	
}
//-------------------------------------------------------
function modificar_footer($arreglo_datos){
	//var_dump($arreglo_datos);
	//------------------------------------
	$recordset = array();
	$objeto = new footerModel();
	$existe = $objeto->existe_footer($arreglo_datos["id"]);
	if($existe[0][0]>0){
		//---
		$recordset = $objeto->modificar_footer($arreglo_datos);
		if($recordset==1){
			//Elimino los telefonos
			$id_direccion = $arreglo_datos["id"];
			$mensajes["mensajes"] = "modificacion_procesada";
		}else{
			$mensajes["mensajes"] = "error";
		}	
		//---
	}else{
		$mensajes["mensajes"] = "no_existe";
	}
	die(json_encode($mensajes));
	//die(json_encode($recordset));	
}
//-------------------------------------------------------
function consultar_footer(){
	$recordset = array();
	$mensajes = array();
	$objeto = new footerModel();
	$recordset = $objeto->consultar_footer();
	$i = 0;
	foreach ($recordset as $campo) {
		//$fecha = new DateTime($campo[5]);
		$a = $i+1;
		$mensajes[] = array("id"=>$campo[0],"descripcion"=>$campo[1],"correo"=>$campo[2],"rif"=>$campo[3],"number"=>$a);
		$i++;
	}
	die(json_encode($mensajes));
}
//-------------------------------------------------------
