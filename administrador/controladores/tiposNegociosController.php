<?php
require_once("../modelos/tiposNegociosModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar':
			consultar($arreglo_datos);
			break;
		case 'consultar_tipos_negocios':
			consultar_tipos_negocios($arreglo_datos);
			break;
		case 'consultar_tipos_negocios_activos':
			consultar_tipos_negocios_activos($arreglo_datos);
			break;	
		case 'registrar_tipos_negocios':
			registrar_tipos_negocios_form($arreglo_datos);
			break;	
		case 'modificar_tipos_negocios':
			modificar_tipos_negocios($arreglo_datos);
			break;	
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;				 			
	}
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	if(isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] = "";

	if(isset($data->titulo))
		$user_data["titulo"] = $data->titulo;
	else
		$user_data["titulo"] = "";

	if(isset($data->id_imagen))
		$user_data["id_imagen"] = $data->id_imagen;
	else
		$user_data["id_imagen"] = "";

	if(isset($data->descripcion))
		$user_data["descripcion"] = $data->descripcion;
	else 
		$user_data["descripcion"] = "";
	
	if( isset($data->id_idioma))
		$user_data["id_idioma"] = $data->id_idioma;
	else
		$user_data["id_idioma"] = "";
	
	if(isset($data->estatus))		
		$user_data["estatus"] = $data->estatus;
	else
		$user_data["estatus"] = "";
	
	return $user_data;
}
//---
//------------------------------------------------------
function registrar_tipos_negocios_form($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new tiposNegociosModel();
	//Verifico si existe
	if($arreglo_datos["id"]!=""){
		$mensajes["mensajes"] = "existe";
	}else{
		$existe = $objeto->existe_tipos_negocios_titulo($arreglo_datos["titulo"]);
		//Verifico si existe la imagen
		if($existe[0][0]>0){
			$mensajes["mensajes"] = "existe";
		}else{
			//no existe, guardo
			$recordset = $objeto->registrar_tipos_negocios($arreglo_datos);
			if($recordset==1){
				$mensajes["mensajes"] = "registro_procesado";
				$id_tipos_negocios = $objeto->maximo_id_tipos_negocios();
				$mensajes["id"] = $id_tipos_negocios[0][0];
			}else{
				$mensajes["mensajes"] = "error";
			}
		}	
	}
	die(json_encode($mensajes));
	//--
}	
//---
function modificar_tipos_negocios($arreglo_datos){
	$recordset = array();
	$objeto = new tiposNegociosModel();
	$existe = $objeto->existe_tipos_negocios($arreglo_datos["id"]);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		$recordset = $objeto->modificar_tipos_negocios($arreglo_datos);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function modificar_estatus($arreglo_datos){
	$recordset = array();
	$objeto = new tiposNegociosModel();
	$existe = $objeto->existe_tipos_negocios($arreglo_datos["id"]);
	//die($arreglo_datos["id"]);
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset = $objeto->modificar_tipos_negocios_estatus($arreglo_datos["id"],$arreglo_datos["estatus"]);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function consultar_tipos_negocios($arreglo_datos){
	$recordset = array();
	$mensajes = array();
	$objeto = new tiposNegociosModel();
	$recordset = $objeto->consultar_tipos_negocios2($arreglo_datos["estatus"]);
	$i = 0;
	foreach ($recordset as $campo) {
		$a = $i+1;
		$descripcion_corta = substr($campo[2],0,200)."...";
		$arreglo_idioma = array("id"=>$campo[4],"descripcion"=>$campo[5]);
		$mensajes[] = array("id"=>$campo[0],"titulo"=>$campo[1],"descripcion"=>$campo[2],"estatus"=>$campo[3],"idioma"=>$arreglo_idioma,"number"=>$a);
		$i++;
	}
	die(json_encode($mensajes));
}
//--
function consultar($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new especialidadesModel();
	$recordset = $objeto->consultar_especialidades();
	//die(json_encode($recordset));
	$i = 0;
	foreach ($recordset as $campo) {
		$mayusculas = normaliza(strtoupper($campo[1]));
		$minusculas = normaliza (strtolower($campo[1]));
		$especialidades[] = array("id"=>$campo[0],"titulo"=>$campo[1], "descripcion"=>$campo[2],"imagen"=>$campo[3],"number"=>$i, "selected"=>false, "mayusculas"=>$mayusculas, "minusculas"=>$minusculas, "cuantos"=>$campo[4]);
		$i++;
	}
	echo(json_encode($especialidades));
	//----------------------------------
}
//--
function consultar_tipos_negocios_activos($arreglo_datos){
	$recordset = array();
	$mensajes = array();
	$objeto = new tiposNegociosModel();
	$recordset = $objeto->consultar_tipos_negocios2($arreglo_datos["estatus"]);
	$i = 0;
	$a = 0;
	foreach ($recordset as $campo) {
		if($campo[3]=='1'){
			//---
			$a = $a+1;
			$arreglo_idioma = array("id"=>$campo[4],"descripcion"=>$campo[5]);
			$descripcion_corta = substr($campo[2],0,200)."...";
			$mensajes[] = array("id"=>$campo[0],"titulo"=>$campo[1],"descripcion"=>$campo[2],"estatus"=>$campo[3],"idioma"=>$arreglo_idioma,"number"=>$a);
			//---
		}
		
		$i++;
	}
	die(json_encode($mensajes));
}
?>