<?php
require_once("../modelos/contactosModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar_clientes':
			consultar_clientes();
			break;
		case 'consultar_empleos':
			consultar_empleos();
			break;	
	}
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	
	return $user_data;
}
//---
function consultar_clientes(){
//------------------------------------
	$recordset = array();
	$objeto = new contactosModel();
	$mensajes = array();
	//Verifico si existe un usuario con esa cedula....
	$recordset = $objeto->consultar_contactos();
	$i = 0;
	foreach ($recordset as $campo) {
		$a = $i+1;

		$mensaje_corto = substr($campo[4],0,200)."...";
		$mensajes[] = array("id"=>$campo[0],"nombres"=>$campo[1],"email"=>$campo[2],"telefono"=>$campo[3],"mensaje"=>$campo[4],"mensaje_corto"=>$mensaje_corto,"number"=>$a);
		$i++;
	}
	die(json_encode($mensajes));
}
//---
function consultar_empleos(){
//------------------------------------
	$recordset = array();
	$objeto = new contactosModel();
	$mensajes = array();
	//Verifico si existe un usuario con esa cedula....
	$recordset = $objeto->consultar_contactos_empleo();
	$i = 0;
	foreach ($recordset as $campo) {

		$mensaje_corto = substr($campo[4],0,200)."...";
		$mensajes[] = array("id"=>$campo[0],"nombres"=>$campo[1],"email"=>$campo[2],"area"=>$campo[3],"mensaje"=>$campo[4],"cv"=>$campo[5],"mensaje_corto"=>$mensaje_corto,"num"=>$i,"telefono"=>$campo[6]);

		$i++;
	}
	die(json_encode($mensajes));
//------------------------------------	
}