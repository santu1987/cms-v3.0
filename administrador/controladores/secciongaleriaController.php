<?php
require_once("../modelos/galeriaModel.php");
require_once("../modelos/secciongaleriaModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_galeria':
			registrar_galeria($arreglo_datos);
			break;	
		case 'modificar_galeria':
			modificar_galeria($arreglo_datos);
			break;
		case 'consultar_galeria':
			consultar_galeria($arreglo_datos);
			break;
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;			
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	if(isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] = "";
	if(isset($data->id_imagen))
		$user_data["id_imagen"] = $data->id_imagen;
	else
		$user_data["id_imagen"] = "";

	if(isset($data->descripcion))
		$user_data["descripcion"] = $data->descripcion;
	else
		$user_data["descripcion"] = "";

	if(isset($data->estatus))
		$user_data["estatus"] = $data->estatus;
	else
		$user_data["estatus"] = "";
	return $user_data;
}
//---
//------------------------------------------------------
function registrar_galeria($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new secciongaleriaModel();
	//Verifico si existe
	if($arreglo_datos["id"]!=""){
		$mensajes["mensajes"] = "existe";
	}else{
		//no existe, guardo
		$recordset_imagen = $objeto->registrar_imagen($arreglo_datos);
		if($recordset_imagen==1){
			$mensajes["mensajes"] = "registro_procesado";
			$id_imagen = $objeto->maximo_id_imagen();
			$mensajes["id"] = $id_imagen[0][0];
		}else{
			$mensajes["mensajes"] = "error";
		}
	}
	die(json_encode($mensajes));
	//--
}	
//---
function modificar_galeria($arreglo_datos){
	$recordset = array();
	$objeto = new secciongaleriaModel();
	$existe = $objeto->existe_imagen($arreglo_datos["id"]);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		$recordset = $objeto->modificar_imagen($arreglo_datos);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function modificar_estatus($arreglo_datos){
	$recordset = array();
	$objeto = new secciongaleriaModel();
	$existe = $objeto->existe_imagen($arreglo_datos["id"]);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset = $objeto->modificar_galeria_estatus($arreglo_datos["id"],$arreglo_datos["estatus"]);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function consultar_galeria(){
	$recordset = array();
	$mensajes = array();
	$objeto = new secciongaleriaModel();
	$recordset = $objeto->consultar_galeria_img();
	$i = 0;
	$soportes = "";
	foreach ($recordset as $campo) {
		$a = $i+1;
		$mensajes[] = array("id_galeria"=>$campo[0],"imagen"=>$campo[2],"descripcion"=>$campo[3],"id_imagen"=>$campo[1],"estatus"=>$campo[4],"number"=>$a);
		$i++;
	}
	die(json_encode($mensajes));
}