<?php
require_once("../modelos/correoAdminModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_correo':
			registrar_correo($arreglo_datos);
			break;
		case 'consultar_correos':
			consultar_correos();
			break;	
		case 'modificar_correo':
			modificar_correo($arreglo_datos);
			break;		
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;				
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	
	if(isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] = "";

	if (isset($data->titulo)){
		$user_data["titulo"] = $data->titulo;	
	}else{
		$user_data["titulo"] = "";
	}	
	

	if(isset($data->correo))	
		$user_data["correo"] = $data->correo;
	else
		$user_data["correo"] = "";
	
	if(isset($data->estatus))
		$user_data["estatus"] = $data->estatus;
	else
		$user_data["estatus"] = "";

	return $user_data;
}
//------------------------------------------------------
function registrar_correo($arreglo_datos){
	//var_dump($arreglo_datos);
	//------------------------------------
	$recordset = array();
	$telefono_vector = array();
	$objeto = new correoAdminModel();
	//Verifico si existe el email
	$existe = $objeto->existe_correo($arreglo_datos["correo"],1);
	if($existe[0][0]==0){
		//--
		$recordset = $objeto->registrar_correo($arreglo_datos);
		if($recordset==1){
			$mensajes["mensajes"] = "registro_procesado";
			$id_correo = $objeto->maximo_id_correo();
			$mensajes["id"] = $id_correo[0][0];
			$id_correo = $id_correo[0][0];
			
			die(json_encode($mensajes));
		}else{
			$mensajes["mensajes"]="error";
		}
		//--
	}else{
		$mensajes["mensajes"] = "existe";
	}
	
	die(json_encode($mensajes));	
}
//-------------------------------------------------------
function modificar_direccion($arreglo_datos){
	//var_dump($arreglo_datos);
	//------------------------------------
	$recordset = array();
	$telefono_vector = array();
	$objeto = new direccionModel();
	$existe = $objeto->existe_direccion($arreglo_datos["id"]);
	if($existe[0][0]>0){
		//---
		$recordset = $objeto->modificar_direccion($arreglo_datos);
		if($recordset==1){
			//Elimino los telefonos
			$id_direccion = $arreglo_datos["id"];
			$recordset_limpiar = $objeto->eliminar_telefonos($id_direccion);
			//Bloque para registrar telefonos
			$telefono_vector = $arreglo_datos["telefono"];
			foreach ($telefono_vector as $value) {
				//--
				$existe = $objeto->existe_telefono_direccion($id_direccion,$value);
				if($existe[0][0]==0){
					$recordset_telefono = $objeto->registrar_telefono($id_direccion,$value);
				}
				//--
			}
			$mensajes["mensajes"] = "modificacion_procesada";
		}else{
			$mensajes["mensajes"] = "error";
		}	
		//---
	}else{
		$mensajes["mensajes"] = "no_existe";
	}
	die(json_encode($mensajes));
	//die(json_encode($recordset));	
}
//-------------------------------------------------------
function consultar_correos(){
	$recordset = array();
	$mensajes = array();
	$objeto = new correoAdminModel();
	$recordset = $objeto->consultar_correos();
	$i = 0;
	$soportes = "";
	foreach ($recordset as $campo) {
		//$fecha = new DateTime($campo[5]);
		$a = $i+1;
		$mensajes[] = array("id"=>$campo[0],"titulo"=>$campo[1],"correo"=>$campo[2], "estatus"=>$campo[3],"number"=>$a);
		$i++;
	}
	die(json_encode($mensajes));
}
//-------------------------------------------------------
function modificar_estatus($arreglo_datos){
	//var_dump($arreglo_datos);
	$recordset = array();
	$objeto = new correoAdminModel();
	$existe = $objeto->existe_correo($arreglo_datos["id"],2);
	//die($existe);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset = $objeto->modificar_correo_estatus($arreglo_datos["id"],$arreglo_datos["estatus"]);
		//die($recordset);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function modificar_correo($arreglo_datos){
	//var_dump($arreglo_datos);
	$recordset = array();
	$objeto = new correoAdminModel();
	$existe = $objeto->existe_correo($arreglo_datos["id"],2);
	//die($existe);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		$existe_otro_correo = $objeto->existe_otro_correo($arreglo_datos["id"],$arreglo_datos["correo"]);
		if($existe_otro_correo[0][0]==0){
		//-----------------------------------
			$recordset = $objeto->modificar_correo($arreglo_datos);
			//die($recordset);
			if($recordset==1){
				$mensajes["mensajes"] = "modificacion_procesada"; 
			}else{
				$mensajes["error"] = "error";
			}
		//-----------------------------------	
		}else{
			$mensajes["mensajes"] = "existe_otro";
		}
		
	}
	die(json_encode($mensajes));
}