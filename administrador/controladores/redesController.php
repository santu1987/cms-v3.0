<?php
require_once("../modelos/redesModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar_redes':
			consultar_redes();
			break;
		case 'registrar_redes':
			registrar_redes($arreglo_datos);
			break;
		case 'consultar_redes_detalle':
			consultar_redes_detalle($arreglo_datos);			
			break;
	}
}
//---
function helper_userdata($data){

	$user_data = array();
	$user_data["accion"] = $data->accion;
	
	if(isset($data->id)){
		$user_data["id"] = $data->id;
	}else{
		$user_data["id"] = "";
	}

	if(isset($data->tipo_red)){
		$user_data["tipo_red"] = $data->tipo_red;
	}else{
		$user_data["tipo_red"] = "";
	}

	if(isset($data->url_red)){
		$user_data["url_red"] = $data->url_red;
	}else{
		$user_data["url_red"] = "";
	}

	if(isset($data->id_tipo_red)){
		$user_data["id_tipo_red"] = $data->id_tipo_red;
	}else{
		$user_data["id_tipo_red"] = "";
	}
	return $user_data;
}
//------------------------------------------------------
function consultar_redes(){
	//------------------------------------
	$recordset = array();
	$objeto = new redesModel();
	$recordset = $objeto->consultar_redes();
	//die(json_encode($recordset));
	$i = 0;
	foreach ($recordset as $campo) {
		$redes[] = array("id"=>$campo[0],"descripcion"=>$campo[1]);
		$i++;
	}
	echo(json_encode($redes));
	//----------------------------------
}
//-------------------------------------------------------
function registrar_redes($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new redesModel();
	//Verifico si existe
	if($arreglo_datos["id"]!=""){
		//--Para modificar
		$existe = $objeto->existe_red($arreglo_datos["id"]);
		if($existe[0][0]>0){
			//--
			$recordset = $objeto->modificar_red($arreglo_datos);
			if($recordset==1){
				$mensajes["mensajes"] = "modificacion_procesada";
				$mensajes["id"] = $arreglo_datos["id"];
			}else{
				$mensajes["mensajes"] = "error";
			}	
		}else{
			$mensajes["mensajes"] = "no_existe";
		}
		
	}else{
		//--Para registrar
		$recordset = $objeto->registrar_red($arreglo_datos);
		if($recordset==1){
			$mensajes["mensajes"] = "registro_procesado";
			$id_red = $objeto->maximo_id_redes();
			$mensajes["id"] = $id_red[0][0];
		}else{
			$mensajes["mensajes"] = "error";
		}
		//--------------------------------	
	}
	die(json_encode($mensajes));
	//--
}
//-------------------------------------------------------
function consultar_redes_detalle($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new redesModel();
	$recordset = $objeto->consultar_redes_detalle($arreglo_datos["id_tipo_red"]);
	//die($recordset);
	$i = 0;
	foreach ($recordset as $campo) {
		$redes[] = array("id"=>$campo[0],"url_red"=>$campo[1]);
		$i++;
	}
	die(json_encode($redes));
	//----------------------------------
}

