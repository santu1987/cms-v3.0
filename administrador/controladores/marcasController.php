<?php
require_once("../modelos/marcasModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_marca':
			registrar_marca($arreglo_datos);
			break;	
		case 'modificar_marca':
			modificar_marca($arreglo_datos);
			break;
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;	
		case 'consultar_marcas':
			consultar_marcas();
			break;
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	if(isset($data->descripcion))
		$user_data["descripcion"] = $data->descripcion;
	else
		$user_data["descripcion"] = "";

	if(isset($data->id_soportes_marca))		
		$user_data["id_soportes_marca"] = $data->id_soportes_marca;
	else
		$user_data["id_soportes_marca"] = "";
	if( isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] = "";
	if( isset($data->id_idioma))
		$user_data["id_idioma"] = $data->id_idioma;
	else
		$user_data["id_idioma"] = "";
	if( isset($data->estatus))
		$user_data["estatus"] = $data->estatus;
	else
		$user_data["estatus"] = "";
	return $user_data;
}
//------------------------------------------------------
function registrar_marca($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new marcasModel();
	//reguistro la marca
	$recordset_marca = $objeto->registrar_marca($arreglo_datos);
	if($recordset_marca==1){
		$mensajes["mensajes"] = "registro_procesado";
		$id_marca = $objeto->maximo_id_marca();
		$mensajes["id_marca"] = $id_marca[0][0];
	}else{
		$mensajes["error"] = "error";
	}
	die(json_encode($mensajes));
}
//----------------------------------
function modificar_marca($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto_marcas = new marcasModel();
	$existe_marcas = $objeto_marcas->consultar_existe_marcas2($arreglo_datos["id"]);
	if($existe_marcas[0][0]==0){
		$mensajes["mensajes"] = "no_existe_marca"; 
	}else{//si existe....

		$recordset_marca = $objeto_marcas->modificar_marca($arreglo_datos);
		if($recordset_marca==1){
			$mensajes["mensajes"] = "modificacion_procesada";
			$mensajes["id_marca"] = $arreglo_datos["id"];
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---------------------------------------------------
function modificar_estatus($arreglo_datos){
	$recordset = array();
	$objeto = new marcasModel();
	$existe = $objeto->consultar_existe_marcas2($arreglo_datos["id"]);
	//Verifico si existe la empresa
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset = $objeto->modificar_marca_estatus($arreglo_datos["id"],$arreglo_datos["estatus"]);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---------------------------------------------------
function consultar_marca(){
	$recordset = array();
	$mensajes = array();
	$objeto = new marcasModel();
	$recordset = $objeto->consultar_marca();
	$i = 0;
	$soportes = "";
        $id_soportes = "";
	foreach ($recordset as $campo) {
		$a = $i+1;
		$record_soportes = $objeto->consulta_soportes_marcas($campo[0]);
		$x = 0;
		$soportes = "";
		foreach ($record_soportes as $campo2) {
			if ($x==0){
				$soportes.=$campo2[0];
				$id_soportes.=$campo2[1];	
			}else{
				$soportes.="|".$campo2[0];
				$id_soportes.="|".$campo2[1];	
			}
			$x++;
		}
		$mensajes[] = array("id"=>$campo[0],"descripcion"=>$campo[1],"marcas_soportes"=>$soportes,"id_soportes"=>$id_soportes,"number"=>$a);
		$i++;
		$id_soportes = "";
	}
	die(json_encode($mensajes));
}
//-----------------------------------------------------
function consultar_marcas(){
	$recordset = array();
	$mensajes = array();
	$objeto = new marcasModel();
	$recordset = $objeto->consultar_marca();
	$i = 0;
	$soportes = "";
        $id_soportes = "";
	foreach ($recordset as $campo) {
		$a = $i+1;
		$record_soportes = $objeto->consulta_soportes_marcas($campo[0]);
		$x = 0;
		$soportes = "";
		foreach ($record_soportes as $campo2) {
			if ($x==0){
				$soportes.=$campo2[0];
				$id_soportes.=$campo2[1];	
			}else{
				$soportes.="|".$campo2[0];
				$id_soportes.="|".$campo2[1];	
			}
			$x++;
		}
		$arreglo_idioma = array("id"=>$campo[2],"descripcion"=>$campo[4]);

		$mensajes[] = array("id"=>$campo[0],"descripcion"=>$campo[1],"descripcion_sh"=>strip_tags($campo[1]),"id_idioma"=>$campo[2],"estatus"=>$campo[3],"marcas_soportes"=>$soportes,"id_soportes"=>$id_soportes,"idioma"=>$arreglo_idioma,"number"=>$a);
		$i++;
		$id_soportes = "";
	}
	die(json_encode($mensajes));
} 
//-----------------------------------------------------