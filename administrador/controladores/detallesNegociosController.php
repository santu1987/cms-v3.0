<?php
require_once("../modelos/detallesNegociosModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_negocios':
			registrar_negocios($arreglo_datos);
			break;	
		case 'modificar_negocios':
			modificar_negocios($arreglo_datos);
			break;
		case 'consultar_negocios':
			consultar_negocios();
			break;	
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;		
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	if(isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] = 	"";

	if(isset($data->tipo_negocio))
		$user_data["tipo_negocio"] = $data->tipo_negocio;
	else
		$user_data["tipo_negocio"] = "";	
	if(isset($data->titulo))
		$user_data["titulo"] = $data->titulo;
	else 
		$user_data["titulo"] = "";

	if(isset($data->id_imagen))
		$user_data["id_imagen"] = $data->id_imagen;
	else
		$user_data["id_imagen"] = "";

	if(isset($data->descripcion1))
		$user_data["descripcion1"] = $data->descripcion1;
	else
		$user_data["descripcion1"] = "";

	if(isset($data->descripcion2))
		$user_data["descripcion2"] = $data->descripcion2;
	else
		$user_data["descripcion2"] = "";

	if(isset($data->descripcion3))
		$user_data["descripcion3"] = $data->descripcion3;
	else
		$user_data["descripcion3"] = "";

	if(isset($data->estatus))
		$user_data["estatus"] = $data->estatus;
	else
		$user_data["estatus"] = "";
	if(isset($data->id_idioma))
		$user_data["id_idioma"] = $data->id_idioma;
	else
		$user_data["id_idioma"] = "";
	return $user_data;
}
//---
//------------------------------------------------------
function registrar_negocios($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new detallesNegociosModel();
	//Verifico si existe
	if($arreglo_datos["id"]!=""){
		$mensajes["mensajes"] = "existe";
	}else{
		//Verificio que no exista un negocio con el mismo titulo
		$existe_titulo = $objeto->existe_titulo($arreglo_datos["titulo"]);
		if($existe_titulo[0][0]>0){
			$mensajes["mensajes"] = "existe_titulo";
		}else{
		//--------------------------------
			//no existe, guardo
			$recordset = $objeto->registrar_negocios($arreglo_datos);
			//die($recordset);
			if($recordset==1){
				$mensajes["mensajes"] = "registro_procesado";
				$id_imagen = $objeto->maximo_id_negocios();
				$mensajes["id"] = $id_imagen[0][0];
			}else{
				$mensajes["mensajes"] = "error";
			}
		//--------------------------------	
		}
	}
	die(json_encode($mensajes));
	//--
}	
//---
function modificar_negocios($arreglo_datos){
	$recordset = array();
	$objeto = new detallesNegociosModel();
	$existe = $objeto->existe_negocio($arreglo_datos["id"]);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		$existe_titulo = $objeto->existe_titulo_otro($arreglo_datos["titulo"],$arreglo_datos["id"]);
		if($existe_titulo[0][0]>0){
			$mensajes["mensajes"] = "existe_titulo";
		}
		else{
			//---
			$recordset = $objeto->modificar_negocios($arreglo_datos);
			//die(json_encode($recordset));
			if($recordset==1){
				$mensajes["mensajes"] = "actualizacion_procesado"; 
			}else{
				$mensajes["error"] = "error";
			}
			//---
		}
	}
	die(json_encode($mensajes));
}
//---
function modificar_estatus($arreglo_datos){
	$recordset = array();
	$objeto = new detallesNegociosModel();
	$existe = $objeto->existe_negocio($arreglo_datos["id"]);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset = $objeto->modificar_negocios_estatus($arreglo_datos["id"],$arreglo_datos["estatus"]);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function consultar_negocios(){
	$recordset = array();
	$mensajes = array();
	$objeto = new detallesNegociosModel();
	$recordset = $objeto->consultar_negocios();
	$i = 0;
	$soportes = "";
	foreach ($recordset as $campo) {
		//$fecha = new DateTime($campo[5]);
		$a = $i+1;
		$descripcion_corta = substr($campo[3],0,600)."...";
		$arreglo_tipo_negocio = array("id"=>$campo[10],"titulo"=>$campo[11],"descripcion"=>$campo[12]);
		$arreglo_idioma = array("id"=>$campo[14],"descripcion"=>$campo[15]);
		$mensajes[] = array("id"=>$campo[0],"titulo"=>$campo[7],"imagen"=>$campo[2],"descripcion1"=>$campo[3],"descripcion2"=>$campo[4],"descripcion3"=>$campo[5],"id_imagen"=>$campo[1],"estatus"=>$campo[6],"number"=>$a,"descripcion_corta"=>$descripcion_corta,"id_tipo_negocio"=>$campo[10],"titulo_negocio"=>$campo[11],"tipo_negocio"=>$arreglo_tipo_negocio,"folleto"=>$campo[13],"idioma"=>$arreglo_idioma,"id_idioma"=>$campo[14]);
		$i++;
	}
	die(json_encode($mensajes));
}
//--