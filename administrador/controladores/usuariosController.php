<?php
require_once("../modelos/usuariosModel.php");
require_once("../modelos/personasModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));
$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar_tipos_usuarios':
			consultar_tipos_usuarios();
			break;
		case 'registrar_usuarios':
			registrar_usuarios($arreglo_datos);
			break;
		case 'consultar_usuarios':
			consultar_usuarios();
			break;
		case 'modificar_usuarios':
			modificar_usuarios($arreglo_datos);
			break;	
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;	
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	if(isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] = "";

	if(isset($data->cedula))
		$user_data["cedula"] = $data->cedula;
	else
		$user_data["cedula"] = "";

	if(isset($data->login))
		$user_data["login"] = $data->login;
	else
		$user_data["login"] = "";

	if(isset($data->nombres_apellidos))
		$user_data["nombres_apellidos"] = $data->nombres_apellidos;
	else
		$user_data["nombres_apellidos"] = "";

	if(isset($data->telefono))
		$user_data["telefono"] = $data->telefono;
	else
		$user_data["telefono"] = "";

	if(isset($data->email))
		$user_data["email"] = $data->email;
	else
		$user_data["email"] = "";

	if(isset($data->estatus))
		$user_data["estatus"] = $data->estatus;
	else
		$user_data["estatus"] = "";

	if(isset($data->clave))
		$user_data["clave"] = $data->clave;
	else
		$user_data["clave"] = "";

	if(isset($data->tipos_usuarios))
		$user_data["tipos_usuarios"] = $data->tipos_usuarios;
	else
		$user_data["tipos_usuarios"] = "";

	if(isset($data->id_persona))
		$user_data["id_personas"] = $data->id_persona;
	else
		$user_data["id_personas"] = "";
	return $user_data;
}
//-Metodo para consultar tipos de usuarios
function consultar_tipos_usuarios(){
	$recordset = array();
	$mensajes = array();
	$objeto = new usuariosModel();
	$recordset = $objeto->consultar_tipos_usuarios();
	$i = 0;
	foreach ($recordset as $campo) {
		$a = $i+1;
		$mensajes[] = array("id"=>$campo[0],"descripcion"=>$campo[1],"number"=>$a);
		$i++;
	}
	echo(json_encode($mensajes));
}

//-Metodo para registrar usuarios
function registrar_usuarios($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new usuariosModel();
	$objeto_personas = new personasModel();
	//Verifico si existe un usuario con esa cedula....
	
	$existe = $objeto->consultar_existe_usuario($arreglo_datos["cedula"],'0',$arreglo_datos["login"]);
	if($existe[0][0]>0){
		//Existe
		$mensajes["mensajes"] = "existe";
	}else{
		//No existe 
		//Verifico si existe en personas
		$existe_personas = $objeto_personas->consultar_existe_persona($arreglo_datos["cedula"],'0');
		if($existe_personas[0][0]>0){
			$mensajes["mensajes"] = "existe_persona";
		}else{
			//Registro persona
			$recordset_personas = $objeto_personas->registrar_persona_datos($arreglo_datos);
			if($recordset_personas==1){
				//------------------------
				$id_persona = $objeto_personas->maximo_id_personas();
				//Registro dr
				$recordset_us = $objeto->registrar_usuario($arreglo_datos,$id_persona);
				if($recordset_us==1){
					$mensajes["mensajes"] = "registro_procesado";
					$mensajes["id_persona"] = $id_persona[0][0];
					$id_us = $objeto->maximo_id_usuarios();
					$mensajes["id"] = $id_us[0][0];
				}else{
					$mensajes["error"] = "error";
				}
				//------------------------
			}else{
				$mensajes["error"] = "error_registro";
			}
		}
	}
	die(json_encode($mensajes));
}
//-------------------------------------------------------------
function modificar_usuarios($arreglo_datos){
	$recordset = array();
	//die("Tipos usuarios:"+$arreglo_datos["tipos_usuarios"]);
	$objeto = new usuariosModel();
	$objeto_personas = new personasModel();
	//Verifico si existe un dr con esa cedula y esos id
	$existe = $objeto->consultar_existe_usuario($arreglo_datos["cedula"],$arreglo_datos["id"],$arreglo_datos["login"]);
	if($existe[0][0]==0){
		$mensajes["mensajes"] = "no_existe"; 
	}else{
		//-------------------------------------------------------------------------
		//verifico que existe login
		$existe_login = $objeto->consultar_existe_login($arreglo_datos["login"],$arreglo_datos["id"]);
		//Si existe el login
		if($existe_login[0][0]>0){
			$mensajes["mensajes"] = "existe_login";
			die(json_encode($mensajes)); 
		}
		//-------------------------------------------------------------------------
		$existe_persona = $objeto_personas->consultar_existe_persona($arreglo_datos["cedula"],$arreglo_datos["id"]);
		//Sino existe
		if($existe_persona[0][0]==0){
			$mensajes["mensajes"] = "no_existe_persona"; 
		}else{//si existe....
			//Modifico persona
			$recordset_us = $objeto->modificar_usuario($arreglo_datos);
			//Modifico dr
			$recordset_personas = $objeto_personas->modificar_personas_datos($arreglo_datos);
			//die($recordset_personas."-".$recordset_us);
			if(($recordset_personas==1)&&($recordset_us==1)){
				$mensajes["mensajes"] = "modificacion_procesada";
			}else{
				$mensajes["error"] = "error";
			}
		}	
	}
	die(json_encode($mensajes));	
}
//----------------------------------
function modificar_estatus($arreglo_datos){
	$recordset = array();
	$objeto_personas = new personasModel();
	$existe_persona = $objeto_personas->consultar_existe_persona2($arreglo_datos["id_personas"]);
	if($existe_persona[0][0]==0){
		$mensajes["mensajes"] = "no_existe_persona"; 
	}else{//si existe....
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset_personas = $objeto_personas->modificar_personas_estatus($arreglo_datos["id_personas"],$arreglo_datos["estatus"]);
		if($recordset_personas==1){
			$mensajes["mensajes"] = "modificacion_procesada";
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}	
///------------------------------------------------------------
//-Metodo para consultar usuarios
function consultar_usuarios(){
	$recordset = array();
	$mensajes = array();
	$objeto = new usuariosModel();
	$recordset = $objeto->consultar_usuarios();
	$i = 0;
	foreach ($recordset as $campo) {
		$a = $i+1;
		$mensajes[] = array(
							"id"=>$campo[0],
							"cedula"=>$campo[2],
							"login"=>$campo[8],
							"nombres_apellidos"=>$campo[3],
							"telefono"=>$campo[9],
							"email"=>$campo[5],
							"tipos_usuarios"=>array("id"=>$campo[7],"descripcion"=>$campo[4]),
							"clave"=>"",
							"id_persona"=>$campo[1],
							"estatus"=>$campo[6],
							"desc_tipo_usuario"=>$campo[4],
							"number"=>$a);
		$i++;
	}
	die(json_encode($mensajes));
}
//-------------------------------------------------------------