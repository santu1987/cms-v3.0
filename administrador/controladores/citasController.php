<?php
require_once("../modelos/doctoresModel.php");
require_once("../modelos/personasModel.php");
require_once("../modelos/correoModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));
$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar_citas_agendar':
			consultar_citas_agendar($arreglo_datos);
			break;
		case 'registrar_cita':
			registrar_cita($arreglo_datos);
			break;
		case 'anular_cita':
			anular_cita($arreglo_datos);
			break;
		case 'consultar_citas_programadas':
			consultar_citas_programadas($arreglo_datos["estatus"],$arreglo_datos["id_especialidad"],$arreglo_datos["id_doctor"]);
			break;	
		case 'limpiar_cita':
			limpiar_cita($arreglo_datos);
			break;				
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	$user_data["id_doctor"] = $data->id_doctor;
	$user_data["id"] = $data->id;
	$user_data["nombres"] = $data->nombres;
	$user_data["hora"] = $data->hora;
	$user_data["fecha"] = $data->fecha;
	$user_data["estatus"] = $data->estatus;
	$user_data["id_especialidad"] = $data->id_especialidad;
	$user_data["vector_id"] = $data->vector_id;
	$user_data["nombre_doctor"] = $data->nombre_doctor;
	$user_data["nombre_especialidad"] = $data->nombre_especialidad;
	return $user_data;
}
//---
function consultar_citas_agendar($arreglo_datos){
	$recordset = array();
	$mensajes = array();
	$objeto = new doctoresModel();
	$recordset = $objeto->consultar_citas_doctores($arreglo_datos["id_doctor"]);
	$i = 0;
	$soportes = "";
	foreach ($recordset as $campo) {
		$a = $i+1;
		$x = 0;
		$vector_fecha = explode("-", $campo[6]);
		$vector_fecha2 = explode("-", $campo[5]);
		$fecha_solicitud = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
		$fecha_agendada = $vector_fecha2[2]."/".$vector_fecha2[1]."/".$vector_fecha2[0];
		$mensajes[] = array("id"=>$campo[0],"id_personas"=>$campo[1],"nombres_persona"=>$campo[2],"observacion"=>$campo[3],"estatus"=>$campo[4],"fecha"=>$fecha_agendada,"fecha_solicitud"=>$fecha_solicitud,"hora_agendada"=>$campo[7],"number"=>$a);
		$i++;
	}
	echo(json_encode($mensajes));
}
//--
function consultar_citas_programadas($estatus,$id_especialidad, $id_doctor){
	$recordset = array();
	$mensajes = array();
	$objeto = new doctoresModel();
	$recordset = $objeto->consultar_citas_programadas($estatus,$id_especialidad, $id_doctor);
	$i = 0;
	foreach ($recordset as $campo) {
		$vector_hora = explode(" ",$campo["super_fecha"]);
		$hora = $vector_hora[1]." ".$vector_hora[2];
		$titulo = $hora."|#".$campo["id"]."|".$campo["nombre_persona"];
		$start = $campo["fecha"];
		$eventos[] = array("title"=>$titulo,"start"=>$start,"id"=>$campo["id"]);
	}
	echo(json_encode($eventos));
}
//--
function registrar_cita($arreglo_datos){
	//-----------------------------------------
	$recordset = array();
	$objeto = new doctoresModel();
	$objeto_persona = new personasModel();
	//Verifico si existe un dr con esa cedula....
	$existe = $objeto->consultar_existe_cita($arreglo_datos["id"]);
	if($existe[0][0]>0){
		//Verifico si existe una cita en ese horario
		$existe_horario = $objeto->consultar_existe_cita_horario($arreglo_datos["fecha"],$arreglo_datos["hora"]);
		//die($existe_horario);
		if($existe_horario[0][0]>0){
			$mensajes["mensajes"] = "existe_horario";
		}else{
			$recordset = $objeto->registrar_cita($arreglo_datos);
			if($recordset==1){
				$mensajes["mensajes"] = "registro_procesado";
				#consulto el correo del usuario
				$recordset_us = $objeto_persona->consultar_correo($arreglo_datos["id"]);
				enviar_correo($arreglo_datos,$recordset_us[0][0]);
			}else{
				$mensajes["error"] = "error";
			}
		}	
	}else{
		$mensajes["mensajes"] = "no_existe";
	}
	//------------------------------------------
	die(json_encode($mensajes));
}
//--------------------------------------------------------
function enviar_correo($arreglo_datos,$correo_destinatario){
	$correo_sistema = "drmv@tumedico-online.com";//aquí va el correo del sistema
	$correo_destino = $correo_destinatario;
	$titulo = "Datos de cita";
	$encabezado = $arreglo_datos['nombres']." Su cita fue agendada a través de www.tumedico-online.com:<br>";
	$result = enviar_correo_contacto($correo_sistema,$correo_destino,$titulo,$encabezado,$arreglo_datos);
	if($result==1){
		$arreglo_retorno["mensajes"] = "registro_procesado";
	}
	else{
		$arreglo_datos["mensajes"] = "error_correo";
		$arreglo_datos["error_correo"] = $result;	
	}
	die(json_encode($arreglo_retorno));
}
//--
function enviar_correo_contacto($correo_sistema,$correo_destino,$titulo,$encabezado,$arreglo_datos){
	$obj = new correoModel();
	$obj2 = new doctoresModel();
	#Consulto los datos de la cita
	$recordset_citas = $obj2->consultar_datos_cita($arreglo_datos["id"]);
	$mensaje = $obj->crear_cuadro($titulo,$encabezado,$arreglo_datos,$recordset_citas[0][3]);
	$resp = $obj->enviar_correo($correo_sistema,$correo_destino,$mensaje,$titulo);
	return $resp;
} 
//--------------------------------------------------------
//--
function anular_cita($arreglo_datos){
	$recordset = array();
	$objeto = new doctoresModel();
	//Verifico si existe un dr con esa cedula....
	foreach ($arreglo_datos["vector_id"] as $id) {
	//-------------------------------------------------
		$existe = $objeto->consultar_existe_cita($id);
		if($existe[0][0]>0){
			$recordset = $objeto->anular_cita($id);
			if($recordset==1){
				$mensajes["mensajes"] = "anulacion_procesada";
			}else{
				$mensajes["error"] = "error";
			}
		}else{
			$mensajes["mensajes"] = "no_existe";
		}
	//----------	
	}
	//------------------------------------------
	die(json_encode($mensajes));
}
//------------------------------------------------------
function limpiar_cita($arreglo_datos){
	$recordset = array();
	$objeto = new doctoresModel();
	//-----------------------------
	$existe = $objeto->consultar_existe_cita($arreglo_datos["id"]);
	if($existe[0][0]>0){
		$recordset = $objeto->limpiar_citas($arreglo_datos["id"]);
		if($recordset==1){
			$mensajes["mensajes"] = "limpieza_procesada";
		}else{
			$mensajes["error"] = "error";
		}
	}else{
		$mensajes["mensajes"] = "no_existe";
	}
	//-----------------------------
	die(json_encode($mensajes));
}
//------------------------------------------------------
