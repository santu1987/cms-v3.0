<?php
require_once("../modelos/mensajeModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_mensaje':
			registrar_mensaje($arreglo_datos);
			break;
		case 'actualizar_mensaje':
			actualizar_mensaje($arreglo_datos);
			break;
		case 'consultar_mensajes':
			consultar_mensajes($arreglo_datos);
			break;	
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;			 			
	}
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	$user_data["titulo"] = $data->titulo;
	$user_data["descripcion"] = $data->descripcion;
	$user_data["estatus"] = $data->estatus;
	$user_data["id"] = $data->id;
	$user_data["id_mensajes"] = $data->id_mensajes;
	return $user_data;
}
//------------------------------------------------------
function registrar_mensaje($arreglo_datos){
	//------------------------------------
	$mensajes = [];
	$recordset = array();
	$objeto = new mensajeModel();
	$data = $objeto->registrar_mensaje($arreglo_datos);
	$recordset = explode("*",$data);
	//die(json_encode($recordset));
	$i = 0;
	if($recordset[1]==true){
		$mensajes["mensajes"] = "proceso exitoso";
		$mensajes["id"] = $recordset[1];
	}else{
		$mensajes["mensajes"] = "error";
	}
	die(json_encode($mensajes));
	//----------------------------------
}
//-------------------------------------------------------
function actualizar_mensaje($arreglo_datos){
	$mensajes = [];
	$recordset = array();
	$objeto = new mensajeModel();
	$data = $objeto->actualizar_mensaje($arreglo_datos);
	$recordset = explode("*",$data);
	//die(json_encode($recordset));
	$i = 0;
	if($recordset[1]==true){
		$mensajes["mensajes"] = "actualizacion exitosa";
		$mensajes["id"] = $recordset[1];
	}else{
		$mensajes["mensajes"] = "error";
	}
	die(json_encode($mensajes));
	//----------------------------------

}
//----------------------------------
function modificar_estatus($arreglo_datos){
	$recordset = array();
	$objeto_mensaje = new mensajeModel();
	$existe_mensaje = $objeto_mensaje->consultar_existe_mensaje($arreglo_datos["id_mensajes"]);
	if($existe_mensaje[0][0]==0){
		$mensajes["mensajes"] = "no_existe_mensaje"; 
	}else{//si existe....
		if($arreglo_datos["estatus"]==0){
			$estatus = 1;
		}else{
			$estatus = 0;
		}
		$recordset_mensajes = $objeto_mensaje->modificar_mensajes_estatus($arreglo_datos["id_mensajes"],$estatus);
		die($recordset_mensajes);
		if($recordset_mensajes==1){
			$mensajes["mensajes"] = "modificacion_procesada";
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}	
//------------------------------------------------------
function consultar_mensajes($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$mensajes = array();
	$objeto = new mensajeModel();
	$recordset = $objeto->consultar_mensajes();
	//die(json_encode($recordset));
	$i = 0;
	foreach ($recordset as $campo) {
		$a = $i+1;
		$mensajes[] = array("id"=>$campo[0], "titulo"=>$campo[1], "descripcion"=>$campo[2], "estatus"=>$campo[3],"number"=>$a);
		$i++;
	}
	echo(json_encode($mensajes));
}
//-------------------------------------------------------
?>