<?php
require_once("../modelos/direccionModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_direccion':
			registrar_direccion($arreglo_datos);
			break;
		case 'consultar_direccion':
			consultar_direccion();
			break;	
		case 'modificar_direccion':
			modificar_direccion($arreglo_datos);
			break;		
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;				
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	
	if(isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] = "";

	if (isset($data->descripcion)){
		$user_data["descripcion"] = $data->descripcion;	
	}else{
		$user_data["descripcion"] = "";
	}	
	

	if(isset($data->telefono))	
		$user_data["telefono"] = $data->telefono;
	else
		$user_data["telefono"] = "";
	
	if(isset($data->estatus))
		$user_data["estatus"] = $data->estatus;
	else
		$user_data["estatus"] = "";

	return $user_data;
}
//------------------------------------------------------
function registrar_direccion($arreglo_datos){
	//var_dump($arreglo_datos);
	//------------------------------------
	$recordset = array();
	$telefono_vector = array();
	$objeto = new direccionModel();
	$recordset = $objeto->registrar_direccion($arreglo_datos);
	if($recordset==1){
		$mensajes["mensajes"] = "registro_procesado";
		$id_direccion = $objeto->maximo_id_direccion();
		$mensajes["id"] = $id_direccion[0][0];
		$id_direccion = $id_direccion[0][0];
		//----------------------------------------------------
		//Bloque para registrar telefonos
		$telefono_vector = $arreglo_datos["telefono"];
		foreach ($telefono_vector as $value) {
			//--
			$existe = $objeto->existe_telefono_direccion($id_direccion,$value);
			if($existe[0][0]==0){
				$recordset_telefono = $objeto->registrar_telefono($id_direccion,$value);
			}
			//--
		}
		//----------------------------------------------------
		die(json_encode($mensajes));
	}else{
		$recordset[0][0]="error";
		die(json_encode($recordset));
	}
	//die(json_encode($recordset));	
}
//-------------------------------------------------------
function modificar_direccion($arreglo_datos){
	//var_dump($arreglo_datos);
	//------------------------------------
	$recordset = array();
	$telefono_vector = array();
	$objeto = new direccionModel();
	$existe = $objeto->existe_direccion($arreglo_datos["id"]);
	if($existe[0][0]>0){
		//---
		$recordset = $objeto->modificar_direccion($arreglo_datos);
		if($recordset==1){
			//Elimino los telefonos
			$id_direccion = $arreglo_datos["id"];
			$recordset_limpiar = $objeto->eliminar_telefonos($id_direccion);
			//Bloque para registrar telefonos
			$telefono_vector = $arreglo_datos["telefono"];
			foreach ($telefono_vector as $value) {
				//--
				$existe = $objeto->existe_telefono_direccion($id_direccion,$value);
				if($existe[0][0]==0){
					$recordset_telefono = $objeto->registrar_telefono($id_direccion,$value);
				}
				//--
			}
			$mensajes["mensajes"] = "modificacion_procesada";
		}else{
			$mensajes["mensajes"] = "error";
		}	
		//---
	}else{
		$mensajes["mensajes"] = "no_existe";
	}
	die(json_encode($mensajes));
	//die(json_encode($recordset));	
}
//-------------------------------------------------------
function consultar_direccion(){
	$recordset = array();
	$mensajes = array();
	$vector_telefonos = array();
	$objeto = new direccionModel();
	$recordset = $objeto->consultar_direcciones();
	$i = 0;
	$soportes = "";
	foreach ($recordset as $campo) {
		//$fecha = new DateTime($campo[5]);
		$a = $i+1;
		$vector_telefonos = $objeto->consultar_telefonos($campo[0]);
		$mensajes[] = array("id"=>$campo[0],"descripcion"=>$campo[1],"telefono"=>$vector_telefonos, "estatus"=>$campo[2],"number"=>$a);
		$i++;
	}
	die(json_encode($mensajes));
}
//-------------------------------------------------------
function modificar_estatus($arreglo_datos){
	//var_dump($arreglo_datos);
	$recordset = array();
	$objeto = new direccionModel();
	$existe = $objeto->existe_direccion($arreglo_datos["id"]);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset = $objeto->modificar_direccion_estatus($arreglo_datos["id"],$arreglo_datos["estatus"]);
		//die($recordset);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}