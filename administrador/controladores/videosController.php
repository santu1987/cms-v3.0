<?php
require_once("../modelos/videosModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_videos':
			registrar_videos($arreglo_datos);
			break;
		case 'consultar_galeria_videos':
			consultar_galeria_videos();
			break;
		case 'modificar_videos':
			modificar_videos($arreglo_datos);
			break;
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);							 			
	}
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	
	if(isset($data->titulo))
		$user_data["titulo"] = $data->titulo;
	else	
		$user_data["titulo"] = "";
	
	if(isset($data->link))
		$user_data["link"] = $data->link;
	else
		$user_data["link"] = "";

	if(isset($data->estatus))
		$user_data["estatus"] = $data->estatus;
	else
		$user_data["estatus"] =  "";
	if(isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] ="";
	if(isset($data->id_idioma))
		$user_data["id_idioma"] = $data->id_idioma;
	else
		$user_data["id_idioma"] = "";
	return $user_data;
}
//--
function registrar_videos($arreglo_datos){
	//------------------------------------
	$mensajes = [];
	$recordset = array();
	$objeto = new videosModel();
	#Verifico si existe un video con ese link
	$existe = $objeto->existe_video($arreglo_datos["link"]);
	//--
	if($existe[0][0]>0){
		$mensajes["mensajes"] = "existe_video";
	}else{
		$recordset = $objeto->registrar_videos($arreglo_datos);
		//die($recordset);
		if($recordset==1){
			$mensajes["mensajes"] = "registro_procesado";
			$id_video = $objeto->maximo_id_video();
			$mensajes["id"] = $id_video[0][0];
		}else{
			$mensajes["mensajes"] = "error";
		}
	}
	//--
	die(json_encode($mensajes));
	//----------------------------------
}
//--
function modificar_videos($arreglo_datos){
	//-------------------------------------
	$mensajes = [];
	$recordset = array();
	$objeto = new videosModel();
	//-Verifico si existe un video con ese link
	$existe = $objeto->existe_video2($arreglo_datos["id"]);
	if($existe[0][0]==0){
		$mensajes["mensajes"] = "no_existe_video";
	}else{
		//-Verifico que el link no lo tenga otro registro
		$existe_link = $objeto->existe_video_link($arreglo_datos["id"],$arreglo_datos["link"]);
		if($existe_link[0][0]>0){
			$mensajes["mensajes"] = "existe_link_otro_video";
		}else{
			//--
			$recordset_videos = $objeto->modificar_videos($arreglo_datos);
			if($recordset_videos==1){
				$mensajes["mensajes"] = "modificacion_procesada";
			}else{
				$mensajes["error"] = "error";
			}
			//--
		}
	}
	die(json_encode($mensajes));
	//-------------------------------------
}
//--
function modificar_estatus($arreglo_datos){
	$recordset = array();
	$objeto = new videosModel();
	$existe_video = $objeto->consultar_existe_video_id($arreglo_datos["id"]);
	if($existe_video[0][0]==0){
		$mensajes["mensajes"] = "no_existe_video"; 
	}else{//si existe....
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset_personas = $objeto->modificar_videos_estatus($arreglo_datos["id"],$arreglo_datos["estatus"]);
		if($recordset_personas==1){
			$mensajes["mensajes"] = "modificacion_procesada";
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//--
function consultar_galeria_videos(){
	$recordset = array();
	$mensajes = array();
	$objeto = new videosModel();
	$recordset = $objeto->consultar_galeria_videos();
	$i = 0;
	foreach ($recordset as $campo) {
		$a = $i+1;
		$arreglo_idioma = array("id"=>$campo[4],"descripcion"=>$campo[5]);

		$mensajes[] = array("id"=>$campo[0],"titulo"=>$campo[1],"link"=>$campo[2],"estatus"=>$campo[3],"number"=>$a,"id_idioma"=>$campo[4],"idioma"=>$arreglo_idioma);
		$i++;
	}
	die(json_encode($mensajes));
}
//------------------------------------------