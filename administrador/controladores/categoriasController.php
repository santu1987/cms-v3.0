<?php
require_once("../modelos/categoriasModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar':
			consultar($arreglo_datos);
			break;	 			
	}
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	return $user_data;
}
//------------------------------------------------------
function consultar($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new categoriasModel();
	$recordset = $objeto->consultar_categorias("");
	//die(json_encode($recordset));
	$i = 0;
	foreach ($recordset as $campo) {
		$categorias[] = array("id"=>$campo[0],"nombre_categoria"=>$campo[1]);
		$i++;
	}
	echo(json_encode($categorias));
	//----------------------------------
}
?>