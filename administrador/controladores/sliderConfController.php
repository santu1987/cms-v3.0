<?php
require_once("../modelos/sliderConfModel.php");
require_once("../vistas_logicas/sliderView.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));
//var_dump($data->slider->imagen);
$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_slider':
			registrar_slider($arreglo_datos);
			break;
		case 'cargar_lista_slider':
			cargar_lista_slider($arreglo_datos);
			break;	
		case 'modificar_slider':
			modificar_slider($arreglo_datos);
			break;						 			
	}
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	if(isset($data->slider->id))
		$user_data["id"] = $data->slider->id;
	else
		$user_data["id"] = "";

	if(isset($data->slider->titulo))
		$user_data["titulo"] = $data->slider->titulo;
	else
		$user_data["titulo"] = "";

	if(isset($data->slider->descripcion))
		$user_data["descripcion"] = $data->slider->descripcion;
	else
		$user_data["descripcion"] = "";

	if(isset($data->slider->imagen))
		$user_data["imagen"] = $data->slider->imagen;
	else
		$user_data["imagen"] ="";

	if(isset($data->idioma))
		$user_data["idioma"] = $data->idioma;
	else
		$user_data["idioma"] ="";
	return $user_data;
}
//---
function registrar_slider($arreglo_datos){
	$recordset = array();
	$objeto = new sliderConfModel();
	$recordset = $objeto->registrar_slider($arreglo_datos);
	if($recordset==1){
		cargar_lista_slider($arreglo_datos);
	}else{
		$mensaje["mensajes"]="error";
		die(json_encode($mensaje));
	}
	//die(json_encode($recordset));	
}
//---
function cargar_lista_slider($arreglo_datos){
	$recordset = array();
	$mensaje = array();
	$idioma = $arreglo_datos["idioma"];
	$arreglo_datos = array();
	$obj = new sliderConfModel();
	$recordset = $obj->consultar_slider_lista($idioma);
	//--Si no hay slider para este idioma...
	if($recordset[0][0]==""){
		$mensaje["mensajes"] = "0";
		die(json_encode($mensaje));
	}
	//--En caso de error
	if($recordset!="error"){
		render_vista_consulta("lista_slider",$recordset);
	}else{
		$mensaje = "error";
		die(json_encode($mensaje));
	}
	//--
}
//---
function modificar_slider($arreglo_datos){
	$recordset = array();
	$objeto = new sliderConfModel();
	//Elimino los registros actuales
	$record_delete = $objeto->eliminar_slider($arreglo_datos["idioma"]);
	//Agrego nuevamente los registros que se encuentren en el formulario
	$recordset = $objeto->registrar_slider($arreglo_datos);
	if($recordset==1){
		cargar_lista_slider($arreglo_datos);
	}else{
		$mensaje="error";
		die(json_encode($mensaje));
	}
}
//---