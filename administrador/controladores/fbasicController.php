<?php
session_start();
require_once("../core/fbasic.php");
require_once("../modelos/inicioModel.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {

		case 'consultar_sesion':
			consultar_sesion();
			break;	
		case 'cerrar_sesion':
			cerrar_sesion();
			break;
		case 'datos_sesion':
			datos_sesion();
			break;	 		 			
	}
}
//---
function helper_userdata($data){
	
	$user_data = array();
	$user_data["accion"] = $data->accion;
	//$user_data["login"] = $data->login;
	//$user_data["clave"] = $data->clave;
	return $user_data;
}

//--------------------------------------------------------
function datos_sesion(){
	$mensajes["nombre_usuario"] = $_SESSION['id'];
	$mensajes["login_usuario"] =  $_SESSION['login'];
	die(json_encode($mensajes));
}
//--------------------------------------------------------
function consultar_sesion(){
	if(isset($_SESSION["id"])){
		$mensajes["respuesta"] = "activo";
	}else{
		$mensajes["respuesta"] = "cerrado";
	}
	die(json_encode($mensajes));
}
//---------------------------------------------------------
function cerrar_sesion(){
	if(isset($_SESSION["id"])){
		session_destroy();
	}
	$mensajes["respuesta"] = "cerrado";
	die(json_encode($mensajes));
}
//--------------------------------------------------------