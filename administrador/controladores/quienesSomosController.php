<?php
require_once("../modelos/quienesSomosModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar':
			consultar($arreglo_datos);
			break;
		case 'consultar_quienes_somos':
			consultar_quienes_somos();
			break;	
		case 'registrar_quienes_somos':
			registrar_quienes_somos_form($arreglo_datos);
			break;	
		case 'modificar_quienes_somos':
			modificar_quienes_somos($arreglo_datos);
			break;	
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;
		case 'consultar_idioma':
			consultar_idioma();
			break;					 			
	}
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	if(isset($data->id))
		$user_data["id"] = $data->id;
	if(isset($data->quienes_somos))
		$user_data["quienes_somos"] = $data->quienes_somos;
	if(isset($data->mision))
		$user_data["mision"] = $data->mision;
	if(isset($data->vision))
		$user_data["vision"] = $data->vision;
	if(isset($data->objetivo))
		$user_data["objetivo"] = $data->objetivo;
	if(isset($data->id_imagen))
		$user_data["id_imagen"] = $data->id_imagen;
	if(isset($data->id_idioma))
		$user_data["id_idioma"] = $data->id_idioma;
	if(isset($data->estatus))
		$user_data["estatus"] = $data->estatus;
	return $user_data;
}
//---
//------------------------------------------------------
function registrar_quienes_somos_form($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new quienesSomosModel();
	//Verifico si existe
	if($arreglo_datos["id"]!=""){
		$mensajes["mensajes"] = "existe";
	}else{
			//no existe, guardo
			$recordset = $objeto->registrar_quienes_somos($arreglo_datos);
			if($recordset==1){
				$mensajes["mensajes"] = "registro_procesado";
				$id_quienes_somos = $objeto->maximo_id_quienes_somos();
				$mensajes["id"] = $id_quienes_somos[0][0];
			}else{
				$mensajes["mensajes"] = "error";
			}
	}
	die(json_encode($mensajes));
	//--
}	
//---
function modificar_quienes_somos($arreglo_datos){
	$recordset = array();
	$objeto = new quienesSomosModel();
	$existe = $objeto->existe_empresa($arreglo_datos["id"]);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		$recordset = $objeto->modificar_quienes_somos($arreglo_datos);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function modificar_estatus($arreglo_datos){
	$recordset = array();
	$objeto = new quienesSomosModel();
	$existe = $objeto->existe_empresa($arreglo_datos["id"]);
	//Verifico si existe la empresa
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset = $objeto->modificar_quienes_somos_estatus($arreglo_datos["id"],$arreglo_datos["estatus"]);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function consultar_quienes_somos(){
	$recordset = array();
	$mensajes = array();
	$objeto = new quienesSomosModel();
	$recordset = $objeto->consultar_quienes_somos();
	$i = 0;
	foreach ($recordset as $campo) {
		$a = $i+1;
		$arreglo_idioma = array("id"=>$campo[7],"descripcion"=>$campo[8]);
		$mensajes[] = array("id"=>$campo[0],"quienes_somos"=>$campo[1],"mision"=>$campo[2],"vision"=>$campo[3],"objetivo"=>$campo[4],"id_imagen"=>$campo[5],"imagen"=>$campo[6],"mensaje_contacto"=>"","id_idioma"=>$campo[7],"idioma"=>$arreglo_idioma,"quienes_somos_sh"=>strip_tags($campo[1]),"mision_sh"=>strip_tags($campo[2]),"vision_sh"=>strip_tags($campo[3]),"objetivo_sh"=>strip_tags($campo[4]),"estatus"=>$campo[9],"number"=>$a);
		$i++;
	}
	die(json_encode($mensajes));
}
//--
function consultar($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new especialidadesModel();
	$recordset = $objeto->consultar_especialidades();
	//die(json_encode($recordset));
	$i = 0;
	foreach ($recordset as $campo) {
		$mayusculas = normaliza(strtoupper($campo[1]));
		$minusculas = normaliza (strtolower($campo[1]));
		$especialidades[] = array("id"=>$campo[0],"titulo"=>$campo[1], "descripcion"=>$campo[2],"imagen"=>$campo[3],"number"=>$i, "selected"=>false, "mayusculas"=>$mayusculas, "minusculas"=>$minusculas, "cuantos"=>$campo[4]);
		$i++;
	}
	echo(json_encode($especialidades));
	//----------------------------------
}
//--
function consultar_idioma(){
//------------------------------------------
	$recordset = array();
	$objeto = new quienesSomosModel();
	$recordset = $objeto->consultar_idioma();
	die(json_encode($recordset));
	$i = 0;
	foreach ($recordset as $campo) {
		$a = $i +1;
		$mensajes[] = array("id"=>$campo[0],"descripcion"=>$campo[1],"number"=>$a);
		$i++;
	}
	die(json_encode($mensajes));
//------------------------------------------	
}
//--
?>