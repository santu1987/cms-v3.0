<?php
session_start();
require_once("../core/fbasic.php");
require_once("../modelos/inicioModel.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'iniciar_sesion':
			iniciar_sesion($arreglo_datos);
			break;
		case 'datos_usuario':
			obtener_datos_usuarios();
			break;	
	}
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	$user_data["login"] = $data->login;
	$user_data["clave"] = $data->clave;
	return $user_data;
}
//------------------------------------------------------
function iniciar_sesion($arreglo_datos){
	$mensajes = array();
	$objeto = new inicioModel;
	$recordset = $objeto->iniciar_sesion($arreglo_datos["login"],$arreglo_datos["clave"]);
	if($recordset!="error"){
		if($recordset[0][0]==1){
			$mensajes["mensajes"] = "inicio con exito";
			$arreglo_us = $objeto->obtener_datos_us($arreglo_datos["login"]);
			$_SESSION['login'] = $arreglo_datos['login'];
			//$_SESSION['tipo_usuario'] = $arreglo_us[0][2];
			$_SESSION['activo'] = true;
			$_SESSION['id'] = $arreglo_us[0][0];
			$_SESSION['nombres_persona'] = $arreglo_us[0][3];
		}else{
			$mensajes["mensajes"] = "error datos";
		}
		
	}else{
		$mensajes["mensajes"] = "error";
	}
	die(json_encode($mensajes));

}
//--------------------------------------------------------
function obtener_datos_usuarios(){
	$recordset = array();
	$recordset['login']	= $_SESSION['login'];
	$recordset['tipo_usuario'] = $_SESSION['tipo_usuario'];
	$recordset['activo'] = $_SESSION['activo'] = true;
	$recordset['id'] = $_SESSION['id'];
	$recordset['nombres_persona'] =	$_SESSION['nombres_persona'];	
	if(isset($_SESSION["id"])){
		die(json_encode($recordset));
	}else{
		cerrar_session();
	}	
}
//--------------------------------------------------------

//--------------------------------------------------------
/*function consultar_sesion(){
	if(isset($_SESSION["id"])){
		$mensajes["respuesta"] = "activo";
	}else{
		$mensajes["respuesta"] = "cerrado";
	}
	die(json_encode($mensajes));
}*/
//--------------------------------------------------------