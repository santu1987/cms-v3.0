<?php
//conexion de bd -----CMS PRUEBA
abstract class Conex
{
		private $conexion;
		private static $servidor="localhost";
		private static $clave="123456";
		private static $usuario="root";
		private $bd="bd_cmsv30";
		//Servidor...
		/*private static $clave="123456";
		private static $usuario="id3796189_root";
		private $bd="id3796189_bd_cmsv30";*/
		//--
		protected $query;
		public $arreglo = array();
		//-- Metodo constructor*/
		public function __construct()
		{
			$this->query="";
		}
		//-- Metodo que permite conectarse a la bd
		private function conectar()
		{
			//valido la sesion antes de conectar
			$this->conexion = mysqli_connect(self::$servidor,self::$usuario,self::$clave,$this->bd);
			mysqli_set_charset($this->conexion,'utf8');
			if($this->conexion)
			{
				return 'SI';
			}
			else
			{
				return 'NO';
			}	
		}
		//--Metodo que cierra la conexion a la bd
		private function desconectar()
		{

		}
		//
		//-- Metodo que permite ejecutar un query
		protected function enviarQuery($sql)
		{
			$mysqli  = $this->conexion;
			$this->query = $mysqli->query($sql);
			return $this->query;
		}
		//-- Calcula cuantos registros tiene la consulta
		protected function cuantos_registros($resultado)
		{
			return $resultado->num_rows;
		}
		//-- Vectoriza el resultado de una consulta
		protected function vectorizar($result)
		{
			return mysqli_fetch_array ($result, MYSQL_BOTH);
		}
		//--Para Consultas
		protected function execute($sql)
		{
			$result = $this->enviarQuery($sql);
			if($result){
				$arr = array();
				while($row = $this->vectorizar($result)){
					$arr[] = $row;
				}
			}else{
				$arr = "error";
			}
			return $arr;
		}
		//--Para procesar consultas
		protected function procesarQuery($sql)
		{
			$this->conectar();
			$rs = $this->execute($sql);
			return $rs;
		}
		//--Para procesar insert, updare, delete
		protected function procesarQuery2($sql)
		{
			$this->conectar();
			$rs = $this->enviarQuery($sql);
			mysqli_close($this->conexion);
			return $rs;
		}
}
?>