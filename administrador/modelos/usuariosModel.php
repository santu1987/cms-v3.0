<?php
require_once("../core/conex.php");
require_once("../core/fbasic.php");
class usuariosModel extends Conex{
	private $rs;
	private $rs2;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Consulta los datos de los usuarios
	public function consultar_existe_usuario($cedula,$id,$login){
		$where = "WHERE 1=1 ";
		if($cedula!="0"){
			$where.=  " AND b.cedula='".$cedula."'";
		}
		if($id!="0"){
			$where.=  " AND a.id='".$id."'";
		}
		if($login!=""){
			$where.=  " OR a.login='".$login."'";
		}
		$sql = "SELECT count(a.id) FROM usuarios a INNER JOIN personas b ON a.id_persona=b.id  ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--
	public function consultar_existe_login($login,$id){
		$where = "WHERE 1=1 ";

		if($id!="0"){
			$where.=  " AND a.id!='".$id."'";
		}
		if($login!=""){
			$where.=  " AND a.login='".$login."'";
		}
		$sql = "SELECT count(a.id) FROM usuarios a INNER JOIN personas b ON a.id_persona=b.id  ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Metodo para registrar usuario
	public function registrar_usuario($datos,$id_persona){
		$sql="INSERT INTO usuarios
						(
							id_persona,
							id_tipo_usuario,
							estatus,
							login,
							clave
						) 
			   VALUES (
			   			'".$id_persona[0][0]."',
			   			'".$datos["tipos_usuarios"]."',
			   			'1',
			   			'".$datos["login"]."',
			   			'".sha1($datos["clave"])."'
			   );";
		// Ejecuto el primer query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//--Metodo para consultar tipos de usuarios
	public function consultar_tipos_usuarios(){
		$where = "WHERE 1=1 ";
		$sql = "SELECT id, descripcion FROM tipos_usuarios ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//Metodo para modificar registro doctores
	public function modificar_usuario($datos){
		if($datos["clave"]!="")
			$modificar_clave = ", clave='".sha1($datos["clave"])."'";
		else
			$modificar_clave = " ";	
		$sql="UPDATE usuarios 
					SET 
						id_tipo_usuario='".$datos["tipos_usuarios"]."',
						estatus ='".$datos["estatus"]."',
						login='".$datos["login"]."'".$modificar_clave."
			  WHERE 
			  		id='".$datos["id"]."'";
		//return $sql;	  		
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//--Metodo para consulta usuarios
	public function consultar_usuarios(){
		$sql = "SELECT 
						a.id,
						b.id AS id_personas,
						b.cedula,
						nombres_apellidos,
						c.descripcion AS tipo_usuario,
						b.email,
						b.estatus AS estatus_usuario,
						c.id AS id_tipo_usuario,
						a.login,
						b.telefono
				FROM 
						usuarios a 
				INNER JOIN 
						personas b
				ON 
						b.id = a.id_persona
				INNER JOIN
						tipos_usuarios c
				ON 
						c.id = a.id_tipo_usuario								
				ORDER BY 
					a.id DESC";
					
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	
	}//
	//--Metodo para consulta usuarios
	public function consultar_usuarios_sesion(){
		$sql = "SELECT 
						a.id,
						b.id AS id_personas,
						b.cedula,
						nombres_apellidos,
						c.descripcion AS tipo_usuario,
						b.email,
						b.estatus AS estatus_usuario,
						c.id AS id_tipo_usuario,
						a.login,
						b.telefono
				FROM 
						usuarios a 
				INNER JOIN 
						personas b
				ON 
						b.id = a.id_persona
				INNER JOIN
						tipos_usuarios c
				ON 
						c.id = a.id_tipo_usuario								
				ORDER BY 
					a.id DESC";
					
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	
	}//
	//--Consulta el ultimo id insertado en tabla usuarios
	public function maximo_id_usuarios(){
		$sql = "SELECT MAX(id) FROM usuarios";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
}	