<?php
require_once("../core/conex.php");
session_start();

class footerModel extends Conex{
	private $rs;
	private $rs2;
	//--Metodo constructor...
	public function __construct(){
	}
	//---
	public function maximo_id_footer(){
		$sql = "SELECT MAX(id) FROM footer";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function existe_footer($id){
		$sql = "SELECT 
						count(*)
				FROM 
						footer 
				WHERE
						id='".$id."'		
				";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function consultar_footer(){
		$sql = "SELECT 
						id,descripcion,correo,rif
				FROM 
						footer a
				order by id DESC		
				";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function registrar_footer($datos){
		$sql="INSERT INTO footer
			  (
					descripcion,
					correo,
					rif
			  ) 
			  VALUES (
			   			'".$datos["descripcion"]."',
			   			'".$datos["correo"]."',
			   			'".$datos["rif"]."'
			  )";
		//return $sql;	  
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	public function modificar_footer($arreglo_datos){
		$sql="UPDATE
				 footer
			  SET 	
			  		descripcion='".$arreglo_datos["descripcion"]."',
			  		correo = '".$arreglo_datos["correo"]."',
			  		rif = '".$arreglo_datos["rif"]."'
			   WHERE
			   		id='".$arreglo_datos["id"]."'";
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
}	