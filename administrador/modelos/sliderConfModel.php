<?php
require_once("../core/conex.php");
class sliderConfModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Consulta los datos de las categorias
	public function registrar_slider($arreglo_datos){
		$cuantos = count($arreglo_datos["titulo"]);
		for($i=0;$i<$cuantos;$i++){
			//echo "Datosxxx:".$arreglo_datos["titulo"][$i]."-".$arreglo_datos["descripcion"][$i];
			$sql = "INSERT INTO slider
								(imagen,
								titulo,
								descripcion,
								id_idioma)
						VALUES(
								'".$arreglo_datos["imagen"][$i]."',
								'".$arreglo_datos["titulo"][$i]."',
								'".sanar_cadena($arreglo_datos["descripcion"][$i])."',
								'".$arreglo_datos["idioma"]."')";
			$this->rs = $this->procesarQuery2($sql);
			$sql = "";
		}	
		return $this->rs;			
	}
	public function consultar_slider_lista($idioma){
		$sql = "SELECT id, titulo, descripcion,imagen, id_idioma FROM slider WHERE id_idioma='".$idioma."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	public function eliminar_slider($idioma){
		$sql = "DELETE FROM slider where id_idioma='".$idioma."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;		
	}
}	