<?php
require_once("../core/conex.php");
require_once("../core/fbasic.php");
class marcasModel extends Conex{
	//--Metodo maximo_id_marca
	public function maximo_id_marca(){
		$sql = "SELECT MAX(id) FROM marca";
		$this->rs3 = $this->procesarQuery($sql);
		return $this->rs3;
	}
	//--Metodo para verificar si existe una marca
	public function consultar_existe_marcas2($id){
		$sql = "SELECT count(*) FROM marca where id='".$id."'";
		$this->rs3 = $this->procesarQuery($sql);
		return $this->rs3;
	}
	//---Metodo para consultar marca
	public function consultar_marca(){
		$sql = "SELECT a.id,a.descripcion, a.id_idioma,a.estatus, b.descripcion FROM marca a INNER JOIN idioma b ON a.id_idioma= b.id order by a.id";
		$this->rs3 = $this->procesarQuery($sql);
		return $this->rs3;
	}
	//--Metodo para consultar soportes de marcas
	public function consulta_soportes_marcas($id){
		$sql = "select 
							e.ruta,
							e.id
						FROM 
							galeria e
						INNER JOIN 
							galeria_marca f
						ON
							f.id_galeria = e.id
						WHERE
							f.id_marca = '".$id."';";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//Metodo para modificar el estatus de la marca
	public function modificar_marca_estatus($id,$estatus){
		$sql="UPDATE marca
				 SET 
						estatus = '".$estatus."'
			  WHERE 
			  		id='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	//--Metodo para modificar una marca
	public function modificar_marca($datos){

		$sql="UPDATE 
					 marca
				SET		
					descripcion='".sanar_cadena($datos["descripcion"])."',
					id_idioma = '".$datos["id_idioma"]."',
					estatus = '".$datos["estatus"]."'
			  WHERE
			  		marca.id='".$datos["id"]."'";
		// Ejecuto el primer query
		$this->rs = $this->procesarQuery2($sql);
		if($this->rs==1){
			//--
			//Elimino todos las imagenes asociadas a esa marca
			$sql_delete = "DELETE FROM galeria_marca WHERE id_marca='".$datos["id"]."'";
			$this->rs_delete = $this->procesarQuery2($sql_delete);

			//Consulto el maximo id de la marca
			$arreglo_soportes_id = array();
			$arreglo_soportes_id = $datos["id_soportes_marca"];
			$sql_soportes = "";
			for($i=0;$i<count($arreglo_soportes_id);$i++){
				$sql_soportes=" INSERT INTO galeria_marca
									(id_marca,id_galeria)
								VALUES
									('".$datos["id"]."','".$arreglo_soportes_id[$i]."'); ";

				$this->rs2 = $this->procesarQuery2($sql_soportes);

			//--					
			}
			//--
		}
		return $this->rs;
	}
	//--Metodo para registrar doctores
	public function registrar_marca($datos){
	
		$sql="INSERT INTO marca
						(
							descripcion,
							id_idioma,
							estatus
						) 
			   VALUES (
			   			'".sanar_cadena($datos["descripcion"])."',
			   			'".$datos["id_idioma"]."',
			   			'0'
			   );";
		// Ejecuto el primer query
		$this->rs = $this->procesarQuery2($sql);
		if($this->rs==1){
			//--
			//Ejecuto el proceso del segundo query
			//Consulto el maximo id de la marca
			$maximo_id = $this->maximo_id_marca();
			$arreglo_soportes_id = array();
			$arreglo_soportes_id = $datos["id_soportes_marca"];
			$sql_soportes = "";
			for($i=0;$i<count($arreglo_soportes_id);$i++){
				$sql_soportes=" INSERT INTO galeria_marca
									(id_marca,id_galeria)
								VALUES
									('".$maximo_id[0][0]."','".$arreglo_soportes_id[$i]."'); ";

				$this->rs2 = $this->procesarQuery2($sql_soportes);

			//--					
			}
			//--
		}
		return $this->rs;

	}
}	