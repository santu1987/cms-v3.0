<?php
require_once("../core/conex.php");
class especialidadesModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//Metodo para consultar ultimo id especialidad 
	public function maximo_id_especialidades(){
		$sql = "SELECT MAX(id) FROM especialidad";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Consulta los datos de las especialidades
	public function consultar_especialidades(){
		$sql = "SELECT a.id, a.titulo, a.descripcion, a.imagen, (SELECT COUNT(*) FROM doctores c where c.id_especialidad = a.id) AS cuantos FROM especialidad a;";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--
	public function consultar_especialidades2(){
		$sql = "SELECT 
						a.id, 
						a.id_imagen,
						a.descripcion,
						a.estatus,
						a.titulo,
						b.ruta
				FROM 
						especialidad a
				INNER JOIN
						galeria b
				ON 
						b.id = a.id_imagen";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Metodo que verifica si existe una especialidad con ese titulo
	public function existe_especialidades_titulo($titulo){
		$where = "WHERE 1=1 ";
		if($id!="0"){
			$where.=  " AND a.titulo='".$titulo."'";
		}
		$sql = "SELECT count(a.id) FROM especialidad a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	
	}
	//---
	//--Metodo para registrar especialidades
	public function registrar_especialidades($datos,$id){
		$sql="INSERT INTO especialidad
						(
							id_imagen,
							titulo,
							descripcion,
							estatus
						) 
			   VALUES (
			   			'".$datos["id_imagen"]."',
			   			'".$datos["titulo"]."',
			   			'".sanar_cadena($datos["descripcion"])."',
			   			'1'
			   )";
		//return $sql;	   
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	//--
	}
	//--Consulta si existe una noticia
	public function existe_especialidades($id){
		$where = "WHERE 1=1 ";
		if($id!="0"){
			$where.=  " AND a.id='".$id."'";
		}
		$sql = "SELECT count(a.id) FROM especialidad a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//---
	//Metodo para modificar el estatus de la especialidad
	public function modificar_especialidades_estatus($id,$estatus){
		$sql="UPDATE especialidad
					SET 
						estatus = '".$estatus."'
			  WHERE 
			  		id='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	//Metodo para modificar registro especialidades
	public function modificar_especialidades($datos){
		$fecha = date("Y-m-d");
		$sql="UPDATE especialidad
					SET 
						id_imagen='".$datos["id_imagen"]."',
						titulo='".$datos["titulo"]."',
						descripcion='".$datos["descripcion"]."',
						estatus='".$datos["estatus"]."'
			  WHERE 
			  		id='".$datos["id"]."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}	
	//--
}	