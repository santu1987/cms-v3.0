<?php
require_once("../core/conex.php");
class noticiasModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Consulta los datos de doctores
	public function consultar_noticias(){
		$sql = "SELECT 
						a.id AS id_noticias,
						a.titulo,
						a.descripcion,
						a.fecha,
						b.ruta
				FROM 
					seccion_noticias a
				INNER JOIN 
					galeria b
				ON 
					a.id_imagen = b.id
				WHERE 
					a.estatus='1'
				order by a.id DESC;";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--Cuantos registros son
	public function cuantas_noticias_son(){
		$sql= "SELECT
					COUNT(*)
			   FROM 
			   		seccion_noticias a
			   WHERE 
					a.estatus='1'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}

}	