<?php
require_once("../core/conex.php");
class categoriasModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Consulta los datos de las categorias
	public function consultar_categorias($id){
		$where = "WHERE 1=1 ";
		if($id!=""){
			$where.= " AND a.id='".$id."'";
		}

		$sql = "SELECT a.id, a.nombre_categoria FROM categorias a ".$where.";";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
}	