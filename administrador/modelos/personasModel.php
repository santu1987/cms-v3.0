<?php
require_once("../core/conex.php");
class personasModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Verifica si existe la persona
	public function consultar_existe_persona($cedula,$id){
		$where = "WHERE 1=1 ";
		if($cedula!="0"){
			$where.=  " AND a.cedula='".$cedula."'";	
		}else{
			$where.= "AND a.id='".$id[0][0]."'";
		}
		
		$sql = "SELECT count(a.id) FROM personas a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--Verifica si existe la persona pasando solo id...
	public function consultar_existe_persona2($id){
		$where = " WHERE 1=1 AND a.id='".$id."'";
		$sql = "SELECT count(a.id) FROM personas a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--Modifico estatus de persona
	public function modificar_personas_estatus($id_personas,$estatus){
		$sql="UPDATE personas
					SET 
						estatus='".$estatus."'
			  WHERE 
			  		id='".$id_personas."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//Registro de persona para usuarios
	public function registrar_persona_datos($datos){
		$sql="INSERT INTO 
						personas
			  (
			  	cedula,
			  	nombres_apellidos,
			  	estatus,
			  	telefono,
			  	email
			  )
			  VALUES 
			  (
			  	'".$datos["cedula"]."',
			  	'".$datos["nombres_apellidos"]."',
			  	'1',
			  	'".$datos["telefono"]."',
			  	'".$datos["email"]."'
			  )";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;	  
	}
	//Metodo para modificar registro de personas para usuarios
	public function modificar_personas_datos($datos){
		$sql="UPDATE personas
					SET 
						cedula = '".$datos["cedula"]."',
						nombres_apellidos ='".$datos["nombres_apellidos"]."',
						telefono = '".$datos["telefono"]."',
						email = '".$datos["email"]."',
						estatus='".$datos["estatus"]."'
			  WHERE 
			  		id='".$datos["id_personas"]."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;	  		
	}
	//Registro de persona para doctores
	public function registrar_persona($datos){
		$sql="INSERT INTO 
						personas
			  (
			  	cedula,
			  	nombres_apellidos,
			  	estatus
			  )
			  VALUES 
			  (
			  	'".$datos["cedula"]."',
			  	'".$datos["nombres_apellidos"]."',
			  	'1'
			  )";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;	  
	}
	//Metodo para modificar registro de personas
	public function modificar_personas($datos){
		$sql="UPDATE personas
					SET 
						nombres_apellidos ='".$datos["nombres_apellidos"]."',
						estatus='".$datos["estatus"]."'
			  WHERE 
			  		id='".$datos["id_personas"]."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;	  		
	}
	//--Consulta el ultimo id insertado en tabla persnas
	public function maximo_id_personas(){
		$sql = "SELECT MAX(id) FROM personas";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--consulto el email del usuario
	public function consultar_correo($id){
		$sql = "SELECT b.email FROM cita a INNER JOIN personas b ON b.id=a.id_persona where a.id='".$id."';";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	} 
}	