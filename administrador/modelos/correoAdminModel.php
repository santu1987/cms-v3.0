<?php
require_once("../core/conex.php");
session_start();

class correoAdminModel extends Conex{
	private $rs;
	private $rs2;
	//--Metodo constructor...
	public function __construct(){
	}
	//---

	//Metodo que sirve para definir si existe el correo
	public function existe_correo($correo,$opcion){
		$where =  " WHERE 1=1 ";
		if($opcion=="1"){
			$where.= " AND a.correo = '".$correo."'";
		}else if($opcion=="2"){
			$where.= " AND a.id = '".$correo."'";
		}
		$sql = "SELECT COUNT(*) FROM correos a ".$where.";";
		//return $sql;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//Metodo que retorna el maximo id del correo
	public function maximo_id_correo(){
		$sql = "SELECT MAX(id) FROM correos";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	//Metodo para registrar correo
	public function registrar_correo($datos){
		$sql="INSERT INTO correos
			  (
					titulo,
					correo,
					estatus
			  ) 
			  VALUES (
			   			'".$datos["titulo"]."',
			   			'".$datos["correo"]."',
			   			'0'   			
			  )";
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;	
	}
	//Metodo para consultar correo
	public function consultar_correos(){
		$sql = "SELECT 
					a.id,
					a.titulo,
					a.correo,
					a.estatus
				FROM 
					correos a";
		//return $sql;			
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//Metodo para modificar el estatus del correo
	public function modificar_correo_estatus($id,$estatus){
		
		$sql="UPDATE correos 
					SET 
						estatus = '".$estatus."'
			  WHERE 
			  		id='".$id."'";
		//return $sql;	  		
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	//Metodo para modificar correos
	public function modificar_correo($datos){
		
		$sql="UPDATE correos 
					SET 
						titulo = '".$datos["titulo"]."',
			   			correo = '".$datos["correo"]."',
						estatus = '".$datos["estatus"]."'
			  WHERE 
			  		id='".$datos["id"]."'";
		//return $sql;	  		
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	public function existe_otro_correo($id,$correo){
		$where = "WHERE 1=1 ";
		$where.= " AND a.correo = '".$correo."'";
		$where.= " AND a.id != '".$id."'";
		$sql = "SELECT COUNT(*) FROM correos a ".$where.";";
		//return $sql;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
}	