<?php
require_once("../core/conex.php");
class contactosModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Consulta los datos de los contactos empleos
	public function consultar_contactos_empleo(){
		$where = "WHERE 1=1 ";
		$sql = "SELECT 
						id,
						nombres,
						email,
						area,
						mensaje,
						cv,
						telefono
				FROM 
						contactos_empleo
				order by id DESC";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;						
	}
	//--
	//--Consulta los datos de los contactos registradops
	public function consultar_contactos(){
		$where = "WHERE 1=1 ";
		$sql = "SELECT 
						id,
						nombres,
						email,
						telefono,
						mensaje
				FROM 
						contactos
				order by id DESC";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--
}	