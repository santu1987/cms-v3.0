<?php
require_once("../core/conex.php");
class galeriaModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Consulta los datos de las categorias
	public function consultar_galeria($id,$nombre){
		$where = "WHERE a.estatus=1 ";
		if($id!=""){
			$where.= " AND a.id_categoria='".$id."'";
		}
		if($nombre!=""){
			$where.= " AND a.ruta like '%".$nombre."%'";	
		}
		$sql = "SELECT a.id, a.ruta FROM galeria a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--Metodo para registrar imagenes
	public function registrar_imagen($path,$categoria){
		$sql = "INSERT INTO galeria (ruta,id_categoria, estatus) VALUES('".$path."','".$categoria."','1')";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo para eliminar imagenes
	public function eliminar_imagen($path){
		$sql = "DELETE FROM galeria where ruta='".$path."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--MEtodo para borrar logicamente imagenes
	public function borrar_imagen($imagenes){
		$where = "WHERE 1=1 ";
		$c = 1;
		foreach ($imagenes as $imagen) {
				if($c == 1){
					$where.=" AND ";
				}else if($c > 1){
					$where.=" OR ";
				}
				$where.= " a.id='".$imagen."'";
		}
		$sql = "UPDATE galeria a SET a.estatus='0' ".$where.";";
		//die($sql);
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
}