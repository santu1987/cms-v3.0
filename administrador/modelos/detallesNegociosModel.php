<?php
require_once("../core/conex.php");
session_start();

class detallesNegociosModel extends Conex{
	private $rs;
	private $rs2;
	//--Metodo constructor...
	public function __construct(){
	}
	//--
	//Metodo para consultar ultimo id insertado 
	public function maximo_id_negocios(){
		$sql = "SELECT MAX(id) FROM detalle_tipo_negocio";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo para copnsultar si existe un negocio con ese titulo
	public function existe_titulo($titulo){
		$sql = "SELECT count(*) FROM detalle_tipo_negocio WHERE titulo='".$titulo."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	public function existe_titulo_otro ($titulo,$id){
		$sql = "SELECT count(*) FROM detalle_tipo_negocio WHERE titulo='".$titulo."' and id!='".$id."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}	
	//--
	//--Metodo para registrar doctores
	public function registrar_negocios($datos){
		$fecha = date("Y-m-d");
		$id_usuario = $_SESSION['id'];
		$titulo_min = strtolower(normaliza($datos["titulo"]));
		$slug_detalles_negocios = str_replace(" ","-",$titulo_min);
		$slug_detalles_negocios = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug_detalles_negocios);
		$sql="INSERT INTO detalle_tipo_negocio
						(
							id_tipo_negocio,
							id_imagen,
							titulo,
							descripcion1,
							descripcion2,
							descripcion3,
							estatus,
							slug,
							id_idioma
						) 
			   VALUES (
			   			'".$datos["tipo_negocio"]."',
			   			'".$datos["id_imagen"]."',
			   			'".$datos["titulo"]."',
			   			'".sanar_cadena($datos["descripcion1"])."',
			   			'".sanar_cadena($datos["descripcion2"])."',
			   			'".sanar_cadena($datos["descripcion3"])."',
			   			'0',
			   			'".$slug_detalles_negocios."',
			   			'".$datos["id_idioma"]."'
			   )";
		//return $sql;	   
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	//--
	}
	//--Consulta si existe una noticia
	public function existe_negocio($id){
		$where = "WHERE 1=1 ";
		if($id!="0"){
			$where.=  " AND a.id='".$id."'";
		}
		$sql = "SELECT count(a.id) FROM detalle_tipo_negocio a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//---
	//Metodo para modificar registro seccion_noticias
	public function modificar_negocios($datos){
		$titulo_min = strtolower($datos["titulo"]);
		$slug = str_replace(" ","-",normaliza($titulo_min));
		$slug = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug);

		$sql="UPDATE detalle_tipo_negocio 
					SET 
						id_tipo_negocio='".$datos["tipo_negocio"]."',
						id_imagen='".$datos["id_imagen"]."',
						titulo='".$datos["titulo"]."',
						descripcion1='".sanar_cadena($datos["descripcion1"])."',
						descripcion2='".sanar_cadena($datos["descripcion2"])."',
						descripcion3='".sanar_cadena($datos["descripcion3"])."',
						estatus='".$datos["estatus"]."',
						slug = '".$slug."',
						id_idioma='".$datos["id_idioma"]."'
			  WHERE 
			  		id='".$datos["id"]."'";
		//return $sql;	  		
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}	
	//--
	//Metodo para modificar el estatus de la noticia
	public function modificar_negocios_estatus($id,$estatus){
		$fecha = date("Y-m-d");
		$sql="UPDATE detalle_tipo_negocio 
					SET 
						estatus = '".$estatus."'
			  WHERE 
			  		id='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}

	//--Metodo que modifica el registro de detalle noticias con el path del archivo
	public function modificar_folleto($path,$id_negocio){
		$sql="UPDATE detalle_tipo_negocio 
					SET 
						folleto = '".$path."'
			  WHERE 
			  		id='".$id_negocio."'";
		//return $sql;	  		
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	public function consultar_negocios(){
		$sql = "SELECT 
						a.id, 
						a.id_imagen,
						b.ruta,
						a.descripcion1,
						a.descripcion2,
						a.descripcion3,
						a.estatus,
						a.titulo,
						a.slug,
						a.id_tipo_negocio,
						c.id as id_tipo_tipo_negocio,
						c.titulo as titulo_tipo_negocio,
						c.descripcion as descripcion_tipo_negocio,
						a.folleto,
						a.id_idioma,
						d.descripcion
				FROM 
						detalle_tipo_negocio a
				INNER JOIN
						galeria b
				ON 
						b.id = a.id_imagen
				INNER JOIN
						tipo_negocio c
				ON 
						c.id= a.id_tipo_negocio
				INNER JOIN 
						idioma d
				ON 
						d.id = a.id_idioma																
				order by a.id DESC";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
}	