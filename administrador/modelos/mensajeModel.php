<?php
require_once("../core/conex.php");
class mensajeModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Consulta los datos de las categorias
	public function registrar_mensaje($datos){
		$where = "WHERE 1=1 ";
		$sql = "INSERT INTO mensajes_pagina(titulo,descripcion,estatus) VALUES ('".$datos["titulo"]."','".$datos["descripcion"]."','".$datos["estatus_publicado"]."')";
		$this->rs = $this->procesarQuery2($sql);
		$sql = "SELECT MAX(id) FROM mensajes_pagina";
		$id = $this->procesarQuery($sql);
		return $this->rs."*".$id[0][0];			
	}
	//--Modifica los datos de los mensajes
	public function actualizar_mensaje($datos){
		$where = " WHERE	id='".$datos["id"]."' ";
		$sql = "UPDATE 
						mensajes_pagina 
				SET
					titulo='".$datos["titulo"]."',
					descripcion = '".$datos["descripcion"]."',
					estatus = '".$datos["estatus"]."'
				".$where."";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs."*".$datos["id"];			
	}
	//--Consulta si existe el mensaje
	public function consultar_existe_mensaje($id){
		$sql= "SELECT count(*)
			   FROM 
			   		mensajes_pagina
			   WHERE
			   		id='".$id."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	   		
	}
	//
	public function modificar_mensajes_estatus($id,$estatus){
		$where = " WHERE	id='".$id."' ";
		$sql = "UPDATE 
						mensajes_pagina 
				SET
					estatus = '".$estatus."'
				".$where."";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs."*".$datos["id"];			
	}
	//--Consulta los datos de los mensajes
	public function consultar_mensajes(){
		$sql = "SELECT id, titulo, descripcion, estatus FROM mensajes_pagina";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//----------------------------------------
}	