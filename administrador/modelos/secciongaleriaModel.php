<?php
require_once("../core/conex.php");
class secciongaleriaModel extends Conex{
	private $rs;
	private $rs2;
	//--Metodo constructor...
	public function __construct(){
	}
	//--
	//Metodo para consultar ultimo id insertado 
	public function maximo_id_imagen(){
		$sql = "SELECT MAX(id) FROM seccion_galeria";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Metodo para registrar doctores
	public function registrar_imagen($datos){
		$sql="INSERT INTO seccion_galeria
						(
							id_imagen,
							descripcion,
							estatus
						) 
			   VALUES (
			   			'".$datos["id_imagen"]."',
			   			'".$datos["descripcion"]."',
			   			'0'
			   )";
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	//--
	}
	//---
	//--Consulta los datos de las galerias
	public function consultar_galeria_img(){
		$sql = "SELECT 
						a.id, 
						a.id_imagen,
						b.ruta,
						a.descripcion,
						a.estatus
				FROM 
						seccion_galeria a
				INNER JOIN 
						galeria b
				ON 
						b.id = a.id_imagen
				ORDER BY 
						a.id DESC";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;				
	}
	//--Consulta si existe una imagen
	public function existe_imagen($id){
		$where = "WHERE 1=1 ";
		if($id!="0"){
			$where.=  " AND a.id='".$id."'";
		}
		$sql = "SELECT count(a.id) FROM seccion_galeria a ".$where;
		//return $sql;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//---
	//Metodo para modificar registro seccion_galerias
	public function modificar_imagen($datos){
		$sql="UPDATE seccion_galeria 
					SET 
						id_imagen='".$datos["id_imagen"]."',
						descripcion='".$datos["descripcion"]."',
						estatus = '".$datos["estatus"]."'
			  WHERE 
			  		id='".$datos["id"]."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}	
	//---
	//Metodo para modificar el estatus de la noticia
	public function modificar_galeria_estatus($id,$estatus){
		$fecha = date("Y-m-d");
		$sql="UPDATE seccion_galeria 
					SET 
						estatus = '".$estatus."'
			  WHERE 
			  		id='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
}	