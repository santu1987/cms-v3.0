<?php
require_once("../core/conex.php");
session_start();

class direccionModel extends Conex{
	private $rs;
	private $rs2;
	//--Metodo constructor...
	public function __construct(){
	}
	//---
	public function maximo_id_direccion(){
		$sql = "SELECT MAX(id) FROM direcciones";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function existe_telefono_direccion($id_direccion,$telefono){
		$sql = "SELECT 
						count(*)
				FROM 
						telefonos a	
				WHERE 
						id_direccion='".$id_direccion."'
				AND
						telefono='".$telefono."'";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function existe_direccion($id){
		$sql = "SELECT 
						count(*)
				FROM 
						direcciones 
				WHERE
						id='".$id."'		
				";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function consultar_direcciones(){
		$sql = "SELECT 
						id,
						descripcion,
						estatus
				FROM 
						direcciones a
				order by id DESC		
				";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function consultar_telefonos($id_direccion){
		$sql = "SELECT 
						telefono
				FROM 
						telefonos a
				WHERE 
						id_direccion='".$id_direccion."'";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function registrar_direccion($datos){
		$sql="INSERT INTO direcciones
			  (
					descripcion,
					estatus
			  ) 
			  VALUES (
			   			'".$datos["descripcion"]."',
			   			'0'   			
			  )";
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	public function registrar_telefono($id_direccion,$telefono){
		$sql="INSERT INTO telefonos
			  (
					id_direccion,
					telefono
			  ) 
			  VALUES (
			   			'".$id_direccion."',
			   			'".$telefono."'   			
			  )";
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}

	//---
	public function modificar_direccion_estatus($id,$estatus){
		$sql="UPDATE direcciones 
					SET 
						estatus = '".$estatus."'
			  WHERE 
			  		id='".$id."'";
		//return $sql;	  		
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;	  		
	}
	//---
	public function modificar_direccion($arreglo_datos){
		$sql="UPDATE
				 direcciones
			  SET 	
			  		descripcion='".$arreglo_datos["descripcion"]."',
					estatus = '".$arreglo_datos["estatus"]."'
			   WHERE
			   		id='".$arreglo_datos["id"]."'";
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	public function eliminar_telefonos($id_direccion){
		$sql="DELETE
			  	FROM 	 
				 telefonos
			  WHERE
			   	 id_direccion='".$id_direccion."'";
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
}	