<?php
require_once("../core/conex.php");
session_start();

class seccionnoticiasModel extends Conex{
	private $rs;
	private $rs2;
	//--Metodo constructor...
	public function __construct(){
	}
	//--
	//Metodo para consultar ultimo id insertado 
	public function maximo_id_noticias(){
		$sql = "SELECT MAX(id) FROM seccion_noticias";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Metodo para registrar doctores
	public function registrar_noticias($datos){
		$fecha = date("Y-m-d");
		$id_usuario = $_SESSION['id'];
		$titulo_min = strtolower(normaliza($datos["titulo"]));
		$slug_noticias = str_replace(" ","-",$titulo_min);
		$slug_noticias = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug_noticias);

		$sql="INSERT INTO seccion_noticias
						(
							id_imagen,
							titulo,
							descripcion,
							estatus,
							fecha,
							id_usuario,
							slug,
							id_idioma
						) 
			   VALUES (
			   			'".$datos["id_imagen"]."',
			   			'".$datos["titulo"]."',
			   			'".sanar_cadena($datos["descripcion"])."',
			   			'0',
			   			'".$fecha."',
			   			'".$id_usuario."',
			   			'".$slug_noticias."',
			   			'".$datos["id_idioma"]."'
			   )";
		//return $sql;	   
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	//--
	}
	//--Consulta si existe una noticia
	public function existe_noticias($id){
		$where = "WHERE 1=1 ";
		if($id!="0"){
			$where.=  " AND a.id='".$id."'";
		}
		$sql = "SELECT count(a.id) FROM seccion_noticias a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//---
	//Metodo para modificar registro seccion_noticias
	public function modificar_noticias($datos){
		$fecha = date("Y-m-d");
		$titulo_min = strtolower(normaliza($datos["titulo"]));
		$slug_noticias = str_replace(" ","-",$titulo_min);
		$slug_noticias = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug_noticias);
		$sql="UPDATE seccion_noticias 
					SET 
						id_imagen='".$datos["id_imagen"]."',
						titulo='".$datos["titulo"]."',
						descripcion='".$datos["descripcion"]."',
						fecha ='".$fecha."',
						estatus='".$datos["estatus"]."',
						slug = '".$slug_noticias."',
						id_idioma ='".$datos["id_idioma"]."'
			  WHERE 
			  		id='".$datos["id"]."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}	
	//--
	//Metodo para modificar el estatus de la noticia
	public function modificar_noticias_estatus($id,$estatus){
		$fecha = date("Y-m-d");
		$sql="UPDATE seccion_noticias 
					SET 
						estatus = '".$estatus."',
						fecha ='".$fecha."'
			  WHERE 
			  		id='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	public function consultar_noticias(){
		$sql = "SELECT 
						a.id, 
						a.id_imagen,
						b.ruta,
						a.descripcion,
						a.estatus,
						a.fecha,
						a.titulo,
						a.id_usuario,
						d.nombres_apellidos,
						a.slug,
						a.id_idioma,
						e.descripcion
				FROM 
						seccion_noticias a
				INNER JOIN
						galeria b
				ON 
						b.id = a.id_imagen
				INNER JOIN
						usuarios c
				ON 
						c.id = a.id_usuario
				INNER JOIN 
						personas d
				ON 
						d.id=c.id_persona
				INNER JOIN 
						idioma e
				ON 
						e.id = a.id_idioma										
				order by 
						a.id DESC";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
}	