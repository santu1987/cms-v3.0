<?php
require_once("../core/conex.php");
session_start();

class redesModel extends Conex{
	private $rs;
	private $rs2;
	//--Metodo constructor...
	public function __construct(){
	}
	//---
	public function consultar_redes(){
		$sql = "SELECT 
						a.id, 
						a.descripcion
				FROM 
						tipo_red a											
				order by 
						a.id DESC";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function existe_red($id){
		$sql = "SELECT 
						count(*)
				FROM 
						red_social a	
				WHERE 
						id='".$id."'";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function registrar_red($datos){
		$sql="INSERT INTO red_social
			  (
					id_tipo_red,
					url_red
			  ) 
			  VALUES (
			   			'".$datos["tipo_red"]."',
			   			'".$datos["url_red"]."'		   			
			  )";
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	public function modificar_red($datos){
		$sql = "UPDATE
						red_social
				SET 
						url_red='".$datos["url_red"]."'
				WHERE 
						id='".$datos["id"]."'";
		//return $sql;	   
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//--
	public function maximo_id_redes(){
		$sql = "SELECT MAX(id) FROM red_social";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
	public function consultar_redes_detalle($id){
		$sql = "SELECT 
						id,
						url_red,
						id_tipo_red
				FROM
					red_social
				WHERE
						id_tipo_red='".$id."'";
		//return $sql;				
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//---
}	