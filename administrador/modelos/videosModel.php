<?php
require_once("../core/conex.php");
class videosModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Metodo para consulta si existe un video con ese link
	public function existe_video($link){
		$where = "WHERE 1=1 ";
		if($link!=""){
			$where.=" AND a.link='".$link."';";
		}
		$sql = "SELECT COUNT(*) FROM videos a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	public function existe_video2($id){
		$where = "WHERE 1=1 ";
		if($id!=""){
			$where.=" AND a.id='".$id."';";
		}
		$sql = "SELECT COUNT(*) FROM videos a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo para consultar si existe otro video con ese link
	public function existe_video_link($id,$link){
		$where = "WHERE estatus=1 ";
		if($id!=""){
			$where.= "AND a.id!='".$id."'";
		}
		if($link!=""){
			$where.=" AND a.link='".$link."';";
		}
		$sql = "SELECT COUNT(*) FROM videos a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo para registrar videos
	public function registrar_videos($arreglo_datos){
		$sql = "INSERT INTO videos (titulo, link, estatus, id_idioma) VALUES('".$arreglo_datos["titulo"]."','".$arreglo_datos["link"]."','0','".$arreglo_datos["id_idioma"]."')";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//--Metodo para obtener el maximo id de video
	public function maximo_id_video(){
		$sql = "SELECT MAX(id) FROM videos";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo para obtener lista de videos cargados
	public function consultar_galeria_videos(){
		$sql = "SELECT 
						a.id,
						a.titulo,
						a.link,
						a.estatus,
						a.id_idioma,
						b.descripcion
				FROM 
						videos a 
				INNER JOIN 
						idioma b
				ON 
						a.id_idioma = b.id				
				ORDER BY 
					a.id DESC";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo para consultar si existe video con ese id
	public function consultar_existe_video_id($id){
		$where = "WHERE 1=1 ";
		if($id!=""){
			$where.= "AND a.id='".$id."'";
		}
		$sql = "SELECT COUNT(*) FROM videos a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	
	}
	//--Metodo para modificar estatus
	public function modificar_videos_estatus($id,$estatus){
		$sql = "UPDATE
						videos 
				SET 
						estatus='".$estatus."' 
				WHERE
						id='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//--Metodo para modificar
	public function modificar_videos($datos){
		$sql = "UPDATE
						videos 
				SET 
						titulo='".$datos["titulo"]."',
						link='".$datos["link"]."',
						estatus='".$datos["estatus"]."',
						id_idioma='".$datos["id_idioma"]."' 
				WHERE
						id='".$datos["id"]."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//--
}