<?php
require_once("../core/conex.php");
class quienesSomosModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//Metodo para consultar ultimo id especialidad 
	public function maximo_id_quienes_somos(){
		$sql = "SELECT MAX(id) FROM empresa";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Consulta los datos de las especialidades
	public function consultar_especialidades(){
		$sql = "SELECT a.id, a.titulo, a.descripcion, a.imagen, (SELECT COUNT(*) FROM doctores c where c.id_especialidad = a.id) AS cuantos FROM especialidad a;";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--
	public function consultar_quienes_somos(){
		$sql = "SELECT 
						a.id, 
						a.quienes_somos,
						a.mision,
						a.vision,
						a.objetivo,
						a.id_imagen,
						b.ruta AS imagen,
						a.id_idioma,
						c.descripcion,
						a.estatus
				FROM 
						empresa a 
				INNER JOIN
						galeria b
				ON 
						b.id = a.id_imagen
				INNER JOIN
						idioma c
				ON 
						c.id = a.id_idioma		
				";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Metodo que verifica si existe una especialidad con ese titulo
	public function existe_empresa($id){
		$where = "WHERE 1=1 ";
		if(($id!="0")&&($id!="")){
			$where.=  " AND id='".$id."'";
		}
		$sql = "SELECT count(id) FROM empresa ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	
	}
	//---
	//--Metodo para registrar especialidades
	public function registrar_quienes_somos($datos,$id){
		$sql="INSERT INTO empresa
						(
							quienes_somos,
							mision,
							vision,
							objetivo,
							id_imagen,
							id_idioma,
							estatus
						) 
			   VALUES (
			   			'".sanar_cadena($datos["quienes_somos"])."',
			   			'".sanar_cadena($datos["mision"])."',
			   			'".sanar_cadena($datos["vision"])."',
			   			'".sanar_cadena($datos["objetivo"])."',
			   			'".$datos["id_imagen"]."',
			   			'".$datos["id_idioma"]."',
			   			'0'
			   )";
		//return $sql;	   
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	//--
	}
	//---
	//Metodo para modificar el estatus de la especialidad
	public function modificar_quienes_somos_estatus($id,$estatus){
		$sql="UPDATE empresa
				 SET 
						estatus = '".$estatus."'
			  WHERE 
			  		id='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	//Metodo para modificar registro especialidades
	public function modificar_quienes_somos($datos){
		$fecha = date("Y-m-d");
		$sql="UPDATE empresa
					SET 
						quienes_somos='".sanar_cadena($datos["quienes_somos"])."',
						mision='".sanar_cadena($datos["mision"])."',
						vision='".sanar_cadena($datos["vision"])."',
						objetivo='".sanar_cadena($datos["objetivo"])."',
						id_imagen = '".$datos["id_imagen"]."',
						id_idioma = '".$datos["id_idioma"]."',
						estatus = '".$datos["estatus"]."'
			  WHERE 
			  		id='".$datos["id"]."'";
		//return($sql);		  		
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	//Metodo para consultar idiomas
	public function consultar_idioma(){
		$sql2 = "SELECT id,descripcion FROM idioma ";
		$this->rs = $this->procesarQuery($sql2);
		return $this->rs;
	}	
	//--
}	