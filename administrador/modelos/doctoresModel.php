<?php
require_once("../core/conex.php");
require_once("../core/fbasic.php");
class doctoresModel extends Conex{
	private $rs;
	private $rs2;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Consulta los datos de los doctores
	public function consultar_existe_dr($cedula,$id){
		$where = "WHERE 1=1 ";
		if($cedula!="0"){
			$where.=  " AND b.cedula='".$cedula."'";
		}
		if($id!="0"){
			$where.=  " AND a.id='".$id."'";
		}
		$sql = "SELECT count(a.id) FROM doctores a INNER JOIN personas b ON a.id_persona=b.id  ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--Metodo para registrar doctores
	public function registrar_dr($datos,$id){
		if($datos["imagen"]!=""){
			$imagen = $datos["imagen"];
		}else{
			$imagen = "../admin/site_media/images/archivos/doctores/doctor.png";
		}
		$sql="INSERT INTO doctores
						(
							id_persona,
							id_especialidad,
							img,
							descripcion
						) 
			   VALUES (
			   			'".$id[0][0]."',
			   			'".$datos["especialidad"]."',
			   			'".$imagen."',
			   			'".sanar_cadena($datos["descripcion"])."'
			   );";
		// Ejecuto el primer query
		$this->rs = $this->procesarQuery2($sql);
		if($this->rs==1){
			//--
			//Ejecuto el proceso del segundo query
			//Consulto el maximo id del dr
			$maximo_id = $this->maximo_id_dr();
			$arreglo_soportes_id = array();
			$arreglo_soportes_id = $datos["id_soportes"];
			$sql_soportes = "";
			for($i=0;$i<count($arreglo_soportes_id);$i++){
				$sql_soportes=" INSERT INTO doctores_soportes
									(id_doctores,id_galeria,estatus)
								VALUES
									('".$maximo_id[0][0]."','".$arreglo_soportes_id[$i]."','1'); ";
				// Ejecuto el primer query
				$this->rs2 = $this->procesarQuery2($sql_soportes);
			//--					
			}
			//--
		}
		return $this->rs;

	}
	//Metodo para modificar registro doctores
	public function modificar_doctor($datos){
		$sql="UPDATE doctores 
					SET 
						id_especialidad='".$datos["especialidad"]."',
						img ='".$datos["imagen"]."',
						descripcion='".sanar_cadena($datos["descripcion"])."'
			  WHERE 
			  		id='".$datos["id"]."'";
		$this->rs = $this->procesarQuery2($sql);
		//Modifico la tabla soportes
		$sql2 = "UPDATE 
						doctores_soportes
				SET
						estatus='2'
				WHERE
						id_doctores='".$datos["id"]."'";
		$this->rs_a = $this->procesarQuery2($sql2);
		//Inserto los nuevos soportes
		$arreglo_soportes_id = array();
		$arreglo_soportes_id = $datos["id_soportes"];
		$sql_soportes = "";
		if(count($arreglo_soportes_id)>0){
			//------------------------------
			for($i=0;$i<count($arreglo_soportes_id);$i++){
				$sql_soportes=" INSERT INTO doctores_soportes
									(id_doctores,id_galeria,estatus)
								VALUES
									('".$datos["id"]."','".$arreglo_soportes_id[$i]."','1'); ";
			//--
			// Ejecuto el query
			$this->rs_b = $this->procesarQuery2($sql_soportes);
			//--					
			}
		}	
		//return count($datos["id_soportes"]);
		return $this->rs;			 		
	}
	//Metodo para consultar ultimo id insertado 
	public function maximo_id_dr(){
		$sql = "SELECT MAX(id) FROM doctores";
		$this->rs3 = $this->procesarQuery($sql);
		return $this->rs3;
	}
	//--Consulta los datos de los mensajes
	public function consultar_doctores(){
		$sql = "SELECT 
					a.id as id,
					b.id as id_persona,
					b.cedula,
					b.nombres_apellidos,
					c.id as id_especialidad,
					a.descripcion,
					a.img,
					c.titulo as nombre_especialidad,
					b.estatus									
				FROM 
					doctores a 
				INNER JOIN 
					personas b
				ON 
					a.id_persona = b.id
				INNER JOIN 
					especialidad c
				ON 
					a.id_especialidad=c.id 
				ORDER BY 
					nombres_apellidos DESC";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Consulta patra soportes doctores....
	public function consulta_soportes_doctores($id){
		$sql = "select 
							e.ruta,
							e.id
						FROM 
							galeria e
						INNER JOIN 
							doctores_soportes f
						ON
							f.id_galeria = e.id
						WHERE
							f.estatus='1'
						AND 
							f.id_doctores = '".$id."';";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Consulta de citas
	public function consultar_citas_doctores($id){
		$sql="SELECT
					a.id,
					a.id_persona,
					b.nombres_apellidos,
					a.observacion,
					a.estatus,
					a.fecha,
					a.fecha_solicitud
			  FROM 
			  		cita a
			  INNER JOIN 
			  	personas b
			  ON 
			  	b.id=a.id_persona
			  INNER JOIN
			  	doctores c
			  ON 
			  	a.id_especialidad=c.id_especialidad 	
			  WHERE
			  		c.id='".$id."'
			  AND
					a.estatus='1'		
			  ORDER BY
			  	a.fecha_solicitud;";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	  		
	}
	//---Consulto si existe la cita
	public function consultar_existe_cita($id){
		$sql = "SELECT count(*)
				FROM 
					cita
				WHERE
					id='".$id."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	
	}
	//---Consulto si existe la cita en ese horario
	public function consultar_existe_cita_horario($fecha,$hora){
		$hora = strtotime($hora);
		$hora24 =  date("H:i", $hora);
		$super_fecha =$fecha." ".$hora24;
		
		$sql = "SELECT count(*)
				FROM 
					cita
				WHERE
					fecha='".$super_fecha."'";
		//return $sql;			
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	
	}
	//--Registrar citas
	public function registrar_cita($datos){
		$hora = strtotime($datos["hora"]);
		$hora24 =  date("H:i", $hora);
		$fecha =$datos["fecha"]." ".$hora24;
		//$newformat = date('Y-m-d h:i:s',$fecha);
		$sql = "UPDATE
						cita
				SET 
					fecha='".$fecha."',
					id_doctor='".$datos["id_doctor"]."',
					estatus='2' 
				WHERE
						cita.id ='".$datos["id"]."'";
		//die($sql);				
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//--Anular citas
	public function anular_cita($id){
		$sql = "UPDATE
						cita
				SET 
					estatus='3' 
				WHERE
						cita.id ='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//--Limpiar citas
	public function limpiar_citas($id){
		$sql = "UPDATE
						cita
				SET 
					estatus='1' 
				WHERE
						cita.id ='".$id."'";				
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//--
	public function consultar_citas_programadas($estatus,$id_especialidad, $id_doctor){
		$sql = "SELECT 
						a.id,
						a.estatus,
						a.fecha,
						b.nombres_apellidos AS nombre_persona,
						DATE_FORMAT(a.fecha,'%d-%m-%Y %h:%i %p') AS super_fecha
				FROM 
					cita a 
				INNER JOIN 
					personas b
				ON 
					a.id_persona = b.id
				INNER JOIN 
					especialidad c 
				ON 
					a.id_especialidad = c.id				
				WHERE
					a.estatus='".$estatus."' 
				AND 
					a.id_especialidad  ='".$id_especialidad."' 
				AND 
					a.id_doctor = '".$id_doctor."' 
				ORDER BY DATE_FORMAT(a.fecha,'%d-%m-%Y %h:%i %p') ";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	public function consultar_datos_cita($id){
		$sql = "SELECT 
						a.id,
						a.estatus,
						a.fecha,
						b.nombres_apellidos AS nombre_persona,
						DATE_FORMAT(a.fecha,'%d-%m-%Y %h:%i %p') AS super_fecha
				FROM 
					cita a 
				INNER JOIN 
					personas b 
				ON 
					b.id=a.id_persona	
				WHERE
						a.id='".$id."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;					
	}
	//----------------------------------------
}
//DATE_FORMAT(a.fecha,'%d-%m-%Y %h:%i %p') AS super_fecha,