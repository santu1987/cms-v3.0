<?php
require_once("../core/conex.php");
class tiposNegociosModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//Metodo para consultar ultimo id especialidad 
	public function maximo_id_tipos_negocios(){
		$sql = "SELECT MAX(id) FROM tipo_negocio";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Consulta los datos de las especialidades
	public function consultar_especialidades(){
		$sql = "SELECT a.id, a.titulo, a.descripcion, a.imagen, (SELECT COUNT(*) FROM doctores c where c.id_especialidad = a.id) AS cuantos FROM especialidad a;";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--
	public function consultar_tipos_negocios2($estatus){
		if($estatus!=""){
			$where = "WHERE 
							a.estatus='".$estatus."'";
		}else{
			$where = "";
		}
		$where = "";
		$sql = "SELECT 
						a.id,
						a.titulo,
						a.descripcion,
						a.estatus,
						a.id_idioma,
						b.descripcion
				FROM 
						tipo_negocio a
				INNER JOIN 
						idioma b
				ON 
						b.id = a.id_idioma				
				";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Metodo que verifica si existe un tipo_negocio con ese titulo
	public function existe_tipos_negocios_titulo($titulo){
		$where = "WHERE 1=1 ";
		$where.=  " AND a.titulo='".$titulo."'";
		$sql = "SELECT count(a.id) FROM tipo_negocio a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;	
	}
	
	//---Metodo que verifica si existe un tipo_negocio con ese id
	public function existe_tipos_negocios($id){
		$where = "WHERE 1=1 ";
		$where.=  " AND a.id='".$id."'";
		$sql = "SELECT count(a.id) FROM tipo_negocio a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;		
	}
	//---

	//--Metodo para registrar especialidades
	public function registrar_tipos_negocios($datos){
		$sql="INSERT INTO tipo_negocio
						(
							titulo,
							descripcion,
							estatus,
							id_idioma
						) 
			   VALUES (
			   			'".$datos["titulo"]."',
			   			'".sanar_cadena($datos["descripcion"])."',
			   			'1',
			   			'".$datos["id_idioma"]."'
			   )";
		//return $sql;	   
		// Ejecuto el query
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	//--
	}
	//--Consulta si existe una noticia
	public function existe_especialidades($id){
		$where = "WHERE 1=1 ";
		if($id!="0"){
			$where.=  " AND a.id='".$id."'";
		}
		$sql = "SELECT count(a.id) FROM especialidad a ".$where;
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//---
	//Metodo para modificar el estatus de la especialidad
	public function modificar_tipos_negocios_estatus($id,$estatus){
		$sql="UPDATE tipo_negocio
					SET 
						estatus = '".$estatus."'
			  WHERE 
			  		id='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}
	//---
	//Metodo para modificar registro especialidades
	public function modificar_tipos_negocios($datos){
		$fecha = date("Y-m-d");
		$sql="UPDATE tipo_negocio
					SET 
						titulo='".$datos["titulo"]."',
						descripcion='".$datos["descripcion"]."',
						estatus='".$datos["estatus"]."',
						id_idioma= '".$datos["id_idioma"]."'
			  WHERE 
			  		id='".$datos["id"]."'";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
	}	
	//--
}	