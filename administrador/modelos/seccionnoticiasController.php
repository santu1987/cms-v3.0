<?php
require_once("../modelos/seccionnoticiasModel.php");
require_once("../core/fbasic.php");
//--Declaraciones
$mensajes = array();
//--Recibo lo enviado por POST
$data = json_decode(file_get_contents("php://input"));

$post = helper_userdata($data);
redireccionar_metodos($post);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'registrar_noticias':
			registrar_noticias($arreglo_datos);
			break;	
		case 'modificar_noticias':
			modificar_noticias($arreglo_datos);
			break;
		case 'consultar_noticias':
			consultar_noticias($arreglo_datos);
			break;	
		case 'modificar_estatus':
			modificar_estatus($arreglo_datos);
			break;		
	}	
}
//---
function helper_userdata($data){
	$user_data = array();
	$user_data["accion"] = $data->accion;
	if(isset($data->id))
		$user_data["id"] = $data->id;
	else
		$user_data["id"] = 	"";

	if(isset($data->titulo))
		$user_data["titulo"] = $data->titulo;
	else 
		$user_data["titulo"] = "";

	if(isset($data->id_imagen))
		$user_data["id_imagen"] = $data->id_imagen;
	else
		$user_data["id_imagen"] = "";

	if(isset($data->descripcion))
		$user_data["descripcion"] = $data->descripcion;
	else
		$user_data["descripcion"] = "";

	if(isset($data->estatus))
		$user_data["estatus"] = $data->estatus;
	else
		$user_data["estatus"] = "";
	return $user_data;
}
//---
//------------------------------------------------------
function registrar_noticias($arreglo_datos){
	//------------------------------------
	$recordset = array();
	$objeto = new seccionnoticiasModel();
	//Verifico si existe
	if($arreglo_datos["id"]!=""){
		$mensajes["mensajes"] = "existe";
	}else{
		//no existe, guardo
		$recordset = $objeto->registrar_noticias($arreglo_datos);
		die($recordset);
		if($recordset==1){
			$mensajes["mensajes"] = "registro_procesado";
			$id_imagen = $objeto->maximo_id_noticias();
			$mensajes["id"] = $id_imagen[0][0];
		}else{
			$mensajes["mensajes"] = "error";
		}
	}
	die(json_encode($mensajes));
	//--
}	
//---
function modificar_noticias($arreglo_datos){
	$recordset = array();
	$objeto = new seccionnoticiasModel();
	$existe = $objeto->existe_noticias($arreglo_datos["id"]);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		$recordset = $objeto->modificar_noticias($arreglo_datos);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function modificar_estatus($arreglo_datos){
	$recordset = array();
	$objeto = new seccionnoticiasModel();
	$existe = $objeto->existe_noticias($arreglo_datos["id"]);
	//Verifico si existe la imagen
	if($existe[0][0]==0){
		$mensajes["mensaje"] = "no_existe";
	}else{
		if($arreglo_datos["estatus"]==0){
			$arreglo_datos["estatus"] = 1;
		}else{
			$arreglo_datos["estatus"] = 0;
		}
		$recordset = $objeto->modificar_noticias_estatus($arreglo_datos["id"],$arreglo_datos["estatus"]);
		if($recordset==1){
			$mensajes["mensajes"] = "modificacion_procesada"; 
		}else{
			$mensajes["error"] = "error";
		}
	}
	die(json_encode($mensajes));
}
//---
function consultar_noticias(){
	$recordset = array();
	$mensajes = array();
	$objeto = new seccionnoticiasModel();
	$recordset = $objeto->consultar_noticias();
	$i = 0;
	$soportes = "";
	foreach ($recordset as $campo) {
		//$fecha = new DateTime($campo[5]);
		$vector_fecha = explode("-", $campo[5]);
		$fecha = $vector_fecha[2]."-".$vector_fecha[1]."-".$vector_fecha[0];
		$a = $i+1;
		$descripcion_corta = substr($campo[3],0,600)."...";
		$mensajes[] = array("id_noticias"=>$campo[0],"titulo"=>$campo[6],"imagen"=>$campo[2],"descripcion"=>$campo[3],"id_imagen"=>$campo[1],"estatus"=>$campo[4],"fecha"=>$fecha,"number"=>$a,"descripcion_corta"=>$descripcion_corta,"id_usuario"=>$campo[7],"nombre_usuario"=>$campo[8]);
		$i++;
	}
	die(json_encode($mensajes));
}
//--