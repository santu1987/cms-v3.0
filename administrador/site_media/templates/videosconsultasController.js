angular.module("ContenteManagerApp")
	.controller("videosconsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "activo";
		$scope.submenu = "admin"
		$scope.titulo_seccion = "Consulta videos";
		$scope.video = {
							"id":"",
							"titulo":"",
							"link":"",
							"estatus":"",
							"number":"",
							"Consulta":"si"

		}
		//
	$scope.consultar_galeria_videos = function(){
		$http.post("./controladores/videosController.php",
		{
			'accion':'consultar_galeria_videos'
		}).success(function(data, status, headers, config){
			alert(data["mensajes"]);
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else{
				$scope.video = data;
				console.log(data);
				setTimeout(function(){
					iniciar_datatable();
				},500);
			}
		}).error(function(data,status){
			console.log(data);
		});
	}
	//--valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}
		});
	}
	//---------------------------------------
/*	$scope.ver_galeria = function(indice){
		serverDataMensajes.puenteData($scope.galeria[indice]);
		$location.path("/galeria");
	}	

	
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}
		});
	}
*/
	//---------------------------------------
	$scope.procesar_activacion_galeria =function(){
		$http.post("./controladores/videosController.php",
		{
			'accion':'modificar_estatus',
			'id': $scope.id_seleccionado_galeria,
			'estatus':$scope.estatus_seleccionado_galeria
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_galeria_videos();
				showSuccess("El registro fue actualizado de manera exitosa");
			}else if(data["mensajes"]=="no_existe_video"){
				showErrorMessage("Ocurrió un error, no existe el video");				
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.ver_video = function(indice){
		cargar_preload();
		serverDataMensajes.puenteData($scope.video[indice]);
		$location.path("/galeria_videos");
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_videos = []
		$scope.estatus_seleccionado_videos = []
		var caja = event.currentTarget.id;
		//alert(caja);
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_galeria = arreglo_atributos[0];
		$scope.estatus_seleccionado_galeria = arreglo_atributos[1];
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//---------------------------------------
	//--Bloque de Eventos
		$scope.sesion_usuario();
		cargar_preload();
		$scope.consultar_galeria_videos();
	//--
	});