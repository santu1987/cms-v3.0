angular.module("ContenteManagerApp",["ngRoute","ngSanitize"])
.config(function($routeProvider,$locationProvider){
	$routeProvider
		.when("/",{
			controller: "MainController",
			templateUrl: "site_media/templates/inicio_sesion.html"
		})
		.when("/panelUs",{
			controller:"entornoController",
			templateUrl : "site_media/templates/panelUs.html"
		})
		.when("/quienes_somos",{
			controller:"quienesSomosController",
			templateUrl:"site_media/templates/quienes_somos.html"
		})
		.when("/consultar_quienes_somos",{
			controller:"quienesSomosConsultasController",
			templateUrl:"site_media/templates/quienes_somos_consulta.html"
		})
		.when("/tipos_negocios",{
			controller:"tiposNegociosController",
			templateUrl:"site_media/templates/tipos_negocios_form.html"
		})
		.when("/detalles_negocios",{
			controller:"detallesNegociosController",
			templateUrl:"site_media/templates/detalles_negocios_form.html"
		})
		.when("/consultar_tipos_negocios",{
			controller:"tiposNegociosconsultasController",
			templateUrl:"site_media/templates/tipos_negocios_consulta.html"
		})
		.when("/consultar_negocios",{
			controller:"detallesNegociosconsultasController",
			templateUrl:"site_media/templates/detalles_negocios_consulta.html"
		})
		.when("/configurar_slider",{
			controller:"sliderConfController",
			templateUrl:"site_media/templates/slider_conf.html"
		})	
		.when("/marcas",{
			controller:"marcasController",
			templateUrl: "site_media/templates/marcas.html"
		})
		.when("/consultar_marcas",{
			controller: "marcasConsultasController",
			templateUrl: "site_media/templates/marcas_consulta.html"
		})		
		.when("/album_fotos",{
			controller:"albumController",
			templateUrl: "site_media/templates/album_fotos.html"
		})
		
		.when("/galeria",{
			controller:"galeriaController",
			templateUrl: "site_media/templates/galeria_form.html"
		})
		.when("/consultar_galeria",{
			controller:"galeriaconsultasController",
			templateUrl: "site_media/templates/galeria_consulta.html"
		})
		.when("/noticias",{
			controller:"noticiasController",
			templateUrl: "site_media/templates/noticias_form.html"
		})
		.when("/consultar_noticias",{
			controller:"noticiasconsultasController",
			templateUrl: "site_media/templates/noticias_consulta.html"
		})
		.when("/galeria_videos",{
			controller:"videosController",
			templateUrl: "site_media/templates/galeria_videos.html"
		})
		.when("/consultar_videos",{
			controller:"videosconsultasController",
			templateUrl: "site_media/templates/videos_consultas.html"
		})
		.when("/usuarios",{
			controller:"usuariosController",
			templateUrl: "site_media/templates/usuarios_form.html"
		})
		.when("/consultar_usuarios",{
			controller:"usuariosconsultasController",
			templateUrl: "site_media/templates/usuarios_consultas.html"
		})
		.when("/album_fotos",{
			controller:"albumController",
			templateUrl: "site_media/templates/album_fotos.html"
		})
		.when("/contactos",{
			controller:"contactosController",
			templateUrl: "site_media/templates/contactos.html"
		})
		.when("/contactos_empleo",{
			controller:"contactosempleoController",
			templateUrl: "site_media/templates/contactos_empleo.html"
		})
		.when("/redes_sociales",{
			controller:"redessocialesController",
			templateUrl: "site_media/templates/redes_sociales_form.html"
		})
		.when("/direccion",{
			controller:"direccionController",
			templateUrl: "site_media/templates/direccion.html"
		})
		.when("/consultar_direccion",{
			controller:"direccionconsultasController",
			templateUrl: "site_media/templates/direccion_consulta.html"
		})
		.when("/correos",{
			controller:"correosController",
			templateUrl: "site_media/templates/correos.html"
		})
		.when("/consultar_correos",{
			controller:"correosconsultasController",
			templateUrl: "site_media/templates/correos_consulta.html"
		})
		.when("/footer",{
			controller:"footerController",
			templateUrl: "site_media/templates/footer_form.html"
		})
		//.when('/user/:userID', {  templateUrl: 'tpl/user.html',  controller: 'UserController' } )
		.otherwise({
	    	redirectTo: '/'
		});
		//$locationProvider.html5Mode(true);
});/*.run(function($rootScope, $route, $location,$http){ //<---- Para bloquear el back botom
   //Bind the `$locationChangeSuccess` event on the rootScope, so that we dont need to 
   //bind in induvidual controllers.

   $rootScope.$on('$locationChangeSuccess', function() {
        $rootScope.actualLocation = $location.path();
    });        

   $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
        if($rootScope.actualLocation === newLocation) {
        	if(newLocation=="/"){
        		//verifico la sesion
        		$http.post("./controladores/fbasicController.php",{ 'accion':'consultar_sesion'})
        			 .success(function(data, estatus, headers, config){
	       			 	if(data["respuesta"]!="cerrado"){
     		        		$location.path("/panelUs");
        			 	}else{
        			 		$location.path("/");
        			 	}
        			 });
        	}
        }
    });
});*/
//---
/*$(window).load(function() {
    $('#preloader').fadeOut('slow');
    $('body').css({'overflow':'visible'});
})*/
//---