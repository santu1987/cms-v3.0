angular.module("ContenteManagerApp")
	.controller("detallesNegociosconsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "negocios";
		$scope.submenu = "admin"
		$scope.menu = "negocios";
		$scope.titulo_seccion = "Consulta detalle de tipo de negocio";
		$scope.detallesNegocios = {
									'id':'',
									'titulo':'',
									'imagen':'',
									'descripcion1':'',
									'descripcion2':'',
									'descripcion3':'',
									'id_imagen':'',
									'estatus':'',
									'tipo_negocio_id':'',
									'tipo_negocio_titulo':'',
									'tipo_negocio':{'id':'','titulo':'','descripcion':''},
									'consulta':'si',
									'id_idioma':''
		}
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.consultar_negocios = function(){
			serverDataMensajes.puenteData("");
			$http.post("./controladores/detallesNegociosController.php",
			{
				'accion':'consultar_negocios'
			}).success(function(data, status, headers, config){
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else{
					$scope.detallesNegocios = data;
					console.log(data)
					//$scope.tipo_negocio.id = $scope.detallesNegocios.tipo_negocio_id
					//$scope.tipo_negocio.titulo = $scope.detallesNegocios.tipo_negocio_titulo 
					setTimeout(function(){
						iniciar_datatable();
					},500);
				}
			}).error(function(data,status){
				console.log(data);
			});
		}
	//---------------------------------
	$scope.ver_negocio = function(indice){
		$scope.detallesNegocios[indice].consulta = "si"
		serverDataMensajes.puenteData($scope.detallesNegocios[indice]);
		$location.path("/detalles_negocios");
	}
	//---------------------------------------
	$scope.procesar_activacion_negocios =function(){
		$http.post("./controladores/detallesNegociosController.php",
		{
			'accion':'modificar_estatus',
			'id': $scope.id_seleccionado_negocios,
			'estatus':$scope.estatus_seleccionado_negocios
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_negocios();
				showSuccess("El registro fue actualizado de manera exitosa");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_negocios = []
		$scope.estatus_seleccionado_negocios = []
		var caja = event.currentTarget.id;
		//alert(caja);
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_negocios = arreglo_atributos[0];
		$scope.estatus_seleccionado_negocios = arreglo_atributos[1];
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado_negocios==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}else{
				quitar_preload()
			}
		});
	}
	//
	$scope.ver_folleto = function(num){
		if($scope.detallesNegocios[num].folleto!=""){
			$("#form_consulta_negocio").attr("action",$scope.detallesNegocios[num].folleto);
			$("#form_consulta_negocio").submit();
		}
	}
	//---------------------------------------
	//--Bloque de Eventos
		$scope.sesion_usuario();
		cargar_preload()
		$scope.consultar_negocios();
	//--
	});