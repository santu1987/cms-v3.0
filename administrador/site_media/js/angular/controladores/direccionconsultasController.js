angular.module("ContenteManagerApp")
	.controller("direccionconsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "contactos";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Consulta dirección";
		$scope.titulo_seccion = "Consulta dirección";
		$scope.activo_img = "inactivo";
		$scope.direccion = {
									"id":"",
									'descripcion':'',
									"telefono":[],
									"estatus":"",
									"consulta":""			
		}
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.consultar_direccion = function(){
			serverDataMensajes.puenteData("");
			$http.post("./controladores/direccionController.php",
			{
				'accion':'consultar_direccion'
			}).success(function(data, status, headers, config){
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else{
					$scope.direccion = data;
					console.log($scope.direccion);
					setTimeout(function(){
						iniciar_datatable();
					},500);
				}
			}).error(function(data,status){
				console.log(data);
			});
		}
	//---------------------------------
	$scope.ver_direccion = function(indice){
		$scope.direccion[indice].consulta = "si"
		serverDataMensajes.puenteData($scope.direccion[indice]);
		$location.path("/direccion");
	}
	//---------------------------------------
	$scope.procesar_activacion_direccion =function(){
		$http.post("./controladores/direccionController.php",
		{
			'accion':'modificar_estatus',
			'id': $scope.id_seleccionado_direccion,
			'estatus':$scope.estatus_seleccionado_direccion
		}).success(function(data, status, headers, config){
			console.log(data);
			if(data["error"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_direccion();
				showSuccess("El registro fue actualizado de manera exitosa");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_direccion = []
		$scope.estatus_seleccionado_direccion = []
		var caja = event.currentTarget.id;
		//alert(caja);
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_direccion = arreglo_atributos[0];
		$scope.estatus_seleccionado_direccion = arreglo_atributos[1];
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado_direccion==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}else{
				quitar_preload()
			}
		});
	}
	//---------------------------------------
	//--Bloque de Eventos
		$scope.sesion_usuario();
		cargar_preload()
		$scope.consultar_direccion();
	//--
	});