angular.module("ContenteManagerApp")
	.controller("sliderConfController", function($scope,$compile,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory){
		//--Cuerpo de delcaraciones
		$scope.menu = "slider";
		$scope.titulo_pagina = "Configurar slider";
		$scope.activo_img = [];
		$scope.slider = {
								'id':[],
								'imagen':[],
								'titulo':[],
								'descripcion':[],
								'idioma':""
		}
		$scope.currentTab = 'slider'
		$scope.imagen_seleccionada = []
		$scope.seccion = ''
		$scope.numero = ''
		$scope.operacion = "registrar";
		$scope.clase = []
		$scope.lista_orden = []
		$scope.cuantos = 0
		//---

		//--Cuerpo de metodos
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---
		$scope.seleccione_img_slider = function(number){
			$("#modal_img").modal("show");
			$scope.numero = number
			$("#numero").val(number)
			//alert($scope.numero);
		}
		//---
		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			//$scope.borrar_imagen.push(id_imagen);
			$scope.slider.imagen = []
			$scope.slider.imagen.push(ruta);
			//alert($scope.slider.imagen);
			$scope.numero = $("#numero").val();

			$("#div_imagen_"+$scope.numero).html($compile("<img ng-src='"+$scope.slider.imagen+"' class='img_view' dat='"+$scope.slider.imagen+"'>")($scope)).fadeIn("slow");  	

			$("#contenedor_slider_"+$scope.numero).removeClass("img_slider")
			//--
			$("#modal_img").modal("hide");
			//--
		}
		//---
		$scope.consultar_galeria = function(){
			$scope.galeria = "";
			galeriaFactory.valor_id_categoria('1');//1 ya que es doctores
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galeria=data;
				console.log($scope.galeria);				
			});
		}
		//---
		$scope.quitar_slider = function(){
			if(parseInt($("#valor_slider").val())>1){
				var ene = $("#valor_slider").val()
				$(".fila_"+ene).remove()
				var number = parseInt($("#valor_slider").val())-1;
				$("#valor_slider").val(number)	
				//--
				//-Actualiza lista orden
				$scope.actualiza_lista_orden();
				//-- 
			}else{
				showErrorMessage("Debe agregar al menos 2 slides");
			}	
		}
		//---
		$scope.agregar_slider = function(){
			
			//$("#super_contenedor").append("<imagen_slider></imagen_slider>")
		    
		    var number = parseInt($("#valor_slider").val())+1;
		   // alert(number);
		    if(number<=5){
		    	var super_fila = "fila_"+number
		  		$("#valor_slider").val(number)
				var imagen_slider ="<div id='fila_"+number+"' class='fila_slider col-lg-12 col-md-12 col-s-12 col-md-12 fila_"+number+"' style='padding-bottom: 10px;'>\
									<div class='btn_order'><div id='btn_up_"+number+"' class='btn_up' ng-click='fila_up_down("+number+",1)' ><i class='fa fa-sort-asc' style='font-size: 24pt;' aria-hidden='true'></i></div>\
									<div id='btn_down_"+number+"' class='btn_down' ng-click='fila_up_down("+number+",2)'><i class='fa fa-sort-desc' style='font-size: 24pt;' aria-hidden='true'></i></div></div>\
									<div class='col-lg-3 col-md-3 col-xs-12 col-sm-12' data='fila_"+number+"'>\
										<div class='form-group'>\
											<label>Título</label>\
											<input type='text' name='titulo_slider' id='titulo_slider_"+number+"' class='form-control titulo' placeholder='Ingrese el título'>\
										</div>\
									</div>\
									<div class='col-lg-3 col-md-3 col-xs-12 col-sm-12'>\
										<div class='form-group'>\
											<label>Descripción</label>\
											<div name='descripcion_slider' id='descripcion_slider_"+number+"' class='div_wisig descripcion' ng-click='wisi_modal_auto(0,"+number+")' >Pulse aquí para ingresar el contenido a cerca de quienes somos\
											</div>\
										</div>\
									</div>\
									<div class='col-lg-4 col-md-4 col-xs-12 col-sm-12'>\
										<div>\
											<div class='img_slider' id='contenedor_slider_"+number+"' ng-click='seleccione_img_slider("+number+")'>\
												<div id='div_imagen_"+number+"' class='imagen'>\
													<span><i class='fa fa-picture-o' aria-hidden='true'></i> Imagen Slider </span>\
												</div>\
												<div style='clear:both'></div>\
											</div>\
										</div>\
									</div>\
								</div>";
				    $("#super_contenedor").append($compile(imagen_slider)($scope)).fadeIn("slow"); 
				    //-Actualiza lista orden
				    $scope.actualiza_lista_orden();
				    //-- 	
		    }else{
		    	showErrorMessage("No puede agregar más de 5 slides");
		    }
		    //<textarea name="descripcion_slider" id="descripcion_slider_'+number+'" class="form-control descripcion" style="resize:none" placeholder="ingrese la descripción"></textarea>\
		  
		}
		//--
		//Metodo que actualiza el orden de lA LISTA
		$scope.actualiza_lista_orden = function(){
			$scope.lista_orden = []
			$(".fila_slider").each(function(index,value){
				$scope.lista_orden.push($(this).attr("id"));
				console.log($scope.lista_orden);

			});	
		}
		//--Para los btn up y down
		$scope.fila_up_down = function(fila2,opcion){
			//alert("Fila:"+fila+", Opcion:"+opcion);
			//-Obtengo la posicion
			var fila = "fila_"+fila2
			var pos = $scope.lista_orden.indexOf(fila);
				//alert(" fila:"+fila+" pos:"+pos)
				//--DOWN
				if(opcion=="2"){
					//--
					ultimo = $scope.lista_orden.length - 1;
					if(pos==ultimo){
						showErrorMessage("No puede mover hacia abajo el último slide");
					}else if(pos>-1){
						pos2 = pos + 1;
						$("."+fila).insertAfter("."+$scope.lista_orden[pos2]).fadeIn('slow');
						$scope.actualiza_lista_orden();
					}
				//--UP
				}else if(opcion == 1){
					if(pos==0){
						showErrorMessage("No puede mover hacia arriba el primer slide");
					}else if(pos>-1){
						pos2 = pos - 1;
						$("."+fila).insertBefore("."+$scope.lista_orden[pos2]).fadeIn('slow');
						$scope.actualiza_lista_orden();
					}
					//--
				}
		}
		//---
		$scope.wisi_modal_auto = function(descripcion,numero){
			//---
			$(".trumbowyg-editor").html("")
			if(descripcion!="0"){
				$(".trumbowyg-editor").html(descripcion)
			}			
			$("#numero").val(numero);
			$scope.descripcion = descripcion;
			$("#modal_contenido").modal("show")
			//---			
		}
		//---
		$scope.agregar_contenido = function(){
			numero = $("#numero").val()
			$("#descripcion_slider_"+numero).html($(".trumbowyg-editor").html())
		}
		//---
		$scope.registrar_slider = function(){
			$scope.recorrer_form_slider();
			//----
			if($scope.validar_form()==true){
				uploader_reg("#mensaje_slider",".titulo,.descripcion,.imagen");
				//Para guardar
				if($scope.operacion!="registrar"){
					//alert("modificar");
					$scope.modificar_slider();	
				}else{
					//alert("insertar");
					$scope.insertar_slider();
				}
			}
			//----
		}
		//---
		//--------------------------------------
		$scope.recorrer_form_slider = function(){
			var data
			var imagen_vector = new Array()
			var titulo_vector = new Array()
			var descripcion_vector = new Array()
			//--Recorro imagenes
			$(".img_view").each(function(index){
				url = $(this).attr("dat")
				imagen_vector.push(url)
			});
			//--Recorro titulo
			$(".titulo").each(function(index){
				titulo = $(this).val()
				titulo_vector.push(titulo)
			});
			//--Recorro descripcion
			$(".descripcion").each(function(index){
				descripcion = $(this).html()
				descripcion_vector.push(descripcion)
				$scope.cuantos = $scope.cuantos+1
			});
			//--
			$scope.slider.titulo = titulo_vector
			$scope.slider.imagen = imagen_vector
			$scope.slider.descripcion = descripcion_vector
			
			console.log($scope.slider)
		}
		//--------------------------------------
		$scope.insertar_slider = function(){
			//$scope.recorrer_form_slider()
			$http.post("./controladores/sliderConfController.php",
			{
				 'slider': $scope.slider,
				 'accion':'registrar_slider',
				 'idioma':$scope.select_idioma.id

			}).success(function(data, estatus, headers, config){
					console.log(data);
					if(data["mensajes"]!="error"){
						showSuccess("El registro fue realizado de manera exitosa");
						//$("#form_slider").html(data);
 						$("#form_slider").html($compile(data)($scope)).fadeIn("slow");  	
						$(".img_slider").removeClass("img_slider")
						$scope.operacion = "modificar";
					}
					else{
						showErrorMessage("Ocurrió un error inesperado");
					}
					desbloquear_pantalla("#mensaje_slider",".trumbowyg-editor");
				}).error(function(data,estatus){
					console.log(data);
				});
		}
		//---
		$scope.modificar_slider = function(){
			//$scope.recorrer_form_slider()
			$http.post("./controladores/sliderConfController.php",
			{
				 'slider': $scope.slider,
				 'accion':'modificar_slider',
				 'idioma':$scope.select_idioma.id

			}).success(function(data, estatus, headers, config){
					if(data["mensajes"]!="error"){
						showSuccess("La modificación fue realizada de manera exitosa");
						//$("#form_slider").html(data);
					    $("#form_slider").html($compile(data)($scope)).fadeIn("slow");  	
						$(".img_slider").removeClass("img_slider")
						$scope.operacion = "modificar";
					}
					else{
						showErrorMessage("Ocurrió un error inesperado");
					}
					desbloquear_pantalla("#mensaje_slider",".trumbowyg-editor");
				}).error(function(data,estatus){
					console.log(data);
				});
		}
		//---
		$scope.consultar_slider = function(event){
			$scope.activo_img = [];
			$scope.slider = {
								'id':[],
								'imagen':[],
								'titulo':[],
								'descripcion':[],
			}
				$http.post("./controladores/sliderConfController.php",
				{
					 'accion':'cargar_lista_slider',
					 'idioma':$scope.select_idioma.id

				}).success(function(data, estatus, headers, config){
					if(data["mensajes"]!="error"){
							if((data!=undefined) && (data["mensajes"]!="0"))
							{	
								//------------------------------------------------------
								$("#form_slider").html($compile(data)($scope)).fadeIn("slow");
								$(".img_slider").removeClass("img_slider")
								$scope.operacion = "modificar";
								$("#valor_slider").val($(".div_slider").size());
								$scope.recorrer_form_slider();
								$scope.actualiza_lista_orden();
								$("#btn_agregar,#btn_quitar").removeClass("invisible").addClass("visible");
								//------------------------------------------------------
							} 
							//--
							if(($scope.cuantos==0)||(data["mensajes"]=="0")){
								$scope.operacion = "registrar";
								$("#super_contenedor").html("");
								$scope.agregar_slider();
							}
						}
						else{
							showErrorMessage("Ocurrió un error inesperado");
						}
						desbloquear_pantalla("#mensaje_slider",".trumbowyg-editor");
					}).error(function(data,estatus){
						console.log(data);
					});
		}
		//---
		$scope.validar_form = function(){
			var valida1 = 0;
			var valida2 = 0;
			var valida3 = 0;
			//-----------------------------------
			//recorro todas las imagenes ara validar las imagenes
			if($("#valor_slider").val()!=$scope.slider.imagen.length){
				 showErrorMessage("No pueden haber imagenes en blanco");
			     valida1 = "1";				
				 return false;	
			}
			//Reccorro los títulos
			$(".titulo").each(function(index){
				titulo = $(this).val()
				if(titulo ==""){
			       showErrorMessage("No pueden haber títulos en blanco");
			       valida2 = "1";				
				   return false;	
			   }
			});
			//--Recorro descripcion
			$(".descripcion").each(function(index){
				descripcion = $(this).html()
				descripcion_vector_se = descripcion.trim();
				if(descripcion_vector_se=="Pulse aquí para ingresar el contenido a cerca de quienes somos"){
					descripcion = "";
				}
				if(descripcion ==""){
			       showErrorMessage("No pueden haber descripciones en blanco");				
				   valida3 = "1";
				   return false;	
			   }
			});
			//-----------------------------------
			if((valida1 ==0)&&(valida2 ==0)&&(valida3 ==0)){
				return true;
			}
			//------------------------------------
			
		}
		//---
		//---
		//---Ver el slider
		$scope.ver_slider = function(){
				
				$scope.clase[0]= "active";
				$scope.clase[1] = "";
				setTimeout(function(){
					$("#div_carousel_0").addClass("active");
				},100)
				$("#modal_slider").modal("show");
				$("#opcion_0").addClass("active");
				
		}
		//---Limpia arreglo de DataMensajes de otros controllers
		$scope.opcion_menu = function(opcion){
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		//----------------------------------------
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//------------------------------------------
		$scope.consultar_idioma = function(){
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data
				console.log($scope.idioma)
			});
			
		}
		//------------------------------------------
		//--Cuerpo de llamados
		$scope.sesion_usuario();
  		$scope.consultar_idioma();
		//$scope.consultar_slider(); 
		$scope.consultar_galeria();
		cargar_preload();
	});