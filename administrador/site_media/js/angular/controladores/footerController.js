angular.module("ContenteManagerApp")
	.controller("footerController", function($scope,$compile,$http,$location,serverDataMensajes,sesionFactory,redesFactory){
		$scope.menu = "contactos";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Footer";
		$scope.activo_img = "inactivo";
		$scope.footer = {
									"id":"",
									'descripcion':'',
									'correo':'',
									'rif':'',
									'consulta':''			
		}
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.cuantos = 1
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
			if($scope.valor_intermedio.lenght!=0){
				$scope.footer = $scope.valor_intermedio;
				if($scope.footer.id==undefined){
					$scope.footer.id = "";
				}
				
				//---
			}
		//--Para registrar direcciones
		$scope.registrar_footer = function(){
			if($scope.validar_form()==true){
			//-----------------------------------	
				//uploader_reg("#mensaje_direccion","#direccion,.tlf_direccion");
				//Para guardar
				if(($scope.footer.id!="")&&($scope.footer.id!=undefined)){
					$scope.modificar_footer();	
				}else{
					$scope.insertar_footer();
				}
			//-----------------------------------	
			}
		}
		//--
		$scope.validar_form = function(){
			//alert(validado_telefono+"=="+$scope.cuantos)
			if($scope.footer.descripcion==""){
				showErrorMessage("Debe ingresar la descripción");	
			   	  return false;	
			}else{
				return true;
			}
		}
		//--
		$scope.consultar_footer = function(){
			$http.post("./controladores/footerController.php",
			{
				'accion':'consultar_footer',
			}).success(function(data, status, headers, config){
				console.log(data);
				if(data){				
					$scope.footer = data[0];
				}
			}).error(function(data,status){
				console.log(data);
				showErrorMessage("Ocurrió un error inesperado: "+ data);
			});
		}
		//--
		$scope.insertar_footer = function(){
			$http.post("./controladores/footerController.php",
			{
				'accion':'registrar_footer',
				'id': $scope.footer.id,
				'descripcion':$scope.footer.descripcion,
				'correo':$scope.footer.correo,
				'rif':$scope.footer.rif
			}).success(function(data, status, headers, config){
				console.log(data);
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}
				else if(data["mensajes"]=="registro_procesado"){
					showSuccess("El registro fue realizado de manera exitosa");
					$scope.limpiar_direccion();
				}
				else if(data["mensajes"]=="modificacion_procesada"){
					showSuccess("El registro fue actualizado de manera exitosa");
					$scope.limpiar_direccion();
				}
			}).error(function(data,status){
				console.log(data);
				showErrorMessage("Ocurrió un error inesperado: "+ data);
			});
		}
		//
		$scope.modificar_footer = function(){
			$http.post("./controladores/footerController.php",
			{
				'accion':'modificar_footer',
				'id': $scope.footer.id,
				'descripcion':$scope.footer.descripcion,
				'correo':$scope.footer.correo,
				'rif':$scope.footer.rif
			}).success(function(data, status, headers, config){
				console.log(data);
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else
				if(data["mensajes"]=="no_existe"){
					showErrorMessage("El registro no existe");
				}
				else if(data["mensajes"]=="modificacion_procesada"){
					showSuccess("El registro fue actualizado de manera exitosa");
					$scope.limpiar_direccion();
				}
			}).error(function(data,status){
				console.log(data);
				showErrorMessage("Ocurrió un error inesperado: "+ data);
			});
		}
		//
		$scope.limpiar_direccion = function(){
			$scope.direccion = {
									"id":"",
									'descripcion':'',
			}
			$scope.cuantos = 1;

		}
		
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//Bloque de llamados
		$scope.consultar_footer();
		$scope.sesion_usuario();
	});	