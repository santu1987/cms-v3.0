angular.module("ContenteManagerApp")
	.controller("marcasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory){
		//Cuerpo declaraciones
		$scope.menu = "activo";
		$scope.menu = "empresa";
		$scope.titulo_pagina = "Marcas";
		$scope.activo_img = "inactivo";
		$scope.marcas = {
								'id':'',
								'descripcion':'',
								'marcas_soportes':'',
								'id_soportes':'',
								'idioma':'',
								'estatus':''
		}
		$scope.imagenes_marca = []
		$scope.borrar_imagen = []
		$scope.galeria_marca = []
		$scope.borrar_imagen_soportes = []
		$scope.activo_img_soportes = "inactivo"
		$scope.path_imagen_seleccionada = []
		$scope.galery = []
		$("#contenido_descripcion").trumbowyg();
		//Cuerpo de metodos
		//---
		//Si el form viene de consulta se alimenta del servicio...
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
		if($scope.valor_intermedio.consulta=="si"){

			//Para estatus
			if($scope.valor_intermedio.lenght!=0){
				$scope.marcas = $scope.valor_intermedio;
				//--
				if($scope.marcas.imagen!=undefined){
					$scope.activo_img='activo'
				}
				$scope.marcas.descripcion ? $("#div_descripcion_marcas").html($scope.marcas.descripcion) : $("#div_descripcion_marcas").html("")
				//--
				if($scope.marcas.id==undefined){
					$scope.marcas.id = "";
				}
				//--
				arreglo_marca = $scope.valor_intermedio.marcas_soportes
				$scope.galeria_marca = arreglo_marca.split(",")

				arreglo_id = $scope.valor_intermedio.id_soportes;
				$scope.borrar_imagen_soportes = arreglo_id.split(",")

				$scope.galeria_marca.length>0 ? $scope.activo_img_soportes = "activo" : $scope.activo_img_soportes = "inactivo"
				//$scope.galeria_marca = $scope.valor_intermedio.marcas_soportes;
				//$scope.borrar_imagen_soportes = $scope.valor_intermedio.id_soportes;
				//--

			}
			if($scope.marcas.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.marcas.estatus = "1";
			}else if($scope.marcas.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.marcas.estatus = "0";
			}
			//Para el idioma
			$scope.marcas.idioma.id = $scope.valor_intermedio.id_idioma
			$('#idioma > option[value="'+$scope.marcas.idioma.id+'"]').attr('selected', 'selected');
			$scope.valor_intermedio.consulta = ""
		}else{
			if($scope.marcas.id==undefined){
					$scope.marcas.id = "";
			}
		}
		//---
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---
		$scope.seleccione_img_marcas = function(){
			$("#modal_img2").modal("show");

			//
			//console.log("Inicio"+$scope.doctores_soportes);
			var arreglo_marcas = $scope.galeria_marca;
			//arreglo_soportes = arreglo_soportes.split("|"); 
			console.log( $scope.galeria_marca)
			var galeria_soporte = $scope.galeria_marca
			var galeryx = new Array();
			//Armo arreglo a consultar
			for (i=0;i<galeria_soporte.length;i++){
				galeryx[i] = galeria_soporte[i];
			}
			//$scope.borrar_imagen_soportes.lenght = 0;//manejo los id
			//$scope.doctores_soportes. length = 0; //maneja las rutas
			console.log(arreglo_marcas)
			//Marco c/u
			for(j=0;j<arreglo_marcas.length;j++){
				//alert("El arreglo:"+j+"Cuantos:"+arreglo_soportes.length+" "+arreglo_soportes[j]);
				posicion = galeryx.indexOf(arreglo_marcas[j]);
				//--Limpio el scope...
				$scope.seleccionar_imagen_soportes_individual("img_soporte"+posicion);
			}
		}
		//---
		$scope.seleccionar_imagen_soportes_individual = function(imagen){
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			//--
			$("#"+imagen).addClass("marcado");
			if($scope.borrar_imagen_soportes.indexOf(id_imagen)==-1){
				$scope.borrar_imagen_soportes.push(id_imagen);//manejo los id
			}
			if($scope.galeria_marca.indexOf(ruta)==-1){
				$scope.galeria_marca.push(ruta); //maneja las rutas
			}	
			$scope.activo_img_soportes = "activo"				
			//--
		}
		//---
		$scope.limpiar_arreglos = function(){
			$scope.borrar_imagen_soportes = []
			$scope.galeria_marca = []
			$(".imgbiblioteca").removeClass("marcado");
			console.log($scope.galeria_marca)
			console.log($scope.borrar_imagen_soportes)
		}
		//---
		$scope.seleccionar_imagen_soportes = function(event){

		//-------------------------------------------------
		var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			if(($("#"+imagen).hasClass("marcado"))==true){
				$("#"+imagen).removeClass("marcado");
				//alert("al iniciar:"+$scope.borrar_imagen_soportes)
				//Limpia los ids
				$indice = $scope.borrar_imagen_soportes.indexOf(id_imagen);
				$scope.borrar_imagen_soportes.splice($indice,1);
				//Limpio las rutas
				$indice_ruta = $scope.galeria_marca.indexOf(ruta);
				$scope.galeria_marca.splice($indice_ruta,1);
				//alert("al borrar:"+$scope.borrar_imagen_soportes)
				//
				if($scope.galeria_marca.length==0){
					$scope.activo_img_soportes = "inactivo"
				}
			}else{
				$("#"+imagen).addClass("marcado");
				$scope.borrar_imagen_soportes.push(id_imagen);//manejo los id
				$scope.galeria_marca.push(ruta); //maneja las rutas
				$scope.activo_img_soportes = "activo"				
			}
			console.log($scope.galeria_marca)
			console.log($scope.borrar_imagen_soportes)
		//-------------------------------------------------	
			//$scope.conf_masonrry();
		}
		//--
		$scope.validar_form = function(){
			if($scope.marcas.descripcion==""){
				showErrorMessage("Debe incluir la descripción!");				
				return false;
			}else if($scope.borrar_imagen_soportes.length==0){
				showErrorMessage("Debe seleccionar al menos una imagen");				
				return false;
			}
			else{
				return true;
			}	
		}
		//--
		$scope.registrar_marca = function(){
			if($scope.validar_form()==true){
				uploader_reg("#mensaje_galeria","#div_descripcion_marcas");
				//Para guardar
				//alert("id_personas:"+$scope.doctor.id_personas);
				//alert("id_doctor:"+$scope.doctor.id);
				if(($scope.marcas.id!=undefined)&&($scope.marcas.id!="")){
					$scope.modificar_marca();	
				}else{
					$scope.insertar_marca();
				}		
			}
		}
		//---
		$scope.insertar_marca = function(){
			console.log($scope.borrar_imagen_soportes);
			$http.post("./controladores/marcasController.php",
			{

				 'descripcion':$scope.marcas.descripcion, 	
				 'id_soportes_marca':$scope.borrar_imagen_soportes,
				 'id_idioma':$scope.marcas.idioma.id,
				 'accion':'registrar_marca'

			}).success(function(data, estatus, headers, config){
							if(data["mensajes"]=="registro_procesado"){
								showSuccess("El registro fue realizado de manera exitosa");
								$scope.limpiar_cajas_marcas();
							}else{
								showErrorMessage("Ocurrió un error inesperado");
							}
						desbloquear_pantalla("#mensaje_galeria","#div_descripcion_marcas")
						}).error(function(data,estatus){
							console.log(data);
			});	
		}
		//---
		$scope.modificar_marca = function(){
			$http.post("./controladores/marcasController.php",
			{
				 'id':$scope.marcas.id,	
				 'descripcion':$scope.marcas.descripcion, 	
				 'id_soportes_marca':$scope.borrar_imagen_soportes,
 				 'id_idioma': $scope.marcas.idioma.id,
				 'estatus':$scope.marcas.estatus,
				 'accion':'modificar_marca'

			}).success(function(data, estatus, headers, config){
							if(data["mensajes"]=="modificacion_procesada"){
								showSuccess("El registro fue actualizado de manera exitosa");
								$scope.limpiar_cajas_marcas();
							}else if(data["mensajes"]=="no_existe_marca"){
								showErrorMessage("La marca no existe");
							}
							else{
								showErrorMessage("Ocurrió un error inesperado");
							}
						desbloquear_pantalla("#mensaje_galeria","#div_descripcion_marcas")
						}).error(function(data,estatus){
							console.log(data);
			});	
		}
		//---
		/*$scope.consultar_marcas = function(){
			$http.post("./controladores/marcasController.php",
			{
				 'accion':'consultar_marca'

			}).success(function(data, estatus, headers, config){
				console.log(data);
				//Para marca
				$scope.marcas = data[0]
				if($scope.marcas!=undefined){
				//-------------------------------
					console.log($scope.marcas)
					$("#div_descripcion_marcas").html($scope.marcas.descripcion)
					//Para soportes
					if($scope.marcas.id_soportes){
						var arreglo_id_soportes = new Array();
						arreglo_id_soportes = $scope.marcas.id_soportes.split("|");
						for(i=0;i<arreglo_id_soportes.length;i++){
							$scope.borrar_imagen_soportes.push(arreglo_id_soportes[i]);	
						}				
					}
					//Para soportes rutas
					if($scope.marcas.marcas_soportes){
						var arreglo_soportes = new Array();
						arreglo_soportes = $scope.marcas.marcas_soportes.split("|");
						for(i=0;i<arreglo_soportes.length;i++){
							//alert("aqui"+arreglo_soportes[i]);
							$scope.galeria_marca.push(arreglo_soportes[i]);	
						}				
						$scope.activo_img_soportes = "activo"
					}
					//
					console.log($scope.marcas.marcas_soportes);
					//Asigno valores a los div
					//-------------------------	
				}else{
					$scope.marcas = {
								'id':'',
								'descripcion':'',
								'marcas_soportes':'',
								'id_soportes':'',
								'id_idioma':'',
								'estatus':''
					}
				}
				
			}).error(function(data,estatus){
					console.log(data);
			});
		}*/
		//--
		$scope.consultar_marcas =  function(){
			setTimeout(function(){
				$scope.limpiar_cajas_marcas();
			},500)
			$location.path("/consultar_marcas");
		}
		//--
		$scope.limpiar_cajas_marcas = function(){
			$scope.marcas = {
								'id':'',
								'descripcion':'',
								'marcas_soportes':'',
								'id_soportes':'',
								'idioma':'',
								'estatus':''
			}
			$scope.limpiar_arreglos();
			$scope.activo_img_soportes = "inactivo"
			$(".trumbowyg-editor").html("");
			$("#div_descripcion_marcas").text("Pulse aquí para ingresra el contenido de quienes somos");
		}	
		//---
		$scope.wisi_modal = function(){
			//---
			$(".trumbowyg-editor").html($scope.marcas.descripcion)
			$("#modal_contenido").modal("show")
		}	
		$scope.agregar_contenido = function(){
			$scope.marcas.descripcion = $(".trumbowyg-editor").html()
			$("#div_descripcion_marcas").html($scope.marcas.descripcion)
		}	
		//----------------------------------------
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//---Limpia arreglo de DataMensajes de otros controllers
		$scope.opcion_menu = function(opcion){
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		//---------------------------------------------------------------
		$scope.consultar_galeria = function(){
			$scope.galeria_m = "";
			galeriaFactory.valor_id_categoria('2');//1 ya que es doctores
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galeria_m=data;
				console.log($scope.galeria_m);				
			});
		}
		//---------------------------------------------------------------
		$scope.consultar_idioma = function(){
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data
				console.log($scope.idioma)
			});
		}
		//---------------------------------------------------------------
		$scope.activar_check = function(caja){
			if($(caja).hasClass("fa-check-square-o")){
				$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.marca.estatus = "0";	
			}else
			if($(caja).hasClass("fa-square-o")){
				$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.marca.estatus = "1";	
			}			
		}
		//---------------------------------------------------------------
		//Cuerpo de Llamados a metodos
		$scope.sesion_usuario();
		$scope.consultar_galeria();
		$scope.consultar_idioma();
		cargar_preload();
		//---------------------------------------------------------
	});