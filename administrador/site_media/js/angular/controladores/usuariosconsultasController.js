angular.module("ContenteManagerApp")
	.controller("usuariosconsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "configuracion";
		$scope.submenu = "usuarios"
		$scope.titulo_seccion = "Consulta usuarios";
		$scope.usuario = {
							"id":"",
			 				"cedula":"",
			 				"login":"",
			 				"nombres_apellidos":"",
			 				"telefono":"",
			 				"email":"",
			 				"tipos_usuarios":{"id":"","descripcion":""},
			 				"clave":"",
			 				"id_persona":"",
			 				"estatus":"",
			 				"consulta":""
		}
	//----------------------------------------------------------
	//datos sesion
	$scope.nombre_usuario = "";
	$scope.login_usuario = "";
	$scope.sesion_usuario = function(){
		sesionFactory.datos_sesion(function(data){
			if(data["nombre_usuario"]!=""){
				$scope.nombre_usuario = data["nombre_usuario"];
				$scope.login_usuario = data["login_usuario"];
			}
		});
	}
	$scope.sesion_usuario();
	//---

	$scope.consultar_usuarios = function(){
		serverDataMensajes.puenteData("");
		$http.post("./controladores/usuariosController.php",
		{
			'accion':'consultar_usuarios'
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else{
				$scope.usuario = data;
				console.log(data)
				setTimeout(function(){
					iniciar_datatable();
				},500);
			}
		}).error(function(data,status){
			console.log(data);
		});
	}
	//-----------------------------------------
	$scope.ver_usuarios = function(indice){
		$scope.usuario[indice].consulta = "si"
		serverDataMensajes.puenteData($scope.usuario[indice]);
		cargar_preload();
		mostrar_preload();
		$location.path("/usuarios");
	}
	//-----------------------------------------
	$scope.procesar_activacion_usuarios =function(){
		$http.post("./controladores/usuariosController.php",
		{
			'accion':'modificar_estatus',
			'id_persona': $scope.id_seleccionado_usuario,
			'estatus':$scope.estatus_seleccionado_usuario
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_usuarios();
				showSuccess("El registro fue actualizado de manera exitosa");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_usuario = []
		$scope.estatus_seleccionado_usuario = []
		var caja = event.currentTarget.id;
		//alert(caja);
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_usuario = arreglo_atributos[0];
		$scope.estatus_seleccionado_usuario = arreglo_atributos[1];
		
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}
		});
	}
	//---------------------------------------
	//--Bloque de Eventos
	    $scope.sesion_usuario();
		cargar_preload()
		$scope.consultar_usuarios();
	//--
	});