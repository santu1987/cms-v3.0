angular.module("ContenteManagerApp")
	.controller("albumController", function($scope,$http,$location,categoriasFactory,galeriaFactory,upload,sesionFactory,serverDataMensajes){
		$scope.titulo_pagina = "Album de Imágenes";
		$scope.menu = "galeria";
		$scope.submenu = "admin"
		$scope.categoria = []
		$scope.galeria = []
		$scope.operacion = ""
		$scope.categoria_fa = ""
		$scope.dropzone_activo = ""
		$scope.caja_valida = ""
		$scope.album = {}
		$scope.files = []
		$scope.borrar_imagen = []
		$scope.album.nombre_imagen = ""
		$scope.album.nombre_archivo = ""
		$scope.album.categoria = [""]
		$(".pace").addClass("pace-active")
		//---------------------------------------
		//--Bloque de funciones
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.cargar_categorias = function(){
			categoriasFactory.cargar_categorias_fa(function(data){
				$scope.categoria = data;
				setTimeout(function(){
					$("#select_categoria").selectpicker('refresh');
				},400);
			});
		}
		//---------------------------------------
		$scope.registrar_album = function (){
			if($scope.validar_form()==true){
				$scope.uploadFile();	
			}
		}
		//----------------------------------------------------------
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//----------------------------------------------------------
		$scope.validar_form = function(){
			if($scope.album.categoria == ""){
				showErrorMessage("Debe indicar una categoría antes de cargar el archivo");				
				return false;
			}else if($scope.album.nombre_imagen==""){
				showErrorMessage("Debe indicar el nombre del archivo");
				return false;
			}else if($scope.file==""){
				showErrorMessage("Debe seleccionar un archivo");
				return false;
			}
			return true;
		}
		//---------------------------------------
		//--Metodo para subir acrhivos
		$scope.uploadFile = function(){
			var file = $scope.file;
			var categoria = $scope.album.categoria.id;
			var nombre_archivo = $scope.album.nombre_imagen;
			uploader_reg("#mensaje_album","#select_categoria,#nombre_imagen,#nombre_archivo");
			//-
			upload.uploadFile(file,categoria,nombre_archivo).then(function(res){
				console.log(res);
				if(res.data!="error_tipo_archivo"){
					showSuccess("La imagen fue cargada de manera exitosa!");
					$scope.consultar_galeria();
					$scope.limpiar_imagen();
					desbloquear_pantalla("#mensaje_album","#select_categoria,#nombre_imagen,#nombre_archivo");
				}else{
					showErrorMessage("Error #5 : Error al subir tipo de archivo, solo puede subir imagenes .jpg");
				}
				//--------------------------------
			});
			//-
		}
		//---------------------------------------
		//--Actualiza las imagenes de la galeria segun la categoria...
		$scope.consultar_galeria = function(){

			$scope.galeria = "";
			galeriaFactory.valor_id_categoria($scope.album.categoria.id);
			galeriaFactory.cargar_galeria_fa(function(data){
					$scope.galeria=data;
					$scope.conf_masonrry();
			});
		}	
		//---------------------------------------
		
		$scope.consultar_imagenes = function(){
			$scope.galeria="";
			galeriaFactory.valor_nombre_imagen($scope.album.nombre_archivo);
			galeriaFactory.cargar_galeria_nombres(function(data){
					$scope.galeria=data;
					$scope.conf_masonrry();
					//console.log($scope.galeria);				
			});
		}
		//---------------------------------------
		$scope.limpiar_imagen = function(){
			$("#list").html("");
			$("#list").html('<img id="previa" class="img_preview img-responsive" src="./site_media/images/precharge.png">');
			$scope.album.nombre_imagen="";
			$("#file").val("");
			//$("#select_categoria").val(0);
			//$("#select_categoria").selectpicker('refresh');
		}
		//-----------------------------------------
		$scope.limpiar_pantalla = function(){
			$scope.limpiar_imagen();
			$scope.album.nombre_archivo="";
			$scope.album.categoria.id = "";
			$scope.consultar_galeria();
			$("#select_categoria").val(0);
			$("#select_categoria").selectpicker('refresh');
			//$scope.conf_masonrry();
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		//-----------------------------------------
		$scope.seleccionar_imagen = function(){
			var imagen = event.target.id;//Para capturar id
			var id_imagen = $("#"+imagen).attr("data");
			if(($("#"+imagen).hasClass("marcado"))==true){
				$("#"+imagen).removeClass("marcado");
				$indice = $scope.borrar_imagen.indexOf(id_imagen);
				$scope.borrar_imagen.splice($indice,1);
			}else{
				$("#"+imagen).addClass("marcado");
				$scope.borrar_imagen.push(id_imagen);
			}
		}
		//-------------------------------------------
		$scope.eliminar_imagen = function(){
			//alert($scope.borrar_imagen.length);
			if($scope.borrar_imagen.length>0){
				mensajes("Desea realmente eliminar esta imagen?");
				$scope.operacion = "Eliminar";
			}else{
				showErrorMessage("Debe seleccionar al menos una imágen para realizar una eliminación");
			}
		}
		//--------------------------------------------
		$scope.procesar_operacion = function(){
			if($scope.operacion=="Eliminar"){
				$scope.borrado();
			}
		}
		//--------------------------------------------
		$scope.borrado = function(){
			$http.post("./controladores/galeriaController.php",
			{
					'imagenes' : $scope.borrar_imagen,
					'accion': 'borrar_imagen'
			}).success(function(data, status, headers, config){
					//alert(data);
					if(data["mensajes"]=="proceso exitoso"){
						showSuccess("El proceso de ha ejecutado de manera satisfactoria");
						$scope.borrar_imagen = [];
						$scope.consultar_galeria();
					}else{
						showErrorMessage("Ocurrió un error inesperado");
					}
					
			}).error(function(data,status){
					//$scope.mensaje_error();
					console.log(data);
			});
		}
		//-------------------------------------------
		$scope.conf_masonrry = function(){
			$(".galeria_img").addClass("animated fadeIn");
            $('.grid').masonry({itemSelector: '.grid-item',percentPosition: true});
			    	setTimeout(function(){
		    			$(".galeria_img").removeClass("animated fadeIn");
		    			if($scope.inicio_masonrry == '1'){
		    				$("#masonrry").masonry( 'destroy' )
		    				$scope.iniciar_masonry();	
		    			}              			
		    		},1000);		    			
		}
		//-------------------------------------------
		$scope.iniciar_masonry = function(){
			setTimeout(function(){
   				$("#masonrry").masonry();
   				$scope.inicio_masonrry = "1";
   			},1000);	
		}
		//---Limpia arreglo de DataMensajes de otros controllers
		$scope.opcion_menu = function(opcion){
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		//-------------------------------------------
		if($scope.caja_valida!=""){
			$scope.caja_valida;
		};
		//---------------------------------------
		//--Bloque de eventos y llamados
		$scope.sesion_usuario();
		$scope.cargar_categorias();
		$scope.consultar_galeria();
		$scope.iniciar_masonry();
		//---------------------------------------
	});
