angular.module("ContenteManagerApp")
	.controller("mensajesconsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "activo";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Consulta mensajes";
		$scope.titulo_seccion = "Consulta de contenido incial";
		$scope.mensajes = {
										"id":"",
										"titulo":"",
										"descripcion":"",
										"estatus":"",
										"number":""
		}		
		//
		$scope.consultar_mensajes = function(){
			serverDataMensajes.puenteData("");
			$http.post("./controladores/mensajeController.php",
			{
				'accion':'consultar_mensajes'
			}).success(function(data, status, headers, config){
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else{
					$scope.mensajes = data;
					setTimeout(function(){
						iniciar_datatable();
					},500);
				}
			}).error(function(data,status){
				console.log(data);
			});
		}
		$scope.ver_mensaje = function(indice){
			serverDataMensajes.puenteData($scope.mensajes[indice]);
			$location.path("/mensaje_inicial");
		}
		//---------------------------------------
		$scope.procesar_activacion = function(){
			$http.post("./controladores/mensajeController.php",
				{
					'accion':'modificar_estatus',
					'id_mensajes': $scope.id_seleccionado,
					'estatus':$scope.estatus_seleccionado
				}).success(function(data, status, headers, config){
					if(data["mensajes"]=="error"){
						showErrorMessage("Ocurrió un error inesperado");
					}else if(data["mensajes"]=="no_existe_mensaje"){
						showErrorMessage("El mensaje no existe");
					}else{
						$scope.consultar_mensajes();
						showSuccess("El registro fue actualizado de manera exitosa");
					}
				}).error(function(data,status){
					console.log(data);
					showErrorMessage("Ocurrió un error inesperado: "+ data);
				});
		}
		//---------------------------------------	
		$scope.activar_registro = function(){
			//$scope.id_seleccionado = "";
			//$scope.estatus_seleccionado = "";
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado = arreglo_atributos[0];
			$scope.estatus_seleccionado = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
			}
			$("#cuerpo_mensaje").text(mensaje);
			$("#modal_mensaje").modal("show");
		}
		//----------------------------------------
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//----------------------------------------
	//---------------------------------------
	//--Bloque de Eventos
		cargar_preload()
		$scope.consultar_mensajes();
		$scope.sesion_usuario();
	//--
	});