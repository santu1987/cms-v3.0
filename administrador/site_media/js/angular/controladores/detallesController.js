angular.module("ContenteManagerApp")
	.controller("detallesNegociosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,tiposNegociosFactory,upload,idiomaFactory){
		$scope.menu = "negocios";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Detalles Negocios";
		$scope.activo_img = "inactivo";
		$scope.currentTab = "datos_basicos"
		$scope.detallesNegocios = {
								'id':'',
								'titulo':'',
								'imagen':'',
								'tipo_negocio':{
												"id":"",
												"titulo":"",
												"descripcion":""},
								'descripcion1':'',
								'descripcion2':'',
								'descripcion3':'',
								'id_imagen':'',
								'estatus':'',
								'consulta':'',
								'idioma':''
		}
		$scope.tipos_negocios = {
									"id":"",
									"titulo":"",
									"descripcion":""
		}
		$scope.opcion = []
		$scope.borrar_imagen = []
		//----------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		//Si el form viene de consulta se alimenta del servicio...
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
		//alert("Consulta"+$scope.valor_intermedio.consulta);
		if($scope.valor_intermedio.consulta=="si"){
			if($scope.valor_intermedio){
				$scope.detallesNegocios = $scope.valor_intermedio;
			}
			if($scope.detallesNegocios.imagen!=undefined){
				$scope.activo_img='activo'
			}
			if($scope.detallesNegocios.descripcion1){
				$("#div_descripcion1").html($scope.detallesNegocios.descripcion1);
			}
			if($scope.detallesNegocios.descripcion2){
				$("#div_descripcion2").html($scope.detallesNegocios.descripcion2);
			}
			if($scope.detallesNegocios.descripcion3){
				$("#div_descripcion3").html($scope.detallesNegocios.descripcion3);
			}
			if($scope.detallesNegocios.id==undefined){
				$scope.detallesNegocios.id = "";
			}
			if($scope.detallesNegocios.tipo_negocio){
				setTimeout(function(){
					$('#tipo_negocio').val($scope.detallesNegocios.tipo_negocio.id);
					$('#tipo_negocio > option[value="'+$scope.detallesNegocios.tipo_negocio.id+'"]').attr('selected', 'selected');
					$("#tipo_negocio").selectpicker('refresh');
				},2500);
			}
			//Para estatus
			if($scope.detallesNegocios.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.detallesNegocios.estatus = "1";
			}else
			if($scope.detallesNegocios.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.detallesNegocios.estatus = "0";
			}
			$scope.detallesNegocios.idioma.id = $scope.valor_intermedio.id_idioma
			$('#idioma > option[value="'+$scope.detallesNegocios.idioma.id+'"]').attr('selected', 'selected');
			//Limpio porque ya consultó.
			$scope.valor_intermedio.consulta="";
		}	
		//------------------------------------------------------------------------------
		//Cuerpo de metodos
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//---Consultar tiposNegocios
		$scope.consultar_tipos_negocios = function(){
			tiposNegociosFactory.valor_estatus('1');//ya que es galeria de imagenes
			tiposNegociosFactory.cargar_tiposNegocios(function(data){
				$scope.tipos_negocios=data;
				console.log($scope.tipos_negocios)
			});
		}
		//---Consultar lista de negocios
		//---
		$scope.consultar_detalles_negocios = function(){
			setTimeout(function(){
				$scope.limpiar_cajas_detalles_negocios();
			},500)
			$location.path("/consultar_negocios");
		}
		//--
		//---
		$scope.wisi_modal = function(opcion){
			//---
			$(".trumbowyg-editor").html("")
			$scope.opcion = opcion
			switch($scope.opcion){
				case "1":
					if($scope.detallesNegocios.descripcion1!=""){
						$(".trumbowyg-editor").html($scope.detallesNegocios.descripcion1)
					}
					break;
				case "2":
					if($scope.detallesNegocios.descripcion2!=""){
						$(".trumbowyg-editor").html($scope.detallesNegocios.descripcion2)
					}
					break;
				case "3":
					if($scope.detallesNegocios.descripcion3!=""){
						$(".trumbowyg-editor").html($scope.detallesNegocios.descripcion3)
					}
					break;			
			}
			//---
			$("#modal_contenido").modal("show")
		}
		//---
		$scope.agregar_contenido = function(){
			switch($scope.opcion){
				case "1":
					$scope.detallesNegocios.descripcion1 = $(".trumbowyg-editor").html()
					$("#div_descripcion1").html($scope.detallesNegocios.descripcion1)
					break;
				case "2":
					$scope.detallesNegocios.descripcion2 = $(".trumbowyg-editor").html()
					$("#div_descripcion2").html($scope.detallesNegocios.descripcion2)	
					break
				case "3":
					$scope.detallesNegocios.descripcion3 = $(".trumbowyg-editor").html()
					$("#div_descripcion3").html($scope.detallesNegocios.descripcion3)	
					break

			}
		}
		//--
		//--Cuerpo de metodos
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");		
		}	
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.valor_id_categoria('5');//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
			});
		}
		//---
		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			/*if(($("#"+imagen).hasClass("marcado"))==true){
				$("#"+imagen).removeClass("marcado");
				$indice = $scope.borrar_imagen.indexOf(id_imagen);
				$scope.borrar_imagen.splice($indice,1);
				$scope.activo_img = "inactivo"
				$scope.noticias.imagen = '';
			}else{
				$("#"+imagen).addClass("marcado");
				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.noticias.id_imagen = id_imagen
				$scope.noticias.imagen = ruta
			}*/
			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.detallesNegocios.id_imagen = id_imagen
			$scope.detallesNegocios.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}
		//---
		//---
		$scope.registrar_detalles_negocios = function(){
			if($scope.validar_form()==true){
				uploader_reg("#mensaje_detalles_negocios","#titulo_detalles_negocios",".trumbowyg-editor");
				//Para guardar
				//alert("id_personas:"+$scope.doctor.id_personas);
				if(($scope.detallesNegocios.id!="")&&($scope.detallesNegocios.id!=undefined)){
					//alert("modificar");
					$scope.modificar_detalles_negocios();	
				}else{
					//alert("insertar");
					$scope.insertar_detalles_negocios();
				}				
			}
		}
		//---
		$scope.insertar_detalles_negocios = function(){
			$http.post("./controladores/detallesNegociosController.php",
			{
				 'id':$scope.detallesNegocios.id,
				 'titulo': $scope.detallesNegocios.titulo,
				 'tipo_negocio':$scope.detallesNegocios.tipo_negocio.id,
				 'id_imagen':$scope.detallesNegocios.id_imagen,
				 'descripcion1':$scope.detallesNegocios.descripcion1,
				 'descripcion2':$scope.detallesNegocios.descripcion2,
				 'descripcion3':$scope.detallesNegocios.descripcion3,
				 'id_idioma':$scope.detallesNegocios.idioma.id,
				 'accion':'registrar_negocios'

			}).success(function(data, estatus, headers, config){
					if(data["mensajes"]=="registro_procesado"){
						$scope.detallesNegocios.id =data["id"];
						//cargo el folleto
						//alert($scope.file);
						if(($scope.file!="")&&($scope.file!=undefined)){
							$scope.uploadFile();
						}else{
							showSuccess("El registro fue realizado de manera exitosa");
							$scope.limpiar_cajas_detalles_negocios();
						}
					}else if(data["mensajes"]=="existe"){
						showSuccess("Ya existe el negocios");
					}else if(data["mensajes"]=="existe_titulo"){
						showSuccess("Ya existe un negocio con ese título");
					}
					else{
						showErrorMessage("Ocurrió un error inesperado");
					}
					desbloquear_pantalla("#mensaje_detalles_negocios","#titulo_detalles_negocios",".trumbowyg-editor");
				}).error(function(data,estatus){
					console.log(data);
			});
		}
		//---
		$scope.modificar_detalles_negocios = function(){
			//alert($scope.detallesNegocios.id_tipo_negocio);
			$http.post("./controladores/detallesNegociosController.php",
			{
				 'id':$scope.detallesNegocios.id,
				 'titulo': $scope.detallesNegocios.titulo,
				 'tipo_negocio':$scope.detallesNegocios.tipo_negocio.id,
				 'id_imagen':$scope.detallesNegocios.id_imagen,
				 'descripcion1':$scope.detallesNegocios.descripcion1,
				 'descripcion2':$scope.detallesNegocios.descripcion2,
				 'descripcion3':$scope.detallesNegocios.descripcion3,
				 'estatus':$scope.detallesNegocios.estatus,
				 'id_idioma':$scope.detallesNegocios.idioma.id,
				 'accion':'modificar_negocios'

			}).success(function(data, estatus, headers, config){
					if(data["mensajes"]=="actualizacion_procesado"){
						$scope.detallesNegocios.id_noticias =data["id"];
						if(($scope.file!="")&&($scope.file!=undefined)){
							$scope.uploadFile();
						}else{
							showSuccess("La actualización fue procesada de manera exitosa");
							$scope.limpiar_cajas_detalles_negocios();
						}						
					}else if(data["mensajes"]=="no_existe"){
						showSuccess("No existe el negocios");
					}else if(data["mensajes"]=="existe_titulo"){
						showSuccess("Ya existe un negocio con ese título");
					}
					else{
						showErrorMessage("Ocurrió un error inesperado");
					}
					desbloquear_pantalla("#mensaje_detalles_negocios","#titulo_detalles_negocios",".trumbowyg-editor");
				}).error(function(data,estatus){
					console.log(data);
			});
		}
		//---
		$scope.validar_form = function(){
			if($scope.detallesNegocios.tipo_negocio.id==""){
				showErrorMessage("Debe indicar el tipo de negocio!");				
				return false;
			}else
			if(($scope.detallesNegocios.titulo=="")||($scope.detallesNegocios.titulo==undefined)){
				showErrorMessage("Debe indicar el título del negocio!");				
				return false;
			}else if(($scope.detallesNegocios.descripcion1=="")||($scope.detallesNegocios.descripcion1==undefined)){
				showErrorMessage("Debe indicar descripción 1!");				
				return false;
			}else if(($scope.detallesNegocios.descripcion2=="")||($scope.detallesNegocios.descripcion2==undefined)){
				showErrorMessage("Debe indicar descripción 2!");				
				return false;
			}/*else if(($scope.detallesNegocios.descripcion3=="")||($scope.detallesNegocios.descripcion3==undefined)){
				showErrorMessage("Debe indicar descripción 3!");				
				return false;
			}*/
			else if(($scope.detallesNegocios.id_imagen=="")||($scope.detallesNegocios.id_imagen==undefined)){
				showErrorMessage("Debe seleccionar al menos una imagen!");				
				return false;
			}
			else{
				return true;
			}
		}
		//---------------------------------------------------------
		$scope.limpiar_cajas_detalles_negocios = function(){
			$scope.detallesNegocios = {
								'id':'',
								'titulo':'',
								'imagen':'',
								'tipo_negocio':{
												"id":"",
												"titulo":"",
												"descripcion":""},
								'descripcion1':'',
								'descripcion2':'',
								'descripcion3':'',
								'id_imagen':'',
								'estatus':'',
								'consulta':'',
								'idioma':''
			}
			$scope.opcion = []
			$scope.borrar_imagen = []
			$scope.activo_img = "inactivo"
			$(".trumbowyg-editor").html("");
			$(".div_wisig").html("Pulse aquí para ingresar el contenido  del negocio");
			$('#tipo_negocio').val("0");
			$('#tipo_negocio > option[value=0]').attr('selected', 'selected');
			$("#tipo_negocio").selectpicker('refresh');
			$("#nombre_archivo").text("");
		}
		//---
		$scope.activar_check = function(caja){
			if($(caja).hasClass("fa-check-square-o")){
				$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.detallesNegocios.estatus = "0";	
			}else
			if($(caja).hasClass("fa-square-o")){
				$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.detallesNegocios.estatus = "1";	
			}			
		}
		//---------------------------------------------------------------
		$scope.consultar_idioma = function(){
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data
				console.log($scope.idioma)
			});
		}
		//---------------------------------------------------------------
		//--Metodo para subir acrhivos
		$scope.uploadFile = function(){
			var file = $scope.file;
			var str = $scope.detallesNegocios.titulo;
			var id_negocio = $scope.detallesNegocios.id;
			var nombre_archivo = "folleto_"+str.replace(' ', '_');
			//uploader_reg("#mensaje_album","#select_categoria,#nombre_imagen,#nombre_archivo");
			//-
			upload.uploadFileFolleto(file,id_negocio,nombre_archivo).then(function(res){
				console.log(res);
				if(res.data!="error_tipo_archivo"){
					showSuccess("El proceso fue realizado de manera exitosa");
					$scope.limpiar_cajas_detalles_negocios();				
					//desbloquear_pantalla("#mensaje_album","#select_categoria,#nombre_imagen,#nombre_archivo");
				}else if(res.data=="no_tamano"){
					showErrorMessage("Proceso ejecutado de manera exitosa, pero el archivo excede el tamaño predefinido para subidas de archivos 10 Mb");				
				}else{
					showErrorMessage("Proceso ejecutado de manera exitosa, pero ocurrió un error al subir tipo de archivo, solo puede subir archivos .pdf, .doc, .docx, .odt");
				}
				//--------------------------------
			});
			//-
		}
		//--
		//---------------------------------------------------------
		//--Cuerpo de llamados
		$scope.sesion_usuario();
		$scope.consultar_galeria_img();
		$scope.consultar_tipos_negocios();
		$scope.consultar_idioma();
		cargar_preload();
	});