angular.module("ContenteManagerApp")
	.controller("noticiasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory){
		//--Cuerpo de delcaraciones
		$scope.menu = "activo";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Noticias";
		$scope.activo_img = "inactivo";
		$scope.noticias = {
								'id_noticias':'',
								'titulo':'',
								'imagen':'',
								'descripcion':'',
								'id_imagen':'',
								'estatus':'',
								'fecha':'',
								'id_usuario':'',
								'nombre_usuario':'',
								'idioma':'',
								'consulta':''
		}
		$scope.borrar_imagen = []
		$scope.valor_intermedio = []
		$scope.galery = []
		$scope.currentTab = 'datos_basicos'
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		//Si el form viene de consulta se alimenta del servicio...
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
		//alert($scope.valor_intermedio.consulta);
		if($scope.valor_intermedio.consulta=="si"){
			//Para estatus
			if($scope.valor_intermedio.lenght!=0){
				$scope.noticias = $scope.valor_intermedio;
				if($scope.noticias.imagen!=undefined){
					$scope.activo_img='activo'
				}
				if($scope.noticias.descripcion){
					$(".trumbowyg-editor,#div_descripcion").html($scope.noticias.descripcion);

				}
				if($scope.noticias.id_noticias==undefined){
					$scope.noticias.id_noticias = "";
				}
			}
			if($scope.noticias.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.noticias.estatus = "1";
			}else
			if($scope.noticias.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.noticias.estatus = "0";
			}
			//Para el idioma
			$scope.noticias.idioma = $scope.valor_intermedio.idioma
			$('#idioma > option[value="'+$scope.noticias.idioma.id+'"]').attr('selected', 'selected');
			$scope.valor_intermedio.consulta = "";
		}	
		
		
		//---
		//--Cuerpo de metodos
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");		
		}	
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.valor_id_categoria('4');//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
			});
		}
		//---
		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			/*if(($("#"+imagen).hasClass("marcado"))==true){
				$("#"+imagen).removeClass("marcado");
				$indice = $scope.borrar_imagen.indexOf(id_imagen);
				$scope.borrar_imagen.splice($indice,1);
				$scope.activo_img = "inactivo"
				$scope.noticias.imagen = '';
			}else{
				$("#"+imagen).addClass("marcado");
				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.noticias.id_imagen = id_imagen
				$scope.noticias.imagen = ruta
			}*/
			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.noticias.id_imagen = id_imagen
			$scope.noticias.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}
		//---
		$scope.registrar_noticias = function(){
			$scope.asignar_model();
			if($scope.validar_form()==true){
				uploader_reg("#mensaje_noticia","#titulo_noticias",".trumbowyg-editor");
				//Para guardar
				//alert("id_personas:"+$scope.doctor.id_personas);
				if(($scope.noticias.id_noticias!="")&&($scope.noticias.id_noticias!=undefined)){
					//alert("modificar");
					$scope.modificar_noticias();	
				}else{
					//alert("insertar");
					$scope.insertar_noticias();
				}
				
			}
		}
		//--
		//---------------------------------------
		$scope.asignar_model = function(){
			$scope.noticias.descripcion = $(".trumbowyg-editor").html();
		}
		//---
		$scope.insertar_noticias = function(){
			$http.post("./controladores/seccionnoticiasController.php",
			{
							 'id':$scope.noticias.id_noticias,
							 'titulo': $scope.noticias.titulo,
							 'id_imagen':$scope.noticias.id_imagen,
							 'descripcion':$scope.noticias.descripcion,
							 'id_idioma':$scope.noticias.idioma.id,
							 'accion':'registrar_noticias'

			}).success(function(data, estatus, headers, config){
					if(data["mensajes"]=="registro_procesado"){
						showSuccess("El registro fue realizado de manera exitosa");
						$scope.noticias.id_noticias =data["id"];
						$scope.limpiar_cajas_noticias();
					}else if(data["mensajes"]=="existe"){
						showSuccess("Ya existe la noticia");
					}
					else{
						showErrorMessage("Ocurrió un error inesperado");
					}
					desbloquear_pantalla("#mensaje_noticia","#titulo_noticias",".trumbowyg-editor");
				}).error(function(data,estatus){
					console.log(data);
			});
		}
		//---
		$scope.validar_form = function(){
			if(($scope.noticias.titulo=="")||($scope.noticias.titulo==undefined)){
				showErrorMessage("Debe indicar el título de la noticia!");				
				return false;
			}else if(($scope.noticias.descripcion=="")||($scope.noticias.descripcion==undefined)){
				showErrorMessage("Debe indicar descripción de la noticia!");				
				return false;
			}else if(($scope.noticias.imagen=="")||($scope.noticias.imagen==undefined)){
				showErrorMessage("Debe selecionar una imagen!");				
				return false;
			}else{
				return true;
			}
		}
		//---
		$scope.activar_check = function(caja){
			if($(caja).hasClass("fa-check-square-o")){
				$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.noticias.estatus = "0";	
			}else
			if($(caja).hasClass("fa-square-o")){
				$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.noticias.estatus = "1";	
			}			
		}
		//---
		$scope.consultar_idioma = function(){
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data
				console.log($scope.idioma)
			});
		}
		//---
		$scope.modificar_noticias = function(){
			$http.post("./controladores/seccionnoticiasController.php",
			{
							 'id':$scope.noticias.id_noticias,
							 'titulo': $scope.noticias.titulo,
							 'id_imagen':$scope.noticias.id_imagen,
							 'descripcion':$scope.noticias.descripcion,
							 'estatus':$scope.noticias.estatus,
							 'id_idioma':$scope.noticias.idioma.id,
							 'accion':'modificar_noticias'

			}).success(function(data, estatus, headers, config){
				if(data["mensajes"]=="modificacion_procesada"){
					showSuccess("El registro fue actualizado de manera exitosa");
					$scope.limpiar_cajas_noticias();
				}else if(data["mensajes"]=="no_existe"){
					showSuccess("No existe la noticia");
				}
				else{
					showErrorMessage("Ocurrió un error inesperado");
				}
				desbloquear_pantalla("#mensaje_noticia","#titulo_noticias",".trumbowyg-editor");
			}).error(function(data,estatus){
					console.log(data);
			});
		}
		//--
		$scope.limpiar_cajas_noticias = function(){
			$scope.noticias = {
								'id_noticias':'',
								'titulo':'',
								'imagen':'',
								'descripcion':'',
								'id_imagen':'',
								'estatus':'',
								'fecha':'',
								'idioma':'',
								'consulta':''
			}
			$scope.noticias.lenght = 0;
			$(".trumbowyg-editor").html("");
			$("#div_descripcion").html("Pulse aquí para ingresar el contenido de la noticia");
			serverDataMensajes.limpiar_arreglo_servicio();
			$scope.activo_img = "inactivo"
		}
		//---
		$scope.consultar_noticias = function(){
			setTimeout(function(){
				$scope.limpiar_cajas_noticias();
			},500)
			$location.path("/consultar_noticias");
		}
		//--
		$scope.wisi_modal = function(){
			//---
			$(".trumbowyg-editor").html("")
			$(".trumbowyg-editor").html($scope.noticias.descripcion)
			$("#modal_contenido").modal("show")
		}
		//---
		$scope.agregar_contenido = function(){
			$scope.noticias.descripcion = $(".trumbowyg-editor").html()
			$("#div_descripcion").html($scope.noticias.descripcion)
		}

//---Limpia arreglo de DataMensajes de otros controllers
		$scope.opcion_menu = function(opcion){
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		//----------------------------------------
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//---------------------------------------
		//--Cuerpo de llamados
		cargar_preload()
		$scope.consultar_idioma();
		$scope.sesion_usuario(); 
		$scope.consultar_galeria_img();
	});