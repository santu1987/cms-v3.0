angular.module("ContenteManagerApp")
	.controller("redessocialesController", function($scope,$http,$location,serverDataMensajes,sesionFactory,redesFactory){
		$scope.menu = "contactos";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Redes Sociales";
		$scope.activo_img = "inactivo";
		$scope.redes_sociales = {
									"id":"",
									'tipo_red_social':{
												"id":"",
												"descripcion":""},
									"url_red":""			
		}
		$scope.tipos_red_social = {
										"id":"",
										"descripcion":""
		}
		//Bloque de metodos
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.consultar_tipo_red_social = function(){
			redesFactory.cargar_redes(function(data){
				$scope.tipos_red_social=data;
				setTimeout(function(){
					$("#tipo_red").selectpicker('refresh');
				},400);
			});
		}
		//
		$scope.registrar_redes_sociales = function(){
			if(($scope.redes_sociales.tipo_red_social.id!="")&&($scope.redes_sociales.tipo_red_social.id!="undefined")&&($scope.redes_sociales.url_red)){	
				$http.post("./controladores/redesController.php",
				{
					'accion':'registrar_redes',
					'id': $scope.redes_sociales.id,
					'tipo_red':$scope.redes_sociales.tipo_red_social.id,
					'url_red':$scope.redes_sociales.url_red
				}).success(function(data, status, headers, config){
					console.log(data);
					if(data["mensajes"]=="error"){
						showErrorMessage("Ocurrió un error inesperado");
					}
					else if(data["mensajes"]=="no_existe"){
						showSuccess("El registro no existe");
					}
					else if(data["mensajes"]=="registro_procesado"){
						showSuccess("El registro fue realizado de manera exitosa");
						$scope.limpiar_redes_sociales();
					}
					else if(data["mensajes"]=="modificacion_procesada"){
						showSuccess("El registro fue actualizado de manera exitosa");
					}
				}).error(function(data,status){
					console.log(data);
					showErrorMessage("Ocurrió un error inesperado: "+ data);
				});
			}else{
				showErrorMessage("Debe seleccionar una red social y su url");
			}
		}
		//
		$scope.limpiar_redes_sociales = function(){
			$scope.redes_sociales = {
									"id":"",
									'tipo_red_social':{
												"id":"",
												"descripcion":""},
									"url_red":""			
			}
			$('#tipo_red').val("0");
			$('#tipo_red > option[value=0]').attr('selected', 'selected');
			$("#tipo_red").selectpicker('refresh');
		}
		//
		$scope.consultar_red = function (){
			redesFactory.asignar_tipo_red($scope.redes_sociales.tipo_red_social.id);
			redesFactory.cargar_redes_detalles(function(data){
				$scope.redes_sociales.id=data[0].id;
				$scope.redes_sociales.url_red=data[0].url_red;
			});
		}
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//Bloque de llamados
		$scope.consultar_tipo_red_social();
		$scope.sesion_usuario();
	});	