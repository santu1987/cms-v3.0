angular.module("ContenteManagerApp")
	.controller("tiposNegociosconsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "negocios";
		$scope.submenu = "admin"
		$scope.titulo_seccion = "Consulta Tipos de negocios";
		$scope.tipos_negocios = {
									"id":"",
									"titulo":"",
									"descripcion":"",
									"consulta":"si"
		}
		//
	//----------------------------------------------------------
	//datos sesion
	$scope.nombre_usuario = "";
	$scope.login_usuario = "";
	$scope.sesion_usuario = function(){
		sesionFactory.datos_sesion(function(data){
			if(data["nombre_usuario"]!=""){
				$scope.nombre_usuario = data["nombre_usuario"];
				$scope.login_usuario = data["login_usuario"];
			}
		});
	}
	$scope.sesion_usuario();
	//---
	
	$scope.consultar_tipos_negocios = function(){
		serverDataMensajes.puenteData("");
		$http.post("./controladores/tiposNegociosController.php",
		{
			'accion':'consultar_tipos_negocios'
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else{
				$scope.tipos_negocios = data;
				console.log($scope.tipos_negocios);
				setTimeout(function(){
					iniciar_datatable();
				},500);
			}
		}).error(function(data,status){
			console.log(data);
		});
	}
	//---------------------------------
	$scope.ver_tipo_negocio = function(indice){
		$scope.tipos_negocios[indice].consulta = "si";
		serverDataMensajes.puenteData($scope.tipos_negocios[indice]);
		$location.path("/tipos_negocios");
	}
	//---------------------------------------
	$scope.procesar_activacion_tipos_negocios =function(){
		$http.post("./controladores/tiposNegociosController.php",
		{
			'accion':'modificar_estatus',
			'id': $scope.id_seleccionado_tipos_negocios,
			'estatus':$scope.estatus_seleccionado_tipos_negocios
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_tipos_negocios();
				showSuccess("El registro fue actualizado de manera exitosa");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_tipos_negocios = []
		$scope.estatus_seleccionado_tipos_negocios = []
		var caja = event.currentTarget.id;
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_tipos_negocios = arreglo_atributos[0];
		$scope.estatus_seleccionado_tipos_negocios = arreglo_atributos[1];
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}
		});
	}
	//---------------------------------------------------------
	//--Bloque de Eventos
		$scope.sesion_usuario();
		cargar_preload()
		$scope.consultar_tipos_negocios();
	//--
	});