//--Redirecciona al entorno inicial...
/*$("#form_inicio2").attr("action","#/entorno");
$("#form_inicio2").attr("target","_self")
$("#form_inicio2").submit();*/
angular.module("ContenteManagerApp")
	.controller("registrarCitasController", function($scope,$http,$location,sesionFactory,citasProgramadasFactory){
	//---------------------------------------
	$scope.menu = "";
	$scope.menu_citas = "activo";
	$scope.submenu = "citas";
	$scope.titulo_pagina = "Registrar citas";
	$scope.titulo_seccion = "Registrar citas";
	$scope.citas = []
	$scope.citas_programadas = []
	$scope.events = []
	$scope.doctores = []
	$scope.doctor = {
							"id":"",
							"id_personas":"",
							"cedula":"",
							"nombres_apellidos":"",
							'especialidad':'',
							'descripcion':'',
							'imagen':'',
							'nombre_especialidad':'',
							'estatus':'',
							'doctores_soportes':'',
							'id_soportes':'',
							'number':''
					}
	$scope.pacientes= {
							'nombres':'',
							'hora':'12:00 PM',
							'dia':''
	}
	$scope.cuantas_citas = []
	$scope.lista_citas = []
	//----------------------------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}
		});
	}
	//---
	$scope.validar_vacio = function(){
		if($scope.doctor.nombre_especialidad===undefined){
			$scope.doctor.nombre_especialidad="";
		}
	}
	//---
	$scope.mostrar_modal_agenda = function(){
		$("#modal_agenda").modal("show");
	}
	//---
	$scope.consultar_doctores = function(){

		$http.post("./controladores/doctoresController.php",
		{
			'accion':'consultar_doctores'
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else{
				$scope.doctores = data;
				setTimeout(function(){
					$("#select_dr").selectpicker('refresh');	
				},500);			
			}
			//----------------------------------------------
	
		}).error(function(data,status){
			console.log(data);
		});
	}
	//---
	$scope.obtener_citas = function(){
		//--
	    var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
		//--
		var id_doctor = $scope.doctor.id
		var id_especialidad = $scope.doctor.especialidad
		$http.post("./controladores/citasController.php",
		{
			'accion':'consultar_citas_agendar',
			'id_doctor':id_doctor
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else{
				//------------------

				//------------------
				$scope.citas = data;
				$scope.cuantas_citas = data.length;
				setTimeout(function(){
					cargar_drag();
				},500);
				//console.log($scope.citas);	
				//------------------------------------------
                $scope.events = $scope.consultar_citas(id_especialidad,id_doctor);
			//----------------------------------------------		
			}
		}).error(function(data,status){
			console.log(data);
		});
	}
	//---------------------------------------------------------
	$scope.obtener_solo_citas = function(){
	//--------------------------------------
		var vector_lista = $scope.lista_citas
		var i = 0
		for(i=0;i<vector_lista.length;i++){
			$("#caja_cita"+vector_lista[i]).css({"display":"none"})
			$scope.cuantas_citas = $scope.cuantas_citas-1
		}
	//--------------------------------------	
	}
	//---------------------------------------------------------
	$scope.consultar_citas = function(id_especialidad,id_doctor){
	//--------------
		$('#calendar').fullCalendar( 'removeEvents');
		$scope.citas_programadas = "";
		citasProgramadasFactory.valor_estatus('2',id_especialidad,id_doctor);//
		citasProgramadasFactory.cargar_citas_programadas(function(data){
			/*for(i=0;i<data.length;i++){
				alert("Fecha ciclo:"+data[i].start);
				//data[i].start = new Date(data[i].start)
				alert("Fecha Modificada:"+data[i].start)
			}*/
			$scope.citas_programadas = data;
			$("#calendar").fullCalendar( 'refresh' )
			if(($scope.citas_programadas[0].title!="")&&($scope.citas_programadas[0].title!=undefined)){

				$("#calendar").fullCalendar( 'addEventSource', $scope.citas_programadas )
			}
			//console.log($scope.citas_programadas);
		});	
	//-------------	
	}
	//---------------------------------------------------------
	//Para ver el detalle de una cita
	$scope.ver_detalle_cita = function(arreglo_cita){
		$scope.pacientes.nombres = arreglo_cita.nombres_persona;
	}
	//---------------------------------------------------------
	//--Cuerpo jquery...
	$("document").on("click",".fc-title",function(){
		//alert("Here a la mil");
	});
	/*$('#calendar').fullCalendar({
    	defaultView: 'timelineDay'
  	});*/
  	$scope.agendar_cita = function(){
  		uploader_reg("#mensaje_cita","#hora_cita_paciente");
  		paciente = $("#nombre_paciente").val()
  		vector_paciente = paciente.split("|")
  		id_ant = vector_paciente[1]
  		super_id = id_ant.split("#")
  		id = super_id[1]
  		nombre_paciente = vector_paciente[2]
		var id_doctor = $scope.doctor.id;
  		$scope.guardar_cita(id,nombre_paciente,$("#hora_cita_paciente").val(),$("#dia_cita_paciente").val(),id_doctor)
  		
  	}
  	//---------------------------------------------------------
  	$scope.guardar_cita = function(id,nombre_paciente,hora,fecha,id_doctor){
  		super_fecha = fecha.substr(6,4)+"-"+fecha.substr(3,2)+"-"+fecha.substr(0,2);
  		$scope.pacientes = {
  								"id":id,
  								"nombres":nombre_paciente,
  								"hora":hora,
  								"fecha":super_fecha,

  		}
  		$http.post("./controladores/citasController.php",
		{
			'accion':'registrar_cita',
			'id':id,
			//'nombres':$scope.pacientes.nombres,
			'hora':$scope.pacientes.hora,
			'fecha':$scope.pacientes.fecha,
			'id_doctor':id_doctor,
			'nombre_doctor':$scope.doctor.nombres_apellidos,
  			'nombre_especialidad':$scope.doctor.nombre_especialidad
		}).success(function(data, status, headers, config){
			if(data["error"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="registro_procesado"){
				showSuccess("La cita seleccionada fue agendada");
				$("#modal_agenda").modal("hide");
				crear_objeto(id,hora,nombre_paciente)
				//----
				$scope.disminuir_objeto_lista(id)
				//----
			}else if(data["mensajes"]=="no_existe"){
				showSuccess("La cita seleccionada no existe");
			}else if (data["mensajes"]=="existe_horario"){
				showSuccess("Ya existe una cita en ese horario");
			}
			desbloquear_pantalla("#mensaje_cita","#hora_cita_paciente")
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+data);
		});
  	}
  	//---------------------------------------------------------
  	$scope.disminuir_objeto_lista = function(id){
		$("#check_publicado"+id).css({"display":"none"})
  		$scope.cuantas_citas = $scope.cuantas_citas-1
  	}
  	//---------------------------------------------------------
  	function crear_objeto(id,hora,nombre_paciente){
  		//var item = $("#calendar").fullCalendar( 'clientEvents', id )
  		/*var item = $('#calendar').fullCalendar('clientEvents', 
  			function(events){ 
  				console.log(events)
  				alert(events.id)
  				if(events.id == id){
  					alert("event id"+events.id+"== id"+id)
  					return events;
  				}
  		});*/

  		var item = $("#calendar").fullCalendar( 'clientEvents', [id] )
 		item[0].title =  hora+"|#"+id+"|"+nombre_paciente
		item[0].start = $scope.pacientes.fecha+" "+$scope.pacientes.hora
		item[0].id = id
		//console.log(item[0])
  		//item.status = 'checked out';
  		$('#calendar').fullCalendar('updateEvent',item[0]);
		//$('#calendar').fullCalendar( 'renderEvent', evento, true);
		//$('#calendar').fullCalendar( 'updateEvent', evento );
  	}
  	//---------------------------------------------------------
  	$scope.borrar_citas = function(){
  		$scope.anular_cita();
  	}
  	//---------------------------------------------------------
  	$scope.liberar_cita = function(){
  		$scope.limpiar_cita()
  	}
  	//---------------------------------------------------------
  	$scope.limpiar_cita = function(){
  		paciente = $("#nombre_paciente").val()
  		vector_paciente = paciente.split("|")
  		id_ant = vector_paciente[1]
  		super_id = id_ant.split("#")
  		id = super_id[1]
  		$http.post("./controladores/citasController.php",
		{
			'accion':'limpiar_cita',
			'id':id
		}).success(function(data, status, headers, config){
			if(data["error"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="limpieza_procesada"){
				showSuccess("La cita ha sido liberada para ser agendada");
				$("#modal_agenda").modal("hide");
				$scope.obtener_citas();
			}else if(data["mensajes"]=="no_existe"){
				showSuccess("La cita seleccionada no existe");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+data);
		});
  	}
  	//---------------------------------------------------------
  	$scope.anular_cita = function(){
  		$http.post("./controladores/citasController.php",
		{
			'accion':'anular_cita',
			'vector_id':$scope.lista_citas
		}).success(function(data, status, headers, config){
			if(data["error"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="anulacion_procesada"){
				showSuccess("La(s) cita(s) seleccionada(s) han sido anulada(s)");
				$scope.obtener_solo_citas()						
			}else if(data["mensajes"]=="no_existe"){
				showSuccess("La cita seleccionada no existe");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+data);
		});
  	}
  	//---------------------------------------------------------
  	$scope.activar_check = function(event){
  		var caja = "#"+event.target.id;
  		var id = $("#"+event.target.id).attr("data")
		if($(caja).hasClass("fa-check-square-o")){
			$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
			$scope.agregar_id(id,2);
		}else
		if($(caja).hasClass("fa-square-o")){
			$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
			$scope.agregar_id(id,1);
		}			
	}
	//---------------------------------------------------------
	$scope.agregar_id = function(id,tipo){
		//---Si tipo=1 -> agrego
		//---Si tipo=2 -> elimino
		if(tipo=="1"){
			$scope.lista_citas.push(id)
		}else if(tipo=="2"){
			var posicion = $scope.lista_citas.indexOf(id)
			$scope.lista_citas.splice(posicion,1);
		}
		//console.log($scope.lista_citas)
	}
	//---------------------------------------------------------
	//---

	//---
	//---------------------------------------------------------
	$scope.sesion_usuario();
	$('#hora_cita_paciente').timepicker()
	$scope.validar_vacio()
	$scope.consultar_doctores();
	//--------------------------------------------------------
	});