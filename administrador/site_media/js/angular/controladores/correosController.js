angular.module("ContenteManagerApp")
	.controller("correosController", function($scope,$compile,$http,$location,serverDataMensajes,sesionFactory,redesFactory){
		$scope.menu = "contactos";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Correos";
		$scope.activo_img = "inactivo";
		$scope.correos = {
									"id":"",
									'correo':'',
									"titulo":'',
									"estatus":'',
									'consulta':''			
		}
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.cuantos = 1

		//----
		//Si el form viene de consulta se alimenta del servicio...
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
		if($scope.valor_intermedio.consulta=="si"){
			if($scope.valor_intermedio.lenght!=0){
				$scope.correos = $scope.valor_intermedio;
				console.log($scope.correos);
				if($scope.correos.id==undefined){
					$scope.correos.id = "";
				}			
				//---
			}
			//Para estatus
			if($scope.correos.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.correos.estatus = "1";
			}else
			if($scope.correos.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.correos.estatus = "0";
			}
			$scope.valor_intermedio.consulta="";
		}	
		//---
		//Bloque de metodos
		
		//--Para registrar direcciones
		$scope.registrar_correo = function(){
			if($scope.validar_form()==true){
			//-----------------------------------	
				//uploader_reg("#mensaje_direccion","#direccion,.tlf_direccion");
				//Para guardar
				if($scope.correos.id!=""){
					$scope.modificar_correo();	
				}else{
					//alert("insertar");
					$scope.insertar_correo();
				}
			//-----------------------------------	
			}
		}
		//--
		$scope.validar_form = function(){
			var validado_telefono = 0;
			var a = 0;
	
			//alert(validado_telefono+"=="+$scope.cuantos)
			if($scope.correos.titulo==""){
				showErrorMessage("Debe ingresar el destinatario");	
			   	  return false;	
			}else if($scope.correos.correo==""){
				showErrorMessage("Debe ingresar el correo");	
			   	  return false;	
			}else
				return true;
		}
		//--
		
		//--
		$scope.insertar_correo = function(){
			$http.post("./controladores/correoController.php",
			{
				'accion':'registrar_correo',
				'id': $scope.correos.id,
				'titulo':$scope.correos.titulo,
				'correo':$scope.correos.correo
			}).success(function(data, status, headers, config){
				console.log(data);
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}
				else if(data["mensajes"]=="registro_procesado"){
					showSuccess("El registro fue realizado de manera exitosa");
					$scope.limpiar_direccion();
				}
				
			}).error(function(data,status){
				console.log(data);
				showErrorMessage("Ocurrió un error inesperado: "+ data);
			});
		}
		//
		$scope.modificar_correo = function(){
			$http.post("./controladores/correoController.php",
			{
				'accion':'modificar_correo',
				'id': $scope.correos.id,
				'titulo':$scope.correos.titulo,
				'correo':$scope.correos.correo,
				'estatus': $scope.correos.estatus
			}).success(function(data, status, headers, config){
				console.log(data);
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else
				if(data["mensajes"]=="no_existe"){
					showErrorMessage("El registro no existe");
				}
				else if(data["mensajes"]=="modificacion_procesada"){
					showSuccess("El registro fue actualizado de manera exitosa");
					$scope.limpiar_direccion();
				}
			}).error(function(data,status){
				console.log(data);
				showErrorMessage("Ocurrió un error inesperado: "+ data);
			});
		}
		//
		$scope.limpiar_direccion = function(){
			$scope.correos = {
									"id":"",
									'titulo':'',
									"correo":'',
									"estatus":''			
			}

		}
		//Consultar las direcciones
		$scope.consultar_direccion = function(){
			setTimeout(function(){
				$scope.limpiar_direccion();
			},500)
			$location.path("/consultar_correos");
		}
		//Para check....
		$scope.activar_check = function(caja){
			if($(caja).hasClass("fa-check-square-o")){
				$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.correos.estatus = "0";	
			}else
			if($(caja).hasClass("fa-square-o")){
				$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.correos.estatus = "1";	
			}			
		}
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//Bloque de llamados
		$scope.sesion_usuario();
	});	