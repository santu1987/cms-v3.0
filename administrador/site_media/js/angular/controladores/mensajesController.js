angular.module("ContenteManagerApp")
	.controller("mensajesController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
		$scope.menu = "activo";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Mensajes";
		$scope.mensaje = {
									"id":"",
									"titulo":"",
									"descripcion":"",
									"estatus":""
		}
		//---------------------------------------
		$scope.validar_vacio = function(){
			if($scope.mensaje.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.mensaje.estatus = "1";
			}else
			if($scope.mensaje.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.mensaje.estatus = "0";
			}
			
		}
		//---------------------------------------
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
		if($scope.valor_intermedio){
			$scope.mensaje = $scope.valor_intermedio;
			$scope.validar_vacio();
		}
		if($scope.mensaje.id==undefined){
				$scope.mensaje.id="";
		}
		//----------------------------------------
		$scope.activar_check = function(caja){
			if($(caja).hasClass("fa-check-square-o")){
				$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.mensaje.estatus = "0";	
			}else
			if($(caja).hasClass("fa-square-o")){
				$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.mensaje.estatus = "1";	
			}			
		}
		//---------------------------------------
		$scope.limpiar_cajas = function (){
			$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o ");
			$scope.mensaje = {
									"id":"",
									"titulo":"",
									"descripcion":"",
									"estatus":""
			}
		}
		//--
		$scope.registrar_mensaje = function(){
			if($scope.validar_mensaje()==true){
				uploader_reg("#mensaje_mensaje","#titulo_mensaje,#descripcion_mensaje");
				if($scope.mensaje.id==""){
					$scope.guardar_mensaje();
				}else{
					$scope.actualizar_mensaje();
				}
			}
		}
		//---------------------------------------
		$scope.guardar_mensaje = function (){
				$http.post("./controladores/mensajeController.php",
				{
						'titulo' : $scope.mensaje.titulo,
						'descripcion' : $scope.mensaje.descripcion,
						'estatus': $scope.mensaje.estatus,
						'accion': 'registrar_mensaje',
						'id':$scope.mensaje.id
				}).success(function(data, status, headers, config){
						if(data["mensajes"]=="proceso exitoso"){
							showSuccess("El proceso de ha ejecutado de manera satisfactoria");
							$scope.mensaje.id = data["id"]
							$scope.limpiar_cajas();
						}else{
							showErrorMessage("Ocurrió un error inesperado");
						}
				desbloquear_pantalla("#mensaje_mensaje","#titulo_mensaje,#descripcion_mensaje");	
				}).error(function(data,status){
						//$scope.mensaje_error();
						console.log(data);
				});

		}
		//---------------------------------------
		$scope.actualizar_mensaje =  function(){
			$http.post("./controladores/mensajeController.php",
				{
						'titulo' : $scope.mensaje.titulo,
						'descripcion' : $scope.mensaje.descripcion,
						'estatus': $scope.mensaje.estatus,
						'accion': 'actualizar_mensaje',
						'id':$scope.mensaje.id
				}).success(function(data, status, headers, config){
						if(data["mensajes"]=="actualizacion exitosa"){
							showSuccess("Actualización realizada de manera satisfactoria");
							$scope.limpiar_cajas();
						}else{
							showErrorMessage("Ocurrió un error inesperado");
						}
				desbloquear_pantalla("#mensaje_mensaje","#titulo_mensaje,#descripcion_mensaje");	
				}).error(function(data,status){
						//$scope.mensaje_error();
						console.log(data);
				});
		}
		//---------------------------------------
		$scope.validar_mensaje = function(){
			if(($scope.mensaje.titulo == "")||($scope.mensaje.titulo==undefined)){
				showErrorMessage("Debe indicar un título del mensaje");				
				return false;
			}else if(($scope.mensaje.descripcion=="")||($scope.mensaje.descripcion==undefined)){
				showErrorMessage("Debe indicar descripcion del mensaje");
				return false;
			}
			return true;
		}
		//---Limpia arreglo de DataMensajes de otros controllers
		$scope.opcion_menu = function(opcion){
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		//----------------------------------------
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//----------------------------------------
		$scope.consultar_mensajes = function(){
			$location.path("/consultar_mensajes");
		}
		//---------------------------------------
		$scope.sesion_usuario();
		cargar_preload();
});