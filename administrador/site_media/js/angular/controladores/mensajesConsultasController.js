angular.module("ContenteManagerApp")
	.controller("mensajesconsultasController", function($scope,$http){
	//---------------------------------------
		$scope.titulo_pagina = "Consulta mensajes";
		$scope.mensaje = {
										"id":"",
										"titulo":"",
										"descripcion":"",
										"number":""
		}		
		//
		$scope.consultar_mensajes = function(){
			$.http.post("./controladores/mensajeController.php",
			{
				'accion':'consultar_mensajes'
			}).success(function(data, status, headers, config){
				alert(data);
				$scope.mensajes = data;
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}
			}).error(function(data,status){
				console.log(data);
			});
		}
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
	//---------------------------------------
	//--Bloque de Eventos
		//$scope.sesion_usuario();
		$scope.consultar_mensajes();
	//--
	});