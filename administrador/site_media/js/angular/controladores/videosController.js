//--Redirecciona al entorno inicial...
/*$("#form_inicio2").attr("action","#/entorno");
$("#form_inicio2").attr("target","_self")
$("#form_inicio2").submit();*/
angular.module("ContenteManagerApp")
	.controller("videosController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory){
	//---------------------------------------
	$scope.menu = "galeria";
	$scope.submenu = "admin"
	$scope.titulo_pagina = "Galería de Vídeos";
	$scope.currentTab = 'datos_basicos';
	$scope.video = {
						"id":"",
						"titulo":"",
						"link":"",
						"consulta":"",
						"idioma":""
	}
	$scope.enlace = [];
	$scope.valor_intermedio = ""
	//----------------------------------------------------------
	//datos sesion
	$scope.nombre_usuario = "";
	$scope.login_usuario = "";
	$scope.sesion_usuario = function(){
		sesionFactory.datos_sesion(function(data){
			if(data["nombre_usuario"]!=""){
				$scope.nombre_usuario = data["nombre_usuario"];
				$scope.login_usuario = data["login_usuario"];
			}
		});
	}
	$scope.sesion_usuario();
	//---

	$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
	//---
	if($scope.valor_intermedio.consulta=="si"){

		if($scope.valor_intermedio.lenght!=0){	
			$scope.video = $scope.valor_intermedio;
			if($scope.video.id==undefined){
				$scope.video.id="";
			}
		}
		
		//---------------------------------------------------------------------------------
		//Para las sessiones
		//Para estatus
		if($scope.video.estatus=="1"){
			$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
			$scope.video.estatus = "1";
		}else
		if($scope.video.estatus=="0"){
			$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
			$scope.video.estatus = "0";
		}
		$scope.video.idioma.id = $scope.valor_intermedio.id_idioma
		$('#idioma > option[value="'+$scope.valor_intermedio.id_idioma+'"]').attr('selected', 'selected');

		$scope.valor_intermedio.consulta="";
		//---------------------------------------------------------------------------------
	}	
	//---
	//--valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}
		});
	}
	
	//--activar check en form
	$scope.activar_check = function(caja){
		if($(caja).hasClass("fa-check-square-o")){
			$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
			$scope.video.estatus = "0";	
		}else
		if($(caja).hasClass("fa-square-o")){
			$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
			$scope.video.estatus = "1";	
		}			
	}
	//---
	$scope.visualizar_video = function(){
		//mensaje_preloader2("#campo_mensaje2","Espere unos segundos mientras se carga la vista previa del video");
		$scope.enlace = cargar_enlace();
		if($scope.enlace==false){
			$scope.enlace =  []
			showErrorMessage("Error en carga de video");
			//$scope.video = {"id":"","titulo":"","link":""}			
		}/*else{
			setTimeout(function(e){
				quitar_preloader_video();
			},3000);
		}*/
	}
	//---
		
	//---------------------------------------------------------
	//---Limpia arreglo de DataMensajes de otros controllers
	$scope.opcion_menu = function(opcion){
		serverDataMensajes.limpiar_arreglo_servicio();
	}
	//---Reinicia el valor del scope
	$scope.reiniciar_vars = function(){
		$scope.video = {
					"id":"",
					"titulo":"",
					"link":"",
					"consulta":"",
					"idioma":""
		}
		$scope.enlace = []
	}
	//--Metodo para registrar video
	$scope.registrar_video = function(){
		if($scope.validar_campos()==true){
			uploader_reg("#campo_mensaje","#titulo_video,#link_video");
			//----------------------------------------------------------
			if(($scope.video.id!=undefined)&&($scope.video.id!="")){
				$scope.modificar_video();	
			}else{
				$scope.insertar_video();
			}
			//----------------------------------------------------------
		}		
	}
	//--Metodo para insertar registro de video
	$scope.insertar_video = function(){
		$http.post("./controladores/videosController.php",
		{
				'id': $scope.video.id,
				'titulo': $scope.video.titulo,
				'link': $scope.video.link,	
				'id_idioma':$scope.video.idioma.id,
				'accion':'registrar_videos'

		}).success(function(data, estatus, headers, config){
			if(data["mensajes"]=="registro_procesado"){
				showSuccess("El registro fue realizado de manera exitosa");
				$scope.video.id_personas =data["id_persona"];
				$scope.video.id = data["id"]
				$scope.limpiar_video()
			}else if(data["mensajes"]=="existe_video"){
				showSuccess("Ya existe ese video");
				$("#link_video").focus();
			}else{
				showErrorMessage("Ocurrió un error inesperado");
			}
		desbloquear_pantalla("#campo_mensaje","#titulo_video,#link_video")
		}).error(function(data,estatus){
			console.log(data);
		})
	}
	//--Metodo para modificar el registro de video
	$scope.modificar_video = function(){
		$http.post("./controladores/videosController.php",{
			'id':$scope.video.id,
			'titulo': $scope.video.titulo,
			'link': $scope.video.link,
			'estatus': $scope.video.estatus,
			'id_idioma':$scope.video.idioma.id,	
			'accion':'modificar_videos'
		}).success(function(data, estatus, headers, config){
			if(data["mensajes"]=="modificacion_procesada"){
				showSuccess("El registro fue actualizado de manera exitosa");
				$scope.limpiar_video();
			}else if(data["mensajes"]=="no_existe_video"){
				showErrorMessage("No existe el video");
			}else if(data["mensajes"]=="existe_link_otro_video"){
				showErrorMessage("Existe otro registro con ese link!")
			}else{
				showErrorMessage("Ocurrió un error inesperado");
			}
			desbloquear_pantalla("#campo_mensaje","#titulo_video,#link_video")
		}).error(function(data,estatus){
			showErrorMessage("Ocurrió un error inesperado:"+data);
		})
	}
	//---------------------------------------------------------------
	$scope.consultar_idioma = function(){
		idiomaFactory.cargar_idioma(function(data){
			$scope.idioma=data
			console.log($scope.idioma)
		});
	}
	//---------------------------------------------------------------
	$scope.consultar_videos = function(){
		cargar_preload();
		$location.path("/consultar_videos");
	}
	//--
	$scope.limpiar_video = function(){
		$scope.reiniciar_vars();
		quitar_video();	
	}
	//--verifica si los campos!=''
	$scope.validar_campos = function(){
		if($scope.video.titulo==""){
			showErrorMessage("Debe ingresar el título del video");
			return false;	
		}else if($scope.video.link==""){
			showErrorMessage("Debe ingresar el link del video");
			return false;
		}else{
			return true;
		}
	}
	//---------------------------------------------------------
	$scope.sesion_usuario();
	$scope.consultar_idioma();
	cargar_preload()
	//--------------------------------------------------------
	});