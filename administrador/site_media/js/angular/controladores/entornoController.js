//--Redirecciona al entorno inicial...
/*$("#form_inicio2").attr("action","#/entorno");
$("#form_inicio2").attr("target","_self")
$("#form_inicio2").submit();*/
angular.module("ContenteManagerApp")
	.controller("entornoController", function($scope,$http,$location,sesionFactory){
	//---------------------------------------
	$scope.menu = "";
	$scope.submenu = "";
	$scope.nombre_usuario = "";
	$scope.login_usuario = "";
	//----------------------------------------------------------
	//valido inicio de sesion
	//datos sesion
	$scope.sesion_usuario = function(){
		sesionFactory.datos_sesion(function(data){
			if(data["nombre_usuario"]!=""){
				$scope.nombre_usuario = data["nombre_usuario"];
				$scope.login_usuario = data["login_usuario"];
			}
		});
	}
	$scope.sesion_usuario();
	//---
	$scope.datos_sesion = function(){
		sesionFactory.datos_sesion(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}
		});
	}
	//---
	$scope.ir_mensaje_inicial = function(){
		cargar_preload();
		$location.path("/mensaje_inicial");
	}
	//---
	$scope.ir_doctores = function(){
		cargar_preload();
		$location.path("/doctores");
	}
	//---
	$scope.ir_especialidades = function(){
		cargar_preload();
		$location.path("/especialidades");
	}
	//---
	$scope.ir_album_fotos = function(){
		cargar_preload();
		$location.path("/album_fotos");
	}
	//---
	$scope.ir_noticias = function(){
		cargar_preload();
		$location.path("/noticias");
	}
	//---
	$scope.ir_galeria = function(){
		cargar_preload();
		$location.path("/galeria");
	}
	//--
	$scope.ir_registrar_citas = function(){
		cargar_preload();
		$location.path("/registrar_citas");
	}
	//--
	$scope.ir_galeria_videos = function(){
		cargar_preload();
		$location.path("/galeria_videos");
	}
	//--
	$scope.ir_usuario = function(){
		cargar_preload();
		mostrar_preload();
		$location.path("/usuarios");
	}
	//--
	$scope.conf_masonrry = function(){
		$(".cuerpo_btn_galeria").addClass("animated fadeIn");
        $('.grid').masonry({itemSelector: '.grid-item2',percentPosition: true});
		    	setTimeout(function(){
	    			$(".cuerpo_btn_galeria").removeClass("animated fadeIn");
	    			if($scope.inicio_masonrry == '1'){
	    				$("#masonrry").masonry( 'destroy' )
	    				$scope.iniciar_masonry();	
	    			}              			
	    		},1000);		    							
	}
	//--
	$scope.iniciar_masonry = function(){
		setTimeout(function(){
				$("#masonrry").masonry();
				$scope.inicio_masonrry = "1";
		},1000);	
	}
	//---------------------------------------------------------
	$scope.conf_masonrry() 
	//$scope.sesion_usuario();
	//--------------------------------------------------------
	});