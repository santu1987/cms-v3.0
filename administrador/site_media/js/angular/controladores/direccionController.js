angular.module("ContenteManagerApp")
	.controller("direccionController", function($scope,$compile,$http,$location,serverDataMensajes,sesionFactory,redesFactory){
		$scope.menu = "contactos";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Dirección";
		$scope.activo_img = "inactivo";
		$scope.direccion = {
									"id":"",
									'descripcion':'',
									"telefono":[],
									"estatus":"",
									"consulta":""			
		}
		$scope.cuantos = 1
		//----
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.agregar_telefono_valores = function(valor){
			$scope.cuantos = $scope.cuantos+1;
			if($scope.cuantos==1){
				$("#telefono_direccion_0").val(valor)
			}else{
			//---	

			number = $scope.cuantos
			number2 = number
			var bloque_telefono = '<div id="fila_'+number+'" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 tlf_direccion_cuadro">\
									<div class="form-group">\
										<label>Teléfono #'+number2+'</label>\
										<input type="text" name="telefono_direccion" id="telefono_direccion" class="form-control  tlf_direccion" placeholder="Formato:+58-xxx-xxx-xx-xx" onKeyPress="return valida(event,this,13,17)" onBlur="valida2(this,13,17);formato_tlf(this);" maxlength="50" value="'+valor+'">\
							    	</div>\
								</div>'
		    $("#super_contenedor").append($compile(bloque_telefono)($scope)).fadeIn("slow");  	
			
			//---
			}
		}
		//----
		//Si el form viene de consulta se alimenta del servicio...
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
		if($scope.valor_intermedio.consulta=="si"){
		//-------------------------------------------------------------------------
			if($scope.valor_intermedio.lenght!=0){
				$scope.direccion = $scope.valor_intermedio;
				console.log($scope.direccion);
				if(($scope.direccion.id==undefined)||($scope.direccion.id=="")){
					$scope.direccion.id = "";
				}
				else{
					$scope.cuantos = 0;
					$.each($scope.direccion.telefono, function( index, value ) {
					  $scope.agregar_telefono_valores(value.telefono)
					});
				}
				
				//---
			}
			//Para estatus
			if($scope.direccion.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.direccion.estatus = "1";
			}else
			if($scope.direccion.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.direccion.estatus = "0";
			}
			$scope.valor_intermedio.consulta="";
		//---------------------------------------------------------------------------------
		}	
		//---
		//Bloque de metodos
		$scope.agregar_telefono = function(){
			number = $scope.cuantos+1
			var bloque_telefono = '<div id="fila_'+number+'" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 tlf_direccion_cuadro">\
									<div class="form-group">\
										<label>Teléfono #'+number+'</label>\
										<input type="text" name="telefono_direccion_'+number+'" id="telefono_direccion" class="form-control  tlf_direccion" placeholder="Formato:+58-xxx-xxx-xx-xx" onKeyPress="return valida(event,this,13,17)" onBlur="valida2(this,13,17);formato_tlf(this);" maxlength="50">\
							    	</div>\
								</div>'
		    $("#super_contenedor").append($compile(bloque_telefono)($scope)).fadeIn("slow");  	
			$scope.cuantos = number;
		}
		
		$scope.quitar_telefono = function(){
			if($scope.cuantos>1){
				var ene = $scope.cuantos
				$("#fila_"+ene).remove()
				$scope.cuantos = parseInt($scope.cuantos)-1;
			}else{
				showErrorMessage("Debe agregar al menos 2 teléfonos");
			}	
		}
		//--Para registrar direcciones
		$scope.registrar_direccion = function(){
			if($scope.validar_form()==true){
			//-----------------------------------	
				//uploader_reg("#mensaje_direccion","#direccion,.tlf_direccion");
				//Para guardar
				if($scope.direccion.id!=""){
					$scope.modificar_direccion();	
				}else{
					//alert("insertar");
					$scope.insertar_direccion();
				}
			//-----------------------------------	
			}
		}
		//--
		$scope.validar_form = function(){
			var validado_telefono = 0;
			var a = 0;
			$( ".tlf_direccion" ).each(function( index ) {
			   if($( this ).val()==""){
			   	  a = index +1;
			   	  showErrorMessage("Debe ingresar el valor del teléfonos #"+a);	
			   	  return false;	
			   }else{
			   		validado_telefono = validado_telefono+1;	
			   }
			});
			//alert(validado_telefono+"=="+$scope.cuantos)
			if($scope.direccion.descripcion==""){
				showErrorMessage("Debe ingresar la dirección");	
			   	  return false;	
			}else if(validado_telefono==$scope.cuantos){
				return true;
			}
		}
		//--
		$scope.recorrer_form_telefono = function(){
			var data
		
			var telefono_vector = new Array()
			//--Recorro telefono
			$(".tlf_direccion").each(function(index){
				telefono = $(this).val()
				telefono_vector.push(telefono)
			});
			//--
			$scope.direccion.telefono = telefono_vector
			console.log($scope.direccion)
		}
		//--
		$scope.insertar_direccion = function(){
			$scope.recorrer_form_telefono()
			$http.post("./controladores/direccionController.php",
			{
				'accion':'registrar_direccion',
				'id': $scope.direccion.id,
				'descripcion':$scope.direccion.descripcion,
				'telefono':$scope.direccion.telefono
			}).success(function(data, status, headers, config){
				console.log(data);
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}
				else if(data["mensajes"]=="registro_procesado"){
					showSuccess("El registro fue realizado de manera exitosa");
					$scope.limpiar_direccion();
				}
				else if(data["mensajes"]=="modificacion_procesada"){
					showSuccess("El registro fue actualizado de manera exitosa");
					$scope.limpiar_direccion();
				}
			}).error(function(data,status){
				console.log(data);
				showErrorMessage("Ocurrió un error inesperado: "+ data);
			});
		}
		//
		$scope.modificar_direccion = function(){
			$scope.recorrer_form_telefono()
			$http.post("./controladores/direccionController.php",
			{
				'accion':'modificar_direccion',
				'id': $scope.direccion.id,
				'descripcion':$scope.direccion.descripcion,
				'telefono':$scope.direccion.telefono,
				'estatus': $scope.direccion.estatus
			}).success(function(data, status, headers, config){
				console.log(data);
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else
				if(data["mensajes"]=="no_existe"){
					showErrorMessage("El registro no existe");
				}
				else if(data["mensajes"]=="modificacion_procesada"){
					showSuccess("El registro fue actualizado de manera exitosa");
					$scope.limpiar_direccion();
				}
			}).error(function(data,status){
				console.log(data);
				showErrorMessage("Ocurrió un error inesperado: "+ data);
			});
		}
		//
		$scope.limpiar_direccion = function(){
			$scope.direccion = {
									"id":"",
									'descripcion':'',
									"telefono":[]			
			}
			$scope.cuantos = 1;
			$(".tlf_direccion").val("");
			$(".tlf_direccion_cuadro").remove();

		}
		//Consultar las direcciones
		$scope.consultar_direccion = function(){
			setTimeout(function(){
				$scope.limpiar_direccion();
			},500)
			$location.path("/consultar_direccion");
		}
		//Para check....
		$scope.activar_check = function(caja){
			if($(caja).hasClass("fa-check-square-o")){
				$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.direccion.estatus = "0";	
			}else
			if($(caja).hasClass("fa-square-o")){
				$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.direccion.estatus = "1";	
			}			
		}
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//Bloque de llamados
		$scope.sesion_usuario();
	});	