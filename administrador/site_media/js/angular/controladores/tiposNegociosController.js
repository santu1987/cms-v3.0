angular.module("ContenteManagerApp")
	.controller("tiposNegociosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory){
		//--Cuerpo de delcaraciones
		$scope.menu = "negocios";
		$scope.titulo_pagina = "Tipos Negocios";
		$scope.activo_img = "inactivo";
		$scope.tipos_negocios = {
									"id":"",
									"titulo":"",
									"descripcion":"",
									"idioma":"",
									"consulta":""
		}
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.valor_intermedio = []
		//Si el form viene de consulta se alimenta del servicio...
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
		if($scope.valor_intermedio.consulta=="si"){
		//--	
			if($scope.valor_intermedio.lenght!=0){
				$scope.tipos_negocios = $scope.valor_intermedio;
				if($scope.tipos_negocios.imagen!=undefined){
					$scope.activo_img='activo'
				}
				if($scope.tipos_negocios.descripcion){
					$(".trumbowyg-editor").html($scope.tipos_negocios.descripcion);
					$(".div_wisig").html($scope.tipos_negocios.descripcion);
				}
				if($scope.tipos_negocios.id==undefined){
					$scope.tipos_negocios.id = '';
				}
			}
			//Para estatus
			if($scope.tipos_negocios.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.tipos_negocios.estatus = "1";
			}else
			if($scope.tipos_negocios.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.tipos_negocios.estatus = "0";
			}
			//Para el idioma
			$scope.tipos_negocios.idioma.id = $scope.valor_intermedio.idioma.id
			$('#idioma > option[value="1"]').attr('selected', 'selected');
			//Limpio porque ya consultó.
			$scope.valor_intermedio.consulta="";
		//---
		}
		//--Cuerpo de metodos
		//---
		$scope.registrar_especialidades = function(){
			$scope.asignar_model();
			if($scope.validar_form()==true){
				uploader_reg("#mensaje_tipos_negocios","#titulo_tipos_negocios",".trumbowyg-editor");
				//Para guardar
				//alert("id_especialidad:"+$scope.especialidades.id_especialidades);
				if(($scope.tipos_negocios.id!="")&&($scope.tipos_negocios.id!=undefined)){
					//alert("modificar");
					$scope.modificar_tipos_negocios();	
				}else{
					//alert("insertar");
					$scope.insertar_tipos_negocios();
				}
				
			}
		}
		//--
		//---------------------------------------
		$scope.asignar_model = function(){
			$scope.tipos_negocios.descripcion = $(".trumbowyg-editor").html();
		}
		//---
		$scope.insertar_tipos_negocios = function(){
			$http.post("./controladores/tiposNegociosController.php",
			{
							 'id':$scope.tipos_negocios.id,
							 'titulo': $scope.tipos_negocios.titulo,
							 'descripcion':$scope.tipos_negocios.descripcion,
							 'id_idioma': $scope.tipos_negocios.idioma.id,
							 'accion':'registrar_tipos_negocios'

			}).success(function(data, estatus, headers, config){
					//alert(data["mensajes"]);
					if(data["mensajes"]=="registro_procesado"){
						showSuccess("El registro fue realizado de manera exitosa");
						$scope.tipos_negocios.id =data["id"];
						$scope.limpiar_cajas_tipos_negocios();
					}else if(data["mensajes"]=="existe"){
						showSuccess("Ya existe el tipo de negocio");
					}
					else{
						showErrorMessage("Ocurrió un error inesperado");
					}
					desbloquear_pantalla("#mensaje_tipos_negocios","#titulo_tipos_negocios",".trumbowyg-editor");
				}).error(function(data,estatus){
					console.log(data);
			});
		}
		//---
		$scope.validar_form = function(){
			if(($scope.tipos_negocios.idioma=="")||($scope.tipos_negocios.idioma=="0")||($scope.tipos_negocios.idioma==undefined)){
				showErrorMessage("Debe seleccionar un idioma");				
				return false;
			}else if(($scope.tipos_negocios.titulo=="")||($scope.tipos_negocios.titulo==undefined)){
				showErrorMessage("Debe indicar el título del tipo de negocio!");				
				return false;
			}else{
				return true;
			}
		}
		//---
		$scope.activar_check = function(caja){
			if($(caja).hasClass("fa-check-square-o")){
				$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.tipos_negocios.estatus = "0";	
			}else
			if($(caja).hasClass("fa-square-o")){
				$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.tipos_negocios.estatus = "1";	
			}			
		}
		//---
		$scope.modificar_tipos_negocios = function(){
			$http.post("./controladores/tiposNegociosController.php",
			{
							 'id':$scope.tipos_negocios.id,
							 'titulo': $scope.tipos_negocios.titulo,
							 'descripcion':$scope.tipos_negocios.descripcion,
							 'estatus': $scope.tipos_negocios.estatus,
							 'id_idioma': $scope.tipos_negocios.idioma.id,
							 'accion':'modificar_tipos_negocios'

			}).success(function(data, estatus, headers, config){
				if(data["mensajes"]=="modificacion_procesada"){
					showSuccess("El registro fue actualizado de manera exitosa");
					$scope.limpiar_cajas_tipos_negocios();
				}else if(data["mensajes"]=="no_existe"){
					showSuccess("No existe el tipo de negocio");
				}
				else{
					showErrorMessage("Ocurrió un error inesperado");
				}
				desbloquear_pantalla("#mensaje_tipos_negocios","#titulo_tipos_negocios",".trumbowyg-editor");
			}).error(function(data,estatus){
					console.log(data);
			});
		}
		//--
		$scope.limpiar_cajas_tipos_negocios = function(){
			$scope.tipos_negocios = {
									"id":"",
									"titulo":"",
									"descripcion":"",
									"idioma":"",
									"consulta":""
			}
			$scope.tipos_negocios.lenght = 0;
			$(".trumbowyg-editor").html("");
			$(".div_wisig").html("Pulse aquí para ingresar el contenido");
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		//---
		$scope.consultar_tipos_negocios = function(){
			setTimeout(function(){
				$scope.limpiar_cajas_tipos_negocios();
			},500)
			$location.path("/consultar_tipos_negocios");
		}
		//---
		$scope.wisi_modal = function(){
			//---
			$(".trumbowyg-editor").html($scope.tipos_negocios.descripcion)
			$("#modal_contenido").modal("show")
			//---
		}	
		//---
		$scope.agregar_contenido = function(){
			$scope.tipos_negocios.descripcion = $(".trumbowyg-editor").html()
			$("#div_descripcion_tipo_negocio").html($scope.tipos_negocios.descripcion)
		}	
		//---Limpia arreglo de DataMensajes de otros controllers
		$scope.opcion_menu = function(opcion){
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		//----------------------------------------
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//------------------------------------------
		$scope.consultar_idioma = function(){
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data
				console.log($scope.idioma)
			});
		}
		//---------------------------------------------------------
		//--Cuerpo de llamados
		$scope.sesion_usuario();
		$scope.consultar_idioma();
		cargar_preload();
	});