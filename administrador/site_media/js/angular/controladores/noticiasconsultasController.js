angular.module("ContenteManagerApp")
	.controller("noticiasconsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "activo";
		$scope.submenu = "admin"
		$scope.titulo_seccion = "Consulta noticias";
		$scope.noticias = {
								'id_noticias':'',
								'titulo':'',
								'imagen':'',
								'descripcion':'',
								'id_imagen':'',
								'estatus':'',
								'fecha':'',
								'descripcion_corta':'',
								'id_usuario':'',
								'nombre_usuario':'',
								'idioma':'',
								'consulta':'si'
		}
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		//---
		$scope.sesion_usuario();
		//---

		//
		$scope.consultar_noticias = function(){
			serverDataMensajes.puenteData("");
			$http.post("./controladores/seccionnoticiasController.php",
			{
				'accion':'consultar_noticias'
			}).success(function(data, status, headers, config){
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else{
					$scope.noticias = data;
					setTimeout(function(){
						iniciar_datatable();
					},500);
				}
			}).error(function(data,status){
				console.log(data);
			});
		}
	//---------------------------------
	$scope.ver_noticia = function(indice){
		$scope.noticias[indice].consulta = "si"
		serverDataMensajes.puenteData($scope.noticias[indice]);
		$location.path("/noticias");
	}
	//---------------------------------------
	$scope.procesar_activacion_noticias =function(){
		$http.post("./controladores/seccionnoticiasController.php",
		{
			'accion':'modificar_estatus',
			'id': $scope.id_seleccionado_noticias,
			'estatus':$scope.estatus_seleccionado_noticias
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_noticias();
				showSuccess("El registro fue actualizado de manera exitosa");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_noticias = []
		$scope.estatus_seleccionado_noticias = []
		var caja = event.currentTarget.id;
		//alert(caja);
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_noticias = arreglo_atributos[0];
		$scope.estatus_seleccionado_noticias = arreglo_atributos[1];
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}else{
				quitar_preload()
			}
		});
	}
	//---------------------------------------
	//--Bloque de Eventos
		$scope.sesion_usuario();
		cargar_preload()
		$scope.consultar_noticias();
	//--
	});