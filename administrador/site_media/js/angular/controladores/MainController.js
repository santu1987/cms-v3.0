angular.module("ContenteManagerApp")
	.controller("MainController", function($scope,$http,$location,sesionFactory){
		$scope.inicio = {};
		$scope.login = ""
		$scope.clave = ""
		//---------------------------------------
		$scope.inciar_sesion = function(){
			if($scope.validar_inicio_sesion()==true){
			//-----------------------------------------------------------------
				uploader_reg("#mensaje_is","#login_p,#clave")
				$http.post("./controladores/inicioController.php",{
					'login': $scope.login,
					'clave': $scope.clave,
					'accion':'iniciar_sesion'
				}).success(function(data, status, header, config){
					//alert(data);
					console.log(data);
					if(data["mensajes"]=="inicio con exito"){
						$scope.enviar_inicio();
					}else if(data["mensajes"] == "error datos"){
						showErrorMessage("Error en datos suministrados");
						desbloquear_pantalla("#mensaje_is","#login_p,#clave");

					}else{
						showErrorMessage("Ocurrió un error inesperado");
						desbloquear_pantalla("#mensaje_is","#login_p,#clave");
					}
				}).error(function(data,status){
					showErrorMessage("Ocurrió un error inesperado: "+data);
					desbloquear_pantalla("#mensaje_is","#login_p,#clave");
				});
			//------------------------------------------------------------------	
			}
		}
		//
		$scope.enviar_inicio = function(){
		  /*$("#form_inicio").attr("action","./panelUs.html");
		  $("#form_inicio").attr("target","_self")
		  $("#form_inicio").submit();*/
		  $location.path("/panelUs");
		}
		//
		/*$scope.enviar_inicio2 = function(){
		  $("#form_inicio2").attr("action","#/entorno");
		  $("#form_inicio2").attr("target","_self")
		  $("#form_inicio2").submit();
		}*/
		//--
		$scope.validar_inicio_sesion = function(){
			if($scope.login==""){
				showErrorMessage("Debe ingresar nombre de usuario");
				$("#login_p").focus();
				return false;
			}else
			if($scope.clave==""){
				showErrorMessage("Debe ingresar clave de usuario");
				$("#clave").focus();
				return false;
			}else{
				return true;
			}
		}

		$scope.cerrar_sesion = function(){
			sesionFactory.cerrar_sesion(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//---------------------------------------
		
		$("#login_p,#clave").keypress(function(e) {
		  if(e.which==13){
		    	$scope.inciar_sesion();
		  }
		}); 
		//---------------------------------------
	});