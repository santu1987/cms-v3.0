angular.module("ContenteManagerApp")
	.controller("marcasConsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "activo";
		$scope.submenu = "admin"
		$scope.titulo_seccion = "Consulta marcas";
		$scope.marcas = {
								'id':'',
								'descripcion':'',
								'marcas_soportes':'',
								'id_soportes':'',
								'id_idioma':'',
								'estatus':'',
								'consulta':'si'
		}
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		//
		$scope.consultar_marcas = function(){
			serverDataMensajes.puenteData("");
			$http.post("./controladores/marcasController.php",
			{
				'accion':'consultar_marcas'
			}).success(function(data, status, headers, config){
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else{
					$scope.marcas = data;
					console.log(data);
					setTimeout(function(){
						iniciar_datatable();
					},500);
				}
			}).error(function(data,status){
				console.log(data);
			});
		}
	//---------------------------------
	$scope.ver_marca = function(indice){
		$scope.marcas[indice].consulta = "si"
		serverDataMensajes.puenteData($scope.marcas[indice]);
		$location.path("/marcas");
	}
	//---------------------------------------
	$scope.procesar_activacion_quienes_somos =function(){
		$http.post("./controladores/marcasController.php",
		{
			'accion':'modificar_estatus',
			'id': $scope.id_seleccionado_marca,
			'estatus':$scope.estatus_seleccionado_marca
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_marcas();
				showSuccess("El registro fue actualizado de manera exitosa");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_marca = []
		$scope.estatus_seleccionado_marca = []
		var caja = event.currentTarget.id;
		//alert(caja);
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_marca = arreglo_atributos[0];
		$scope.estatus_seleccionado_marca = arreglo_atributos[1];
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado_marca==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}else{
				quitar_preload()
			}
		});
	}
	
	//---------------------------------------
	//--Bloque de Eventos
		$scope.sesion_usuario();
		cargar_preload()
		$scope.consultar_marcas();
	//--
	});