angular.module("ContenteManagerApp")
	.controller("usuariosController", function($scope,$http,$location,serverDataMensajes,sesionFactory,tiposUsuariosFactory){
	//---------------------------------------
	$scope.menu = "configuracion";
	$scope.submenu = "usuarios"
	$scope.titulo_pagina = "Usuarios";
	$scope.usuario = {
		 				"id":"",
		 				"cedula":"",
		 				"login":"",
		 				"nombres_apellidos":"",
		 				"telefono":"",
		 				"email":"",
		 				"tipos_usuarios":{"id":"","descripcion":""},
		 				"clave":"",
		 				"id_persona":"",
		 				"estatus":"",
		 				"consulta":""
	}
	$scope.tipos_usuarios = []
	//----------------------------------------------------------
	//datos sesion
	$scope.nombre_usuario = "";
	$scope.login_usuario = "";
	$scope.sesion_usuario = function(){
		sesionFactory.datos_sesion(function(data){
			if(data["nombre_usuario"]!=""){
				$scope.nombre_usuario = data["nombre_usuario"];
				$scope.login_usuario = data["login_usuario"];
			}
		});
	}
	$scope.sesion_usuario();
	//---

	//----------------------------------------------------------
	$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
	if($scope.valor_intermedio.consulta=='si'){
		if($scope.valor_intermedio){
				$scope.usuario = $scope.valor_intermedio;
		}
		//---
		//if($scope.valor_intermedio){
		if($scope.valor_intermedio.lenght!=0){	
			$scope.usuario = $scope.valor_intermedio;
			if($scope.usuario.id==undefined){
				$scope.usuario.id="";
			}
		}
		if($scope.usuario.consulta=='si'){
			//Para estatus
			if($scope.usuario.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.usuario.estatus = "1";
			}else
			if($scope.usuario.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.usuario.estatus = "0";
			}
			if($scope.usuario.tipos_usuarios){
					setTimeout(function(){
						$('#tipo_usuario').val($scope.usuario.tipos_usuarios.id);
						$('#tipo_usuario > option[value="'+$scope.usuario.tipos_usuarios.id+'"]').attr('selected', 'selected');
						$("#tipo_usuario").selectpicker('refresh');
					},1000);
			}
		}
	}	
	//---
	//--valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}
		});
	}
	
	/*$scope.validar_vacio = function(){
		if($scope.usuario.consulta=='si'){
			//Para estatus
			if($scope.usuario.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.usuario.estatus = "1";
			}else
			if($scope.usuario.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.usuario.estatus = "0";
			}
			if($scope.usuario.tipos_usuarios){
					setTimeout(function(){
						$('#tipo_usuario').val($scope.usuario.tipos_usuarios.id);
						$('#tipo_usuario > option[value="'+$scope.usuario.tipos_usuarios.id+'"]').attr('selected', 'selected');
						$("#tipo_usuario").selectpicker('refresh');
					},1000);
			}
		}	
	}	*/	
	//--
	//$scope.validar_vacio();			
	if($scope.usuario.id==undefined){
		$scope.usuario.id="";
	}
	if($scope.usuario.clave==undefined){
		$scope.usuario.clave="";
	}
	//--activar check en form
	$scope.activar_check = function(caja){
		if($(caja).hasClass("fa-check-square-o")){
			$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
			$scope.usuario.estatus = "0";	
		}else
		if($(caja).hasClass("fa-square-o")){
			$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
			$scope.usuario.estatus = "1";	
		}			
	}
	//---------------------------------------------------------
	//---Limpia arreglo de DataMensajes de otros controllers
	$scope.opcion_menu = function(opcion){
		serverDataMensajes.limpiar_arreglo_servicio();
	}
	//---Reinicia el valor del scope
	$scope.reiniciar_vars = function(){
		$scope.usuario = {
				 			"id":"",
			 				"cedula":"",
			 				"login":"",
			 				"nombres_apellidos":"",
			 				"telefono":"",
			 				"email":"",
			 				"tipos_usuarios":{"id":"","descripcion":""},
			 				"clave":"",
			 				"id_persona":"",
			 				"estatus":""

		}
		$("#usuario_clave2").val("")
		$('#tipo_usuario').val("0");
		$('#tipo_usuario > option[value=0]').attr('selected', 'selected');
		setTimeout(function(){
			$("#tipo_usuario").selectpicker('refresh');
		},800);
	}
	//--Metodo para registrar video
	$scope.registrar_usuarios = function(){
		if($scope.validar_campos()==true){
			uploader_reg("#campo_mensaje","#cedula_usuario,#usuario_nombres,#usuario_telefono,#usuario_email");
			//----------------------------------------------------------
			if(($scope.usuario.id!=undefined)&&($scope.usuario.id!="")){
				$scope.modificar_usuario();	
			}else{
				$scope.insertar_usuario();
			}
			//----------------------------------------------------------
		}		
	}
	//--Metodo para insertar registro de video
	$scope.insertar_usuario = function(){
		$http.post("./controladores/usuariosController.php",
		{
				'id': $scope.usuario.id,
				'cedula': $scope.usuario.cedula,
				'nombres_apellidos': $scope.usuario.nombres_apellidos,
				'login': $scope.usuario.login,	
				'telefono':$scope.usuario.telefono,
				'email':$scope.usuario.email,
				'clave':$scope.usuario.clave,
				'tipos_usuarios':$scope.usuario.tipos_usuarios.id,
				'accion':'registrar_usuarios'
		}).success(function(data, estatus, headers, config){
			if(data["mensajes"]=="registro_procesado"){
				showSuccess("El registro fue realizado de manera exitosa");
				$scope.usuario.id_personas =data["id_persona"];
				$scope.usuario.id = data["id"]
				$scope.limpiar_usuario()
			}else if(data["mensajes"]=="existe"){
				showSuccess("Ya existe ese usuario");
				$("#link_video").focus();
			}else if(data["mensajes"]=="existe_persona"){
				showSuccess("Ya existe la persona");
				$("#link_video").focus();
			}
			else{
				showErrorMessage("Ocurrió un error inesperado");
			}
			desbloquear_pantalla("#campo_mensaje","#cedula_usuario,#usuario_nombres,#usuario_telefono,#usuario_email");
		}).error(function(data,estatus){
			console.log(data);
		})
	}
	//--Metodo para modificar el registro de video
	$scope.modificar_usuario = function(){
		$http.post("./controladores/usuariosController.php",{
			'id': $scope.usuario.id,
			'cedula': $scope.usuario.cedula,
			'nombres_apellidos': $scope.usuario.nombres_apellidos,
			'login': $scope.usuario.login,	
			'telefono':$scope.usuario.telefono,
			'email':$scope.usuario.email,
			'clave':$scope.usuario.clave,
			'tipos_usuarios':$scope.usuario.tipos_usuarios.id,
			'id_persona':$scope.usuario.id_persona,
			'estatus':$scope.usuario.estatus,
			'accion':'modificar_usuarios'
		}).success(function(data, estatus, headers, config){
			if(data["mensajes"]=="modificacion_procesada"){
				showSuccess("El registro fue actualizado de manera exitosa");
				$scope.limpiar_usuario();
			}else if(data["mensajes"]=="no_existe"){
				showErrorMessage("No existe el usuario");
			}else if(data["mensajes"]=="no_existe_persona"){
				showErrorMessage("No existe la persona");
			}else if(data["mensajes"]=="existe_login"){
				showErrorMessage("Ya hay un usuario con ese login");
			}else{
				showErrorMessage("Ocurrió un error inesperado");
			}
			desbloquear_pantalla("#campo_mensaje","#cedula_usuario,#usuario_nombres,#usuario_telefono,#usuario_email");
		}).error(function(data,estatus){
			showErrorMessage("Ocurrió un error inesperado:"+data);
		})
	}
	//--
	$scope.consultar_usuarios = function(){
		cargar_preload();
		mostrar_preload();
		$location.path("/consultar_usuarios");
	}
	//--
	$scope.limpiar_usuario = function(){
		$scope.reiniciar_vars();
	}
	//--verifica si los campos!=''
	$scope.validar_campos = function(){
		if($scope.usuario.cedula==""){
			showErrorMessage("Debe ingresar la cédula");
			return false;	
		}else if($scope.usuario.login==""){
			showErrorMessage("Debe ingresar el login");
			return false;	
		}
		else if($scope.usuario.nombres_apellidos==""){
			showErrorMessage("Debe ingresar nombres y apellidos");
			return false;
		}else if(($scope.usuario.tipos_usuarios.id==0)||($scope.usuario.tipos_usuarios.id=="")){
			showErrorMessage("Debe seleccionar el tipo de usuario");
			return false;
		}else if($("#usuario_telefono").val()==""){
			showErrorMessage("Debe ingresar telefono");
			$scope.usuario.telefono="";
			return false;
		}else if($("#usuario_email").val()==""){
			showErrorMessage("Debe ingresar email");
			return false;
		}else if(($scope.usuario.clave=="")&&(($scope.usuario.id=="")||($scope.usuario.id==undefined))){
			showErrorMessage("El campo de la clave no puede estar en blanco");
			return false;
		}else if((($scope.usuario.clave) != ($("#usuario_clave2").val()))&&(($scope.usuario.id=="")||($scope.usuario.id==undefined))){
			showErrorMessage("Los valores de las claves deben coincidir");
			return false;
		}else if(($scope.usuario.clave.length<8)&&(($scope.usuario.id=="")||($scope.usuario.id==undefined))){
			showErrorMessage("La clave debe tener 8 dígitos");
			return false;
		}
		else{
			return true;
		}
	}
	//--Consultar tipos de usuarios
	$scope.consultar_tipos_usuarios = function(){
		$scope.tipos_usuarios = "";
		tiposUsuariosFactory.cargar_usuarios(function(data){
				$scope.tipos_usuarios=data;
				setTimeout(function(){
					$("#tipo_usuario").selectpicker('refresh');
				},800);
				//console.log(data);		
		});
	}
	//---------------------------------------------------------
	$scope.sesion_usuario();
	cargar_preload()
	$scope.consultar_tipos_usuarios()
	//--------------------------------------------------------
	});