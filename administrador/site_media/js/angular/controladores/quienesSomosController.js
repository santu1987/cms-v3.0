angular.module("ContenteManagerApp")
	.controller("quienesSomosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory){
		//--Cuerpo de delcaraciones
		$scope.menu = "empresa";
		$scope.titulo_pagina = "Quienes Somos";
		$scope.activo_img = "inactivo";
		$scope.currentTab = 'datos_basicos'
		$scope.empresa = {
								'id':'',
								'quienes_somos':'',
								'mision':'',
								'vision':'',
								'objetivo':'',
								'mensaje_contacto':'',
								'imagen':'',
								'id_imagen':'',
								'idioma':''
		}
		$scope.borrar_imagen = []
		$scope.valor_intermedio = []
		$scope.galery = []
		$scope.seccion = ''
		$scope.opcion = ''
		$scope.valor_opcion = ''
		//---
		//--Cuerpo de metodos
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		//----------------------------------------------------------
		//datos sesion
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		//---
		//Si el form viene de consulta se alimenta del servicio...
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
		//alert($scope.valor_intermedio.consulta);
		if($scope.valor_intermedio.consulta=="si"){

			//Para estatus
			if($scope.valor_intermedio.lenght!=0){
				$scope.empresa = $scope.valor_intermedio;
				if($scope.empresa.imagen!=undefined){
					$scope.activo_img='activo'
				}
				
				$scope.empresa.quienes_somos ? $("#div_quienes_somos").html($scope.empresa.quienes_somos) : $("#div_quienes_somos").html("")

				$scope.empresa.objetivo ? $("#div_objetivo").html($scope.empresa.objetivo) : $("#div_objetivo").html("")
				
				$scope.empresa.mision ? $("#div_mision").html($scope.empresa.mision) : $("#div_mision").html("")

				$scope.empresa.vision ? $("#div_vision").html($scope.empresa.vision) : $("#div_vision").html("")


				if($scope.empresa.id==undefined){
					$scope.empresa.id = "";
				}


			}
			if($scope.empresa.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.empresa.estatus = "1";
			}else
			if($scope.empresa.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.empresa.estatus = "0";
			}
			//Para el idioma
			$scope.empresa.idioma.id = $scope.valor_intermedio.id_idioma
			$('#idioma').val($scope.empresa.idioma.id);
			$scope.valor_intermedio.consulta = "";
		}
		//---
		$scope.wisi_modal = function(opcion){
			//---
			$(".trumbowyg-editor").html("")
			$scope.opcion = opcion
			if($scope.empresa!=undefined){
			//---------------------------------
				switch($scope.opcion){
					case "1":
						$scope.seccion= "Quienes Somos"
						if($scope.empresa.quienes_somos!=""){
							$(".trumbowyg-editor").html($scope.empresa.quienes_somos)
						}
						break;
					case "2":
						$scope.seccion= "Objetivo"
						if($scope.empresa.objetivo!=""){
							$(".trumbowyg-editor").html($scope.empresa.objetivo)
						}
						break;
					case "3":
						$scope.seccion= "Misión"
						if($scope.empresa.mision!=""){
							$(".trumbowyg-editor").html($scope.empresa.mision)
						}
						break;
					case "4":
						$scope.seccion= "Visión"
						if($scope.empresa.vision!=""){
							$(".trumbowyg-editor").html($scope.empresa.vision)
						}
						break;			
				}
			//---------------------------------	
			}
			
			//---
			$("#modal_contenido").modal("show")
		}
		//---
		$scope.agregar_contenido = function(){
			if($scope.empresa==undefined){
				$scope.empresa = {
									'id':'',
									'quienes_somos':'',
									'mision':'',
									'vision':'',
									'objetivo':'',
									'mensaje_contacto':'',
									'imagen':'',
									'id_imagen':'',
									'idioma':''
				}
			}
			switch($scope.opcion){
				case "1":
					$scope.empresa.quienes_somos = $(".trumbowyg-editor").html()
					$("#div_quienes_somos").html($scope.empresa.quienes_somos)
					break;
				case "2":
					$scope.empresa.objetivo = $(".trumbowyg-editor").html()
					$("#div_objetivo").html($scope.empresa.objetivo)	
					break
				case "3":
					$scope.empresa.mision = $(".trumbowyg-editor").html()
					$("#div_mision").html($scope.empresa.mision)	
					break
				case "4":
					$scope.empresa.vision = $(".trumbowyg-editor").html()
					$("#div_vision").html($scope.empresa.vision)	
					break		
			}
		}
		//---
		$scope.registrar_quienes_somos = function(){
			if($scope.validar_form()==true){
				uploader_reg("#mensaje_quienes_somos",".trumbowyg-editor");
				//Para guardar
				if(($scope.empresa.id!="")&&($scope.empresa.id!=undefined)){
					$scope.modificar_quienes_somos();	
				}else{
					$scope.insertar_quienes_somos();
				}

				
			}
		}
		//--
		//--------------------------------------
		//---
		$scope.insertar_quienes_somos = function(){
			$http.post("./controladores/quienesSomosController.php",
			{
				 'id':$scope.empresa.id,
				 'quienes_somos': $scope.empresa.quienes_somos,
				 'mision':$scope.empresa.mision,
				 'vision':$scope.empresa.vision,
				 'objetivo':$scope.empresa.objetivo,
				 'id_imagen':$scope.empresa.id_imagen,
				 'id_idioma': $scope.empresa.idioma.id,
				 'accion':'registrar_quienes_somos'

			}).success(function(data, estatus, headers, config){
					if(data["mensajes"]=="registro_procesado"){
						showSuccess("El registro fue realizado de manera exitosa");
						$scope.empresa.id =data["id"];
						$scope.limpiar_cajas_quienes_somos();
					}
					else{
						showErrorMessage("Ocurrió un error inesperado");
					}
					desbloquear_pantalla("#mensaje_quienes_somos",".trumbowyg-editor");
				}).error(function(data,estatus){
					console.log(data);
			});
		}
		//---
		$scope.validar_form = function(){
			if(($scope.empresa.idioma.id=="")||($scope.empresa.idioma.id==undefined)){
				showErrorMessage("Debe seleccionar un idioma");				
				return false;
			}else if(($scope.empresa.quienes_somos=="")||($scope.empresa.quienes_somos==undefined)){
				showErrorMessage("Debe ingresar el contenido de quienes somos");				
				return false;
			}else if(($scope.empresa.objetivo=="")||($scope.empresa.objetivo==undefined)){
				showErrorMessage("Debe ingresar el contenido de objetivo");				
				return false;
			}else if(($scope.empresa.mision=="")||($scope.empresa.mision==undefined)){
				showErrorMessage("Debe ingresar el contenido de misión");				
				return false;
			}else if(($scope.empresa.vision=="")||($scope.empresa.vision==undefined)){
				showErrorMessage("Debe ingresar el contenido de visión");				
				return false;
			}else if(($scope.empresa.id_imagen=="")||($scope.empresa.id_imagen==undefined)){
				showErrorMessage("Debe seleccionar una imagen");				
				return false;
			}else{
				return true;
			}
		}
		//---
		$scope.modificar_quienes_somos = function(){

			$http.post("./controladores/quienesSomosController.php",
			{
				 'id':$scope.empresa.id,
				 'quienes_somos': $scope.empresa.quienes_somos,
				 'mision':$scope.empresa.mision,
				 'vision':$scope.empresa.vision,
				 'objetivo':$scope.empresa.objetivo,
				 'id_imagen':$scope.empresa.id_imagen,
				 'id_idioma': $scope.empresa.idioma.id,
				 'estatus': $scope.empresa.estatus,
				 'accion':'modificar_quienes_somos'

			}).success(function(data, estatus, headers, config){
				if(data["mensajes"]=="modificacion_procesada"){
					showSuccess("El registro fue actualizado de manera exitosa");
					$scope.limpiar_cajas_quienes_somos();
				}else if(data["mensajes"]=="no_existe"){
					showSuccess("No existe el registro de empresa");
				}
				else{
					showErrorMessage("Ocurrió un error inesperado");
				}
				desbloquear_pantalla("#mensaje_quienes_somos",".trumbowyg-editor");
			}).error(function(data,estatus){
					console.log(data);
			});
		}
		//---
		/*$scope.consultar_quienes_somos = function(){
			$http.post("./controladores/quienesSomosController.php",
			{
				 'accion':'consultar_quienes_somos'

			}).success(function(data, estatus, headers, config){
				console.log(data);
				$scope.empresa= data[0]	
				console.log($scope.empresa);
				//Asigno valores a los div
				if($scope.empresa!=undefined){
				//--------------------------------------
					$("#div_quienes_somos").html($scope.empresa.quienes_somos)
					$("#div_mision").html($scope.empresa.mision)
					$("#div_vision").html($scope.empresa.vision)
					$("#div_objetivo").html($scope.empresa.objetivo)
					if($scope.empresa.imagen!=""){
						$scope.activo_img = "activo";
					}
				//--------------------------------------	
				}else{
					$scope.empresa = {
											'id':'',
											'quienes_somos':'',
											'mision':'',
											'vision':'',
											'objetivo':'',
											'mensaje_contacto':'',
											'imagen':'',
											'id_imagen':'',
											'idioma':''
					}
				}
				
				
			}).error(function(data,estatus){
					console.log(data);
			});
		}*/
		//------------------------------------------------------
		$scope.consultar_quienes_somos = function(){
			setTimeout(function(){
				$scope.limpiar_cajas_quienes_somos();
			},500)
			$location.path("/consultar_quienes_somos");
		}
		//------------------------------------------------------
		$scope.limpiar_cajas_quienes_somos = function(){
			$scope.empresa = {
								'id':'',
								'quienes_somos':'',
								'mision':'',
								'vision':'',
								'objetivo':'',
								'mensaje_contacto':'',
								'imagen':'',
								'id_imagen':'',
								'idioma':''
			}
			$("#contenido_descripcion").text("");
			$("#div_quienes_somos").text("Pulse aquí para ingresra el contenido de quienes somos");
			$("#div_objetivo").text("Pulse aquí para ingresra el contenido de objetivo");
			$("#div_mision").text("Pulse aquí para ingresra el contenido de misión");
			$("#div_vision").text("Pulse aquí para ingresra el contenido de visión");
			$("#idioma").val("0");
			$scope.activo_img = "inactivo"
		}
		//------------------------------------------------------
		//--Cuerpo de metodos
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");		
		}	
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.valor_id_categoria('3');//ya que es galeria de especialidades
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
			});
		}
		//---
		$scope.consultar_idioma = function(){
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data
				console.log($scope.idioma)
			});
		}
		//---
		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.empresa.id_imagen = id_imagen
			$scope.empresa.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}
		//---Limpia arreglo de DataMensajes de otros controllers
		$scope.opcion_menu = function(opcion){
			serverDataMensajes.limpiar_arreglo_servicio();
			$scope.valor_opcion = opcion;
		}
		//----------------------------------------
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				//alert(data["respuesta"]);
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//---------------------------------------------------------
		$scope.activar_check = function(caja){
			if($(caja).hasClass("fa-check-square-o")){
				$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.empresa.estatus = "0";	
			}else
			if($(caja).hasClass("fa-square-o")){
				$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.empresa.estatus = "1";	
			}			
		}
		//---
		//if($scope.valor_opcion=="quienes_somos"){
			//--Cuerpo de llamados
		$scope.sesion_usuario();
		//$scope.consultar_quienes_somos(); 
		$scope.consultar_galeria_img();
		$scope.consultar_idioma();
		cargar_preload();
		//}
		//--------------------------------------------------------
		
	});