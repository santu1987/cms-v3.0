angular.module("ContenteManagerApp")
	.controller("contactosempleoController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory, contactosFactory){
		//Cuerpo declaraciones
		$scope.menu = "activo";
		$scope.menu = "contactos";
		$scope.titulo_pagina = "Contactos Empleo";
		$scope.activo_img = "inactivo";
		$scope.titulo_seccion = "Contactos Empleo";
		$scope.contactos_empleo = {
								'id':'',
								'nombres':'',
								'email':'',
								'area':'',
								'mensaje':'',
								'cv':'',
								'telefono':'',
								'num':'',
								'consulta':'si'
		}
		$scope.detalle = {
								'id':'',
								'nombres':'',
								'email':'',
								'area':'',
								'mensaje':'',
								'cv':'',
								'telefono':'',
								'num':''
		}						
		$scope.titulo_mensaje = [];
		//---
		//Cuerpo de llamados
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.consultar_contactos_empleo = function(){
			contactosFactory.cargar_contactos_empleos(function(data){
				$scope.contactos_empleo=data;
				console.log($scope.contactos_empleo)
				setTimeout(function(){
						iniciar_datatable();
				},500);
			});
		}
		//--Para visualizar resumen
		$scope.ver_detalle = function(index){
			$("#modal_mensaje").modal("show");
			$scope.detalle = $scope.contactos_empleo[index]
			$scope.titulo_mensaje = "Resumén de contacto"

		}
		//--Para descargar cv
		$scope.descargar_cv = function(num){
			if($scope.contactos_empleo[num].cv!=""){
				$("#form_contacto_empleado").attr("action",$scope.contactos_empleo[num].cv);
    			$("#form_contacto_empleado").submit();
			}
		}
		//Cuerpo de metodos
		$scope.consultar_contactos_empleo();
		//---

	});	