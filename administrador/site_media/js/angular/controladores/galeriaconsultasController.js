angular.module("ContenteManagerApp")
	.controller("galeriaconsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "galeria";
		$scope.submenu = "admin"
		$scope.titulo_seccion = "Consulta galería";
		$scope.galeria = {
								'id_galeria':'',
								'imagen':'',
								'descripcion':'',
								'id_imagen':'',
								'estatus':'',
								'consulta':''
		}
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.consultar_galeria_img = function(){
			$http.post("./controladores/secciongaleriaController.php",
			{
				'accion':'consultar_galeria'
			}).success(function(data, status, headers, config){
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else{
					$scope.galeria = data;
					console.log($scope.galeria);
					setTimeout(function(){
						iniciar_datatable();
					},500);
				}
			}).error(function(data,status){
				console.log(data);
			});
		}
	//---------------------------------------
	$scope.ver_galeria = function(indice){
		$scope.galeria[indice].consulta = "si";
		serverDataMensajes.puenteData($scope.galeria[indice]);
		$location.path("/galeria");
	}	
	//---------------------------------------
	$scope.procesar_activacion_galeria =function(){
		$http.post("./controladores/secciongaleriaController.php",
		{
			'accion':'modificar_estatus',
			'id': $scope.id_seleccionado_galeria,
			'estatus':$scope.estatus_seleccionado_galeria
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_galeria_img();
				showSuccess("El registro fue actualizado de manera exitosa");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_galeria = []
		$scope.estatus_seleccionado_galeria = []
		var caja = event.currentTarget.id;
		//alert(caja);
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_galeria = arreglo_atributos[0];
		$scope.estatus_seleccionado_galeria = arreglo_atributos[1];
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}
		});
	}
	//---------------------------------------
	//---------------------------------------
	//--Bloque de Eventos
		$scope.sesion_usuario();
		cargar_preload();
		$scope.consultar_galeria_img();
	//--
	});