angular.module("ContenteManagerApp")
	.controller("quienesSomosConsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "activo";
		$scope.submenu = "admin"
		$scope.titulo_seccion = "Consulta quienes somos";
		$scope.empresa = {
								'id':'',
								'quienes_somos':'',
								'mision':'',
								'vision':'',
								'objetivo':'',
								'mensaje_contacto':'',
								'imagen':'',
								'id_imagen':'',
								'idioma':'',
								'consulta':'si'
		}
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		//
		$scope.consultar_quienes_somos_c = function(){
			serverDataMensajes.puenteData("");
			$http.post("./controladores/quienesSomosController.php",
			{
				'accion':'consultar_quienes_somos'
			}).success(function(data, status, headers, config){
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else{
					$scope.empresa = data;
					setTimeout(function(){
						iniciar_datatable();
					},500);
				}
			}).error(function(data,status){
				console.log(data);
			});
		}
	//---------------------------------
	$scope.ver_empresa = function(indice){
		$scope.empresa[indice].consulta = "si"
		serverDataMensajes.puenteData($scope.empresa[indice]);
		$location.path("/quienes_somos");
	}
	//---------------------------------------
	$scope.procesar_activacion_quienes_somos =function(){
		$http.post("./controladores/quienesSomosController.php",
		{
			'accion':'modificar_estatus',
			'id': $scope.id_seleccionado_qs,
			'estatus':$scope.estatus_seleccionado_qs
		}).success(function(data, status, headers, config){
			if(data["mensajes"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_quienes_somos_c();
				showSuccess("El registro fue actualizado de manera exitosa");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_qs = []
		$scope.estatus_seleccionado_qs = []
		var caja = event.currentTarget.id;
		//alert(caja);
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_qs = arreglo_atributos[0];
		$scope.estatus_seleccionado_qs = arreglo_atributos[1];
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado_qs==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}else{
				quitar_preload()
			}
		});
	}
	//---------------------------------------
	//--Bloque de Eventos
		$scope.sesion_usuario();
		cargar_preload()
		$scope.consultar_quienes_somos_c();
	//--
	});