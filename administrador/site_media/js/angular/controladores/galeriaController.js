angular.module("ContenteManagerApp")
	.controller("galeriaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory){
		//Cuerpo declaraciones
		$scope.menu = "galeria";
		$scope.submenu = "admin";
		$scope.titulo_pagina = "Galeria";
		$scope.activo_img = "inactivo";
		$scope.galeria = {
								'id_galeria':'',
								'imagen':'',
								'descripcion':'',
								'id_imagen':'',
								'estatus':'',
								'consulta':'',
								'idioma':'',
		}
		$scope.borrar_imagen = []
		//Cuerpo de metodos
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		//---
		//Si el form viene de consulta se alimenta del servicio...
		$scope.valor_intermedio = serverDataMensajes.obtener_arreglo();
		//---
		if($scope.valor_intermedio.consulta=="si"){
			if($scope.valor_intermedio.lenght!=0){
				$scope.galeria = $scope.valor_intermedio;
				if($scope.galeria.imagen!=undefined){
					$scope.activo_img='activo'
				}
				if($scope.galeria.id_galeria==undefined){
					$scope.galeria.id_galeria = '';
				}
			}
			//Para estatus
			if($scope.galeria.estatus=="1"){
				$("#check_publicado").removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.galeria.estatus = "1";
			}else
			if($scope.galeria.estatus=="0"){
				$("#check_publicado").removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.galeria.estatus = "0";
			}
			$scope.valor_intermedio.consulta="";
		}	
		//---
		$scope.activar_check = function(caja){
			if($(caja).hasClass("fa-check-square-o")){
				$(caja).removeClass("fa-check-square-o").addClass("fa-square-o");
				$scope.galeria.estatus = "0";	
			}else
			if($(caja).hasClass("fa-square-o")){
				$(caja).removeClass("fa-square-o").addClass("fa-check-square-o");
				$scope.galeria.estatus = "1";	
			}			
		}
		//---
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
				
		}	
		//---
		$scope.consultar_galeria_img = function(){
			galeriaFactory.valor_id_categoria('6');//1 ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galeria_imagen=data;
			});
		}
		//---
		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			/*if(($("#"+imagen).hasClass("marcado"))==true){
				$("#"+imagen).removeClass("marcado");
				$indice = $scope.borrar_imagen.indexOf(id_imagen);
				$scope.borrar_imagen.splice($indice,1);
				$scope.activo_img = "inactivo"
				$scope.galeria.imagen = '';
			}else{
				$("#"+imagen).addClass("marcado");
				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.galeria.id_imagen = id_imagen
				$scope.galeria.imagen = ruta
			}*/
			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.galeria.id_imagen = id_imagen
			$scope.galeria.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}
		//---
		$scope.registrar_galeria = function(){
			if($scope.validar_form()==true){
				//Para guardar
				//alert("id_personas:"+$scope.doctor.id_personas);
				uploader_reg("#mensaje_galeria","#galeria_descripcion");
				if(($scope.galeria.id_galeria!="")&&($scope.galeria.id_galeria!=undefined)){
					//alert("modificar");
					$scope.modificar_galeria();	
				}else{
					//alert("insertar");
					$scope.insertar_galeria();
				}
				
			}
		}
		//---
		$scope.validar_form = function(){
			if($scope.galeria.imagen==""){
				showErrorMessage("Debe selecionar una imagen!");				
				return false;
			}else if($scope.galeria.descripcion==""){
				showErrorMessage("Debe indicar descripción de la imagen!");				
				return false;
			}else{
				return true;
			}
		}
		//--
		$scope.insertar_galeria = function (){
			//alert($scope.galeria.id_imagen);
			$http.post("./controladores/secciongaleriaController.php",
			{
							 'id':$scope.galeria.id_galeria,
							 'id_imagen':$scope.galeria.id_imagen,
							 'descripcion':$scope.galeria.descripcion,
							 'id_idioma':$scope.galeria.idioma.id,
							 'accion':'registrar_galeria'

			}).success(function(data, estatus, headers, config){
					if(data["mensajes"]=="registro_procesado"){
						showSuccess("El registro fue realizado de manera exitosa");
						$scope.galeria.id_galeria =data["id"];
						$scope.limpiar_cajas();
					}else if(data["mensajes"]=="existe"){
						showSuccess("Ya existe la imagen registrada");
					}
					else{
						showErrorMessage("Ocurrió un error inesperado");
					}
					desbloquear_pantalla("#mensaje_galeria","#galeria_descripcion");
				}).error(function(data,estatus){
					console.log(data);
			});	
		}
		//--
		$scope.modificar_galeria = function(){
			$http.post("./controladores/secciongaleriaController.php",
			{
							 'id':$scope.galeria.id_galeria,
							 'id_imagen':$scope.galeria.id_imagen,
							 'descripcion':$scope.galeria.descripcion,
							 'estatus': $scope.galeria.estatus,
							 'id_idioma':$scope.galeria.idioma.id,
							 'accion':'modificar_galeria'

			}).success(function(data, estatus, headers, config){
				if(data["mensajes"]=="modificacion_procesada"){
					showSuccess("El registro fue actualizado de manera exitosa");
					$scope.limpiar_cajas();
				}else if(data["mensajes"]=="no_existe"){
					showSuccess("No existe la imagen");
				}
				else{
					showErrorMessage("Ocurrió un error inesperado");
				}
				desbloquear_pantalla("#mensaje_galeria","#galeria_descripcion");
			});
		}
		//--
		$scope.limpiar_cajas = function(){
			$scope.galeria = {
								'id_galeria':'',
								'imagen':'',
								'descripcion':'',
								'id_imagen':'',
								'estatus':'',
								'idioma':''
			}
			$scope.activo_img = "inactivo"
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		
		//---Limpia arreglo de DataMensajes de otros controllers
		$scope.opcion_menu = function(opcion){
			serverDataMensajes.limpiar_arreglo_servicio();
		}
		//----------------------------------------
		//---
		$scope.consultar_galeria = function(){
			setTimeout(function(){
				$scope.limpiar_cajas();
			},500)
			$location.path("/consultar_galeria");
		}
		//----------------------------------------
		//valido inicio de sesion
		$scope.sesion_usuario = function(){
			sesionFactory.sesion_usuario(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//---------------------------------------------------------
		//Cuerpo de Llamados a metodos
		$scope.sesion_usuario();
		cargar_preload();
		$scope.consultar_galeria_img();
		//---------------------------------------------------------
	});