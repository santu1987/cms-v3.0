angular.module("ContenteManagerApp")
	.controller("correosconsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory){
	//---------------------------------------
		$scope.menu = "contactos";
		$scope.submenu = "admin"
		$scope.titulo_pagina = "Consulta correos";
		$scope.titulo_seccion = "Consulta correos";
		$scope.activo_img = "inactivo";
		$scope.correos = {
									"id":"",
									'correo':'',
									"titulo":'',
									"estatus":'',
									"consulta":""			
		}
		$scope.cuantos = 1
		//Cuerpo de metodos
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.consultar_correos = function(){
			serverDataMensajes.puenteData("");
			$http.post("./controladores/correoController.php",
			{
				'accion':'consultar_correos'
			}).success(function(data, status, headers, config){
				console.log(data);
				if(data["mensajes"]=="error"){
					showErrorMessage("Ocurrió un error inesperado");
				}else{
					$scope.correos = data;
					console.log($scope.correos);
					setTimeout(function(){
						iniciar_datatable();
					},500);
				}
			}).error(function(data,status){
				console.log(data);
			});
		}
	//---------------------------------
	$scope.ver_correo = function(indice){
		$scope.correos[indice].consulta = "si"
		serverDataMensajes.puenteData($scope.correos[indice]);
		$location.path("/correos");
	}
	//---------------------------------------
	$scope.procesar_activacion_correo =function(){
		$http.post("./controladores/correoController.php",
		{
			'accion':'modificar_estatus',
			'id': $scope.id_seleccionado_email,
			'estatus':$scope.estatus_seleccionado_email
		}).success(function(data, status, headers, config){
			console.log(data);
			if(data["error"]=="error"){
				showErrorMessage("Ocurrió un error inesperado");
			}else if(data["mensajes"]=="modificacion_procesada"){
				$scope.consultar_correos();
				showSuccess("El registro fue actualizado de manera exitosa");
			}
		}).error(function(data,status){
			console.log(data);
			showErrorMessage("Ocurrió un error inesperado: "+ data);
		});
	}
	//---------------------------------------
	$scope.activar_registro = function(event){
		$scope.id_seleccionado_email = []
		$scope.estatus_seleccionado_email = []
		var caja = event.currentTarget.id;
		//alert(caja);
		var atributos = $("#"+caja).attr("data");
		var arreglo_atributos = atributos.split("|");
		$scope.id_seleccionado_email = arreglo_atributos[0];
		$scope.estatus_seleccionado_email = arreglo_atributos[1];
		$("#cabecera_mensaje").text("Información:");
		if ($scope.estatus_seleccionado_email==0){
			mensaje = "Desea modificar el estatus de este registro a publicado? ";
		}else{
			mensaje = "Desea modificar el estatus de este registro a inactivo? ";
		}
		$("#cuerpo_mensaje").text(mensaje);
		$("#modal_mensaje").modal("show");
	}
	//----------------------------------------
	//valido inicio de sesion
	$scope.sesion_usuario = function(){
		sesionFactory.sesion_usuario(function(data){
			if(data["respuesta"]=="cerrado"){
				$location.path("/");
			}else{
				quitar_preload()
			}
		});
	}
	//---------------------------------------
	//--Bloque de Eventos
		$scope.sesion_usuario();
		cargar_preload()
		$scope.consultar_correos();
	//--
	});