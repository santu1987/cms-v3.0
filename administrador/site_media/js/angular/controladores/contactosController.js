angular.module("ContenteManagerApp")
	.controller("contactosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory, contactosFactory){
		//Cuerpo declaraciones
		$scope.menu = "activo";
		$scope.menu = "contactos";
		$scope.titulo_pagina = "Contactos";
		$scope.activo_img = "inactivo";
		$scope.titulo_seccion = "Contactos";
		$scope.contactos = {
								'id':'',
								'nombres':'',
								'email':'',
								'telefono':'',
								'mensaje':''
		}
		$scope.detalle = {
								'id':'',
								'nombres':'',
								'email':'',
								'telefono':'',
								'mensaje':''
		}
		$scope.titulo_mensaje = [];
		//---
		//Cuerpo de llamados
		//----------------------------------------------------------
		//datos sesion
		$scope.nombre_usuario = "";
		$scope.login_usuario = "";
		$scope.sesion_usuario = function(){
			sesionFactory.datos_sesion(function(data){
				if(data["nombre_usuario"]!=""){
					$scope.nombre_usuario = data["nombre_usuario"];
					$scope.login_usuario = data["login_usuario"];
				}
			});
		}
		$scope.sesion_usuario();
		//---

		$scope.consultar_contactos = function(){
			contactosFactory.cargar_contactos(function(data){
				$scope.contactos=data;
				console.log($scope.contactos)
				setTimeout(function(){
						iniciar_datatable();
				},500);
			});
		}
		//--Para visualizar resumen
		$scope.ver_detalle = function(index){
			$("#modal_mensaje").modal("show");
			$scope.detalle = $scope.contactos[index]
			$scope.titulo_mensaje = "Resumén de contacto"

		}
		//Cuerpo de metodos
		$scope.consultar_contactos();
		//---

	});	