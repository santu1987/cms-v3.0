angular.module("ContenteManagerApp")
//---------------------------------------------------------------------------------------
//--Bloque de servicios
//--Servicio para cargar imagenes...	
.service('upload', ["$http", "$q", function ($http, $q) 
{
	this.uploadFile = function(file, categoria,nombre_archivo)
	{
		var deferred = $q.defer();
		var formData = new FormData();
		formData.append("categoria", categoria);
		formData.append("file", file);
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post("./controladores/imagenesController.php", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	},
	this.uploadFileFolleto = function(file, id_negocio,nombre_archivo)
	{
		var deferred = $q.defer();
		var formData = new FormData();
		
		formData.append("id_negocio", id_negocio);
		formData.append("file", file);
		/*formData.append("categoria", categoria);
		formData.append("file", file);*/
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post("./controladores/archivosController.php", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	}	
}])
//--Servicio para compartir datos de mensajes iniciales
.service('serverDataMensajes',[function(){
	var puente = [];
	this.puenteData = function(arreglo){
		puente = arreglo;
		return puente;
	}
	this.obtener_arreglo = function(){
		return puente;
	}
	this.limpiar_arreglo_servicio = function (){
		puente = [];
		return puente;
	}
}])
//--Bloque de factorias
//Factory para verificar inicio de sesion 
.factory("sesionFactory",['$http', function($http){
	return{
			datos_sesion : function(callback){
				$http.post("./controladores/fbasicController.php", { accion:'datos_sesion'}).success(callback);
			},
			sesion_usuario : function(callback){
				$http.post("./controladores/fbasicController.php", { accion:'consultar_sesion'}).success(callback);
			},
			cerrar_sesion: function(callback){
				$http.post("./controladores/fbasicController.php", { accion:'cerrar_sesion'}).success(callback);	
			}
	}
}])
.factory("categoriasFactory",['$http', function($http){
	return{
			cargar_categorias_fa : function(callback){
				$http.post("./controladores/categoriasController.php", { accion:'consultar'}).success(callback);
			}
	}
}])
//Para las especialidades
.factory("especialidadFactory",['$http', function($http){
	return{
			cargar_especialidad : function(callback){
				$http.post("./controladores/especialidadesController.php", { accion:'consultar'}).success(callback);
			}
	}
}])
//Para consultar idiomas
.factory("idiomaFactory",['$http', function($http){
	return{
			cargar_idioma : function(callback){
				$http.post("./controladores/quienesSomosController.php", { accion:'consultar_idioma'}).success(callback);
			}
	}
}])
//Para la galeria
.factory("galeriaFactory",['$http', function($http){

	return{
			valor_id_categoria : function (id,nombre){
				if(id!="")
					categoria = id;
				else
					categoria = "";
			},
			valor_nombre_imagen : function (nombre){
				if(nombre!=""){
					xxx = nombre;
				}
				else{
					xxx = "";
				}
			},
			cargar_galeria_fa : function(callback){
				$http.post("./controladores/galeriaController.php",{ accion:'consultar_imagenes', categoria:categoria}).success(callback);
			},
			cargar_galeria_nombres : function(callback){
				$http.post("./controladores/galeriaController.php",{ accion:'consultar_imagenes',nombre:xxx}).success(callback);
			}
	}
}])
//Para las citas
.factory("citasProgramadasFactory",['$http', function($http){

	return{
			valor_estatus : function (estatus_op, especialidad, doctor){
				if(estatus_op!="")
					estatus = estatus_op;
				else
					estatus = "";
				if(especialidad!="")
					id_especialidad = especialidad;
				else
					id_especialidad = "";
				if(doctor!="")
					id_doctor = doctor
				else
					id_doctor = doctor	
			},
			cargar_citas_programadas : function(callback){
				$http.post("./controladores/citasController.php",{ accion:'consultar_citas_programadas', estatus:estatus, id_especialidad:id_especialidad, id_doctor:id_doctor }).success(callback);
			}
	}
}])
.factory("tiposNegociosFactory",['$http', function($http){
	return{
		valor_estatus : function (estatus_op){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
		},		
		cargar_tiposNegocios : function(callback){
			$http.post("./controladores/tiposNegociosController.php", {accion:'consultar_tipos_negocios'}).success(callback);
		}
	}	
}])
.factory("tiposUsuariosFactory",['$http', function($http){
	return{
		cargar_usuarios : function(callback){
			$http.post("./controladores/usuariosController.php", {accion:'consultar_tipos_usuarios'}).success(callback);
		}
	}
}])
.factory("contactosFactory",['$http', function($http){
	return{
		cargar_contactos : function(callback){
			$http.post("./controladores/contactosController.php", {accion:'consultar_clientes'}).success(callback);
		},
		cargar_contactos_empleos : function(callback){
			$http.post("./controladores/contactosController.php", {accion:'consultar_empleos'}).success(callback);
		}
	}	
}])
.factory("redesFactory",['$http', function($http){
	return{
		asignar_tipo_red : function (valor_id_tipo_red){
			if(valor_id_tipo_red!=""){
				id_tipo_red= valor_id_tipo_red;
			}else{
				id_tipo_red = ""
			}
		},
		cargar_redes_detalles: function(callback){
			$http.post("./controladores/redesController.php", {accion:'consultar_redes_detalle', 'id_tipo_red':id_tipo_red}).success(callback);
		},
		cargar_redes : function(callback){
			$http.post("./controladores/redesController.php", {accion:'consultar_redes'}).success(callback);
		}
	}	
}]);