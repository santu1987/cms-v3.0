<?php
require("../core/fbasic.php");
//--Armo diccionario 
function render_list($html,$data){
$render = "";
//---------------------------------------------------------------------------------------
	$match_cal = set_match_identificador_dinamico($html,"<!--row_slider-->");
	if($data!="error"){
		for($i=0;$i<count($data);$i++){
			$a = $i+1;
			$dic = array(
							"{id}" 	=> $data[$i][0],
							"{titulo}" 	=> $data[$i][1],
							"{descripcion}" => $data[$i][2],
							"{imagen}"	=> $data[$i][3],
							"{numero}" => $a
						);
			$render.=str_replace(array_keys($dic), array_values($dic), $match_cal);
		}
	}
	$html = str_replace($match_cal, $render, $html);
	return $html;
//----------------------------------------------------------------------------------------	
}
//--Para renderización estática
/*function render_estaticos($html,$data){
	foreach ($data as $clave => $valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}*/
//----------------------------------------------------------------------------------------
//--Para renderizacion dinamica
function render_vista_consulta($html,$data){
	$template = "";
	//print $html;
	$template=get_template($html);
	$html = render_list($template,$data);//--renderización dinamica
	//$html = render_estaticos($html,$arr_pag);//--renderización estatica
	print $html;
	//die("aaa");
	//die($html);
}
//----------------------------------------------------------------------------------------
?>